#!/bin/sh
echo "Running LigandFit on 1J4R data..."
phenix.ligandfit data=1J4R.mtz model=1J4R_no_ligand.pdb ligand=1J4R_random.pdb input_labels='FP ' lig_map_type=fo-fc_difference_map
