POMGNT2: Alphafold model, multiple domains, two copies.
POMGNT2 = protein O-linked mannose acetylglucosaminyl transferase-2

The enyme O-linked mannose N-acetylglucosaminyl transferase 2, often
abbreviated as POMGnT2, plays a role in the O-linked glycosylation of proteins;
specifically, POMGnT2 is responsible for adding N-acetylglucosamine (GlcNAc) to
mannose residues in the glycan structures of proteins.

The first Alphafold database was trained on PDB and sequence data available on
15 February 2021. At this time there were no detectable homologues of POMGNT2
that had known structures, so models built with this database had no templates.

Two groups determined structures of POMGNT2 after this date. Yang et al made a
construct with the mature sequence of the human protein (starting at residue
25) and determined two structures, deposited as PDB entries 6xfi and 6ix2.
Imae et al made a soluble construct of the bovine protein (95% identical to
human) starting at residue 45 (plus a 6-residue expression tag) and determined
three structures as PDB entries 7e9j, 7e9k and 7e9L.

For this tutorial we will use entry 7e9L, i.e. one of the structures of the
bovine protein.

Files: 
model: AF-Q8NAT1-F1-model_v1.pdb (human)
sequence of expression construct: POMGNT2_construct.seq
diffraction data: 7e9l.mtz
PAE matrix: AF-Q8NAT1-F1-predicted_aligned_error_v1.json
rendering of PAE matrix: AF-Q8NAT1_PAE.png

A Matthews volume calculation indicates that there are two copies in the
asymmetric unit. As well, there is a 12-residue mannosylated peptide and a UDP
ligand bound to each copy. 

The data extend to 2.1 Å resolution. Although models of both the human and
bovine versions of POMGnT2 are now available from the AlphaFold database at the
EBI, only the human version (95% identical to bovine) was available at the time
this tutorial was developed.

The PAE (predicted aligned error) matrix (in .json format) is very useful in
allowing you to automatically divide the model into domains. To see the
information contained in this matrix, take a look at the rendering in the .png
file.  Alternatively, you can interact with the PAE matrix on the AlphaFold
database site (https://alphafold.ebi.ac.uk/entry/Q8NAT1), drawing a box around
blocks of dark green to see what this corresponds to on the predicted structure.

Use process_predicted_model from the Phenix GUI (under the AlphaFold tab),
remembering to specify the PAE file in .json format as an optional input file.
This will convert the pLDDT values in the B-factor column into appropriate
B-values and automatically divide the model into domains, using information in
the PAE matrix. 

Examine the input and output PDB files in coot, ChimeraX or PyMol, coloring by
B-factor. Note that the highest confidence regions of the structure have large
pLDDT values; if these were interpreted as B-factors, they would get the least
weight in the MR calculations! After the model has been processed, they are
given low B-factors. In addition, the lowest-confidence residues are removed.

Consider which domains you should be searching for in the MR job, remembering
the protein construct used for this crystal. Phaser requires separate files for
each separate domain you are searching for. For example, the second domain of
chain A of the AlphaFold model will be called chain A2 of the processed model.

Phaser needs to have an estimate for coordinate error. Experience so far
suggests that an estimated overall RMSD of 0.8-1.2A works well for AlphaFold
models; for this case, the choice of 1.0 will work well.

When you've solved the structure, you might want to look for evidence of the
peptide and UDP ligands!

Citations: Yang et al., https://doi.org/10.1107/S2059798321001261
Imae et al., https://doi.org/10.1111/gtc.12853

