#!/bin/csh -f

# run.csh

# This example is density modification of a cryo-EM map
# The data are half-maps for apoferritin (EMD entry 20026, 1.8 A)

# The files are:

# emd_20026_half_map_1_box.ccp4 emd_20026_half_map_2_box.ccp4 -- half maps
# seq.dat -- sequence file
# 20026_auto_sharpen_A.ccp4 -- deposited map, half-map sharpened
# apoferritin_chainA_rsr.pdb -- model refined against sharpened map

# The maps are all cut out (boxed) from the deposited maps (just to make the
# files smaller for this demo).

# You can set the number of processors (nproc) here:
set nproc=4

echo "RUNNING DENSITY MODIFICATION AND REFINEMENT WITH NPROC=$nproc"

# A. DENSITY MODIFICATION OF HALF-MAPS

# To run one cycle of density modification from the command line:

phenix.resolve_cryo_em emd_20026_half_map_1_box.ccp4 \
     emd_20026_half_map_2_box.ccp4 \
    resolution=2.2 dm_resolution=1.8 \
    seq_file=seq.dat \
    cycles=1 \
    nproc=$nproc \
    box_before_analysis=False \
    density_modify_unsharpened_maps=True\
    output_directory="." \
    initial_map_file_name=initial_map.ccp4

   
# You might want to add the following to improve the final map a little:
# real_space_weighting=True
# sigma_weighting=True

# In about 15 minutes this will produce the file:
# denmod_map.ccp4  -- density-modified map

# You may want to cut out the parts of your map that match apoferritin_chainA_rsr.pdb:

phenix.map_box ignore_symmetry_conflicts=true \
     apoferritin_chainA_rsr.pdb denmod_map.ccp4 prefix=denmod_map_A

# We are saying "ignore_symmetry_conflicts=True" because the map in 
# denmod_map.ccp4 may have a different "unit cell" than 
# apoferritin_chainA_rsr.pdb.  Here the "unit cell" is just a description of
# the box of points that are in the map. It can sometimes refer to the size
# of the original map and sometimes to the size of the working part of the
# map and this command says to ignore these differences.

# In Coot or Chimera you can load in the model (apoferritin_chainA_rsr.pdb) 
# and compare the sharpened deposited map (20026_auto_sharpen_A.ccp4) 
# with the density modified map (denmod_map_A.ccp4). Look at residues 13 
# and 81 and the main chain near residue 161 for examples of substantial 
# improvement in the map.

ls -ltr denmod_map.ccp4 denmod_map_A.ccp4
