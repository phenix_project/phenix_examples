This example is density modification of a cryo-EM map.

The data are half-maps for apoferritin (EMD entry 20026, 1.8 A)

The files are:

emd_20026_half_map_1_box.ccp4 emd_20026_half_map_2_box.ccp4 -- half maps
seq.dat -- sequence file
20026_auto_sharpen_A.ccp4 -- deposited map, half-map sharpened
apoferritin_chainA_rsr.pdb -- model refined against sharpened map

The maps are all cut out (boxed) from the deposited maps (just to make the
files smaller for this demo).

===========================================================================
DENSITY MODIFICATION TUTORIAL (GUI, 15 minute run time)
===========================================================================

This tutorial will carry out density modification using 1.8 A data on 
human apoferritin.  It is designed to be run from the Phenix GUI

Set up tutorial data
====================
In the Phenix GUI:
New project/Set up tutorial data/Select a dataset
Choose: human apo-ferritin (Density modification, model-building and refinement at 1.8 A)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Examine sharpened deposited map
===============================

Select Coot at top of GUI window, then select 
   File/Open coordinates/apoferritin_chainA_rsr.pdb
 Then in Coot select File/Open map/emd-200026_auto_sharpen_A.ccp4
In Coot choose Dispay Manager andselect Properties for the map.  Then
  Set the level: 5 and choose "rmsd".  Then set Change by rmsd: 0.3. Then hit OK

Look at Tyr81 (Draw/GotoAtom/Residue number:81) and
His13 (side-chain) and Glu162 (main-chain) density

Run density modification
========================

Phenix GUI: Select Cryo-EM/Map improvement and choose: ResolveCryoEM
Using the Add File button, load the half maps and sequence file:
Half map file 1: emd_20026_half_map_1_box.ccp4
Half map file 2: emd_20026_half_map_2_box.ccp4
Seq file:                            seq.dat

A title and resolution of 1.97 will appear.

Hit Run to start density modification.

When density modification is complete, you can view the density-modified 
map superimposed on your sharpened starting map by clicking on 
the "View in Coot" button.

Look at Tyr81 and His13 (side-chain) and Glu162 (main-chain) density.
You will see that the density-modified map from ResolveCryoEM is more clear than
the sharpened deposited map.


===========================================================================
DENSITY MODIFICATION TUTORIAL (COMMAND-LINE)
===========================================================================

A. DENSITY MODIFICATION OF HALF-MAPS

To run one cycle of density modification from the command line:

phenix.resolve_cryo_em emd_20026_half_map_1_box.ccp4 \
    emd_20026_half_map_2_box.ccp4 \
   resolution=2.2 dm_resolution=1.8 \
   seq_file=seq.dat \
   cycles=1 \ 
   nproc=4 \
   box_before_analysis=False \
   density_modify_unsharpened_maps=True\
   initial_map_file_name=initial_map.ccp4

  
You might want to add the following to improve the final map a little:
real_space_weighting=True
sigma_weighting=True

In about 15 minutes this will produce (in a resolve_cryo_em_xx directory)
the file:

denmod_map.ccp4  -- density-modified map

You may want to cut out the parts of your map that match apoferritin_chainA_rsr.pdb:

phenix.map_box apoferritin_chainA_rsr.pdb denmod_map.ccp4 prefix=denmod_map_A

In Coot or Chimera you can load in the model (apoferritin_chainA_rsr.pdb) 
and compare the sharpened deposited map (20026_auto_sharpen_A.ccp4) 
with the density modified map (denmod_map_A.ccp4). Look at residues 13 
and 81 and the main chain near residue 161 for examples of substantial 
improvement in the map.

