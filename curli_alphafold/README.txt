AlphaFold modeling and rebuilding (Cryo-EM)

This example is using the Phenix PredictModel tool to model the
E.coli curli complex in PDB entry 7brm and EMDB entry 30160.
cut out one chain to make the map small.

The files are:

curli.seq  -- sequence of the receptor binding domain
7brm_30160_curli.ccp4 -- map cut out from EMD 30160 with curli protein 

Additional files for reference:
7brm_30160_alphafold.pdb -- An example of an AlphaFold model that you will get
7brm_30160_curli.pdb -- The deposited model of the receptor binding domain


===========================================================================
===========================================================================

This tutorial will carry out AlphaFold modeling and rebuilding using
the E. coli curli protein and a map at a resolution of 3.6 A.
It is designed to be run from the Phenix GUI and using Google Colab.

Set up tutorial data
====================

In the Phenix GUI:
New project/Set up tutorial data/Select a dataset
Choose: CRYO-EM/Dock and rebuild AlphaFold model (curli complex with alphafold) 

Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Examine deposited map and model
===============================

Have a look at the map we are working with and the model we would like to get.

Select Coot in the Phenix GUI (top of page)
Then select: File/Open coordinates/7brm_30160_curli.pdb
             File/Open map/7brm_30160_curli.ccp4
             Dispay Manager/select Properties for the map/Set level: 2
               rmsd/Change by rmsd: 0.2/ then Apply and OK

Have a look at the model and see if the map is clear or not. (Should be pretty 
good).


ALPHAFOLD MODELING  (10 minutes using PredictModel)
===================================================

In the Phenix GUI select AlphaFold (Predicted Models) and then 
"AlphaFold Model Prediction"

Select "Add file" and choose "curli.seq".  This should set up a job title and
load the sequence in.

In the tab "Prediction Settings", uncheck the box "Use templates from PDB" 
just so that in this example we predict without using the answer.

Hit Run to run the prediction.  A new tab "Prediction cycle 1" will come up
and the Phenix AlphaFold server will work on your prediction. After about 10 
minutes a model should be obtained. You can use the "Server" button at the
upper right of the Phenix GUI to see what te server is doing.

When the prediction is finished you should see some graphs indicating the
quality of the prediction. Look at the predicted confidence (plDDT) by residue
number.  You can see that some parts are better-predicted than others. The
average pLDDT is about 85, a moderate confidence.

Examine the AlphaFold model
===========================

In the PredictModel GUI, select "Open in Coot"

This will open your AlphaFold model.

Just for comparison, let's superimpose the AlphaFold model on the deposited
model. In Coot select "File", then Open Coordiates, and select
7brm_30160_curli.pdb.

Now in your Coot window select:

Calculate ... then SSM Superpose
Set Reference Structure to 7brm_30160_curli.pdb
Set Moving Structure to your model, 
PredictAndBuild_1_ALPHAFOLD_A_7brm_30160_curli_Chain=A_MODEL.pdb 

hit Apply to superimpose. You may need to change the view to see the 
superimposed models.  You can do this with the "Goto Atom" button in
Coot (looks like a yellow ball on a horizontal green stick).


Your AlphaFold model should now be (more or less) superposed on the deposited
structure. Have a look at the models and the map.  You should see that part of 
the AlphaFold model superimposes very closely on the map and deposited model,
and part does not.  Now click on some of the atoms in the AlphaFold model. 
Their B-values reflect the confidence in that residue (plDDT values) on a scale
of 0 to 100, where 70 means fairly confident and 90 means very confident. Notice
that the plDDT values are generally lower in regions that disagree with the 
map and deposited model.

Rebuild the AlphaFold model using the cryo-EM map (15 min)
===========================================================

Now we can use the map and the AlphaFold model with its confidence measures
to rebuild the model.

Let's dock this model in your map and rebuild it.

In the GUI under CryoEM/Docking and model rebuilding, select 
"PredictAndBuild (Cryo-EM)"

Select Add file, and load your model from your PredictModel 
directory.  

Now add your sequence file and map files:
  curli.seq
  7brm_30160_curli.ccp4 

Make sure the "data type" for the map is "Map file"  (not half-map)
..and set the resolution (below the input box) to 3.6 (A).

You are ready to rebuild the model.  Hit Run to start.


The steps carreid out are: 
  trimming (remove poorly-predicted regions)
  docking (find helices and strands in map and superpose trimmed model on them)
  morphing (superpose the full AlphaFold model on the docked trimmed model and
    morph full model if necessary)
  refinement (refine docked morphed model)
  loop fitting (rebuild poorly-predicted and poorly-fitting regions)
  model reassembly (put together all the best parts)
  final refinement (refine the assembled model)

Once a full cycle is carried out, PredictAndBuild will run AlphaFold again,
using the new model as a template.  This process is iterated until
the model does not change any more. During each cycle the average confidence
(pLDDT) should increase a little.

While it is running you can load the intermediate files (docked, morphed, 
refined, etc) and see what is happening.

When it is done you can hit "open in coot" or "open in ChimeraX" 
and the rebuilt model should be
shown along with the map.  Compare the rebuilt model with the superposed
AlphaFold model.  You should find that the rebuilt model has moved even the 
better parts of the model and that the rebuilt model matches the deposited 
model quite well except for the N-terminus which did not get rebuilt well
in this example.
