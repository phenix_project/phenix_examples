Tutorial on restraints for low resolution refinement.

Purpose:
- Demonstrate the use of secondary structure restraint
- Demonstrate the use of reference model restraints
- Demonstrate the use of NCS restraints
- Demonstrate the use of Ramachandran restraints

Restraints for low resolution are very similar for X-ray and Cryo-EM refinement. This tutorial is based on X-ray data.

Files in this tutorial:

1jkt.pdb - low-resolution (3.5A) model from PDB, contains 2 similar chains.
1jkt-sf.mtz - experimental data, obtained by running phenix.cif_as_mtz on 1jkt-sf.cif (file from PDB).
4pf4.pdb - high-resolution (1.1A) model of the same protein from PDB, contains 1 chain.
curated_ss.param - manually curated Secondary structure parameters

illustrations/
res32-33.cxs, helix101-107.cxs - ChimeraX scenes for illustrations
effect_of_rama_restraints.pdf - Comparama plot for Ramachandran restraints refinement
1jkt_chainA.pdb - chain A of 1jkt
1jkt_chainB.pdb_fitted.pdb - chain B of 1jkt superposed on chain A
4pf4.pdb_fitted.pdb - High-resolution 4pf4 model superposed on chain A of 1jkt.


Chapter 0. No restraints at all. Just defaults.
===============================================
Setup and run phenix.refine with 1jkt.pdb and 1jkt-sf.mtz.

Stats before refinement:
Rwork 0.2732
Rfree 0.3563
Clashscore  94.97
Rama outliers  16.42
Rama favored   61.13
Rama-Z   -7.21

Stats after refinement:
Rwork 0.2374
Rfree 0.3515
Clashscore  36.69
Rama outliers  6.93
Rama favored   74.09
Rama-Z   -6.10


Chapter 1. Secondary Structure.
===============================
Episode 1.
----------
Load 1jkt.pdb, 1jkt-sf.mtz to phenix.refine GUI. Select “Secondary structure” restraints. Optionally in “Select atoms” run SS finder, observe different annotations for chains A and B, 3-10 helices 3 residues long etc. Run default refinement with default SS on. Observe “removed outlier” records in .log file part related to establishing SS.

Stats after refinement:
Rwork 0.2388
Rfree 0.3501
Clashscore  32.89
Rama outliers  6.75
Rama favored   75.18
Rama-Z   -6.06

Episode 2.
----------

Load 4pf4.pdb, select “Secondary structure” restraints, run finding SS ignoring annotations. Optionally could use phenix.real_space_refine, we won’t actually run refinement here. Observe useless short helices there as well. Edit SS - remove extras, maybe duplicate and edit couple helices. Export them as a file.
Afterwards use supplied curated_ss.param.

Episode 3.
----------

Load 1jkt.pdb, 1jkt-sf.mtz, curated_ss.param to phenix.refine GUI. Select “Secondary structure restraints”. Run refinement.
Note lower Ramachandran outliers.

Stats after refinement:
Rwork 0.2376
Rfree 0.3497
Clashscore  34.23
Rama outliers  5.66
Rama favored   76.64
Rama-Z   -6.25

Episode 4. Keep outliers.
-------------------------

Same as 3, but without outlier rejection.
Show “All parameters” -> “search parameters” -> “outlier”: “filter out h-bond outliers in SS”. Run refinement.
Note increased Ramachandran outliers and clashscore compared to #1-3. Also could note increased amount of SS h-bonds.

Stats after refinement:
Rwork 0.2469
Rfree 0.3504
Clashscore  38.14
Rama outliers  8.03
Rama favored   72.45
Rama-Z   -6.18


Chapter 2. Reference Model restraints.
======================================
Episode 1.
----------

Load 1jkt.pdb, 1jkt-sf.mtz, 4pf4.pdb into phenix.refine GUI. Assign 4pf4 as reference model. Make sure space group is P4, as in 1jkt, not P 212121 as in 4pf4. Click the checkbox “Reference Model”. Run refinement. Note the part of the log regarding reference model restraints, in particular matching residues. Matching makes sense.
Compare results of refinement with #1-1 and #1-3. Observe the difference in:
Lower Rwork, Rfree, Ramachandran outliers.

Observe the look of helix 101-107 in ChimeraX.
illustrations/helix100-108.cxs: chains A and B and high-resolution model superimposed.

Stats after refinement:
Rwork 0.2454
Rfree 0.3400
Clashscore  2.74
Rama outliers  4.74
Rama favored   86.31
Rama-Z   -3.04

Episode 2.
----------

Same as above but adding proper SS from high_resolution_ss.eff.
Note slightly better stats Rfree, Ramachandran outliers.

Stats after refinement:
Rwork 0.2429
Rfree 0.3394
Clashscore  27.63
Rama outliers  4.38
Rama favored   86.86
Rama-Z   -3.17



Chapter 3. NCS.
===============
Episode 1. Torsion.
-------------------

Setup refinement with Torsion NCS only. Observe matching of residues in the log.
Open results in Coot, there: Draw -> “NCS ghost control”: “Display non-crystallographic ghosts”, observe slight difference in positions.
Rfree is worse than #2-2 (reference model + SS)

Stats after refinement:
Rwork 0.2404
Rfree 0.3460
Clashscore  30.43
Rama outliers  4.38
Rama favored   80.47
Rama-Z   -6.10

Episode 2. Constraints.
-----------------------

Setup refinement with NCS constraints.
“Select atoms”-> “NCS” tab, “Find NCS”. Discuss parameters. Note “Residue match radius”. Leave defaults, click “OK”. Note residues 33, 48-50, 63, etc are excluded.
Look at illustrations/res32-33.cxs: Glu32 (Glutamic acid) is still close enough, but Lys33 (Lysine) diverge more so they are excluded.

Find NCS again with “Residue match radius” = 10. Observe nice “Chain A”, “Chain B” selections.  Save and close.

Run refinement.
Note worse Rfree, Ramachandran outliers, clashscore compared to #3-1.
Open results in Coot, there: Draw -> “NCS ghost control”: “Display non-crystallographic ghosts”, observe no difference in positions. One can see NCS ghost if zoom extremely closely, then it begins to separate from primary chain.

Stats after refinement:
Rwork 0.2437
Rfree 0.3619
Clashscore  40.60
Rama outliers  8.03
Rama favored   76.64
Rama-Z   -5.76

Chapter 4. Ramachandran restraints.
===================================

Setup refinement. Find and enable Ramachandran restraints. “Model interpretation” -> “Ramachandran restraints”. Discuss proper usage of Ramachandran restraints. This one is not - too many outliers in starting model, we’ll get weird Rama plot, they won’t be fixed properly, rather pushed into the closest favorable region.

Run refinement. Note that the number of Ramachandran outliers is the smallest across all previous refinements. Note elevated clashscore. Compare stats and Ramachandran plots from this refinement and #2-2 (reference model + SS). Note Rama-Z scores.

Check illustrations/effect_of_rama_restraints.pdf - comparama plot showing how residues moved on Ramachandran plot as a result of this refinement.

Stats after refinement:
Rwork 0.2391
Rfree 0.3520
Clashscore  34.00
Rama outliers  1.46
Rama favored   82.66
Rama-Z   -5.47

