Full text of this tutorial, with illustrations, can be found at
https://phenix-online.org/documentation/tutorials/molprobity.html.
Structure validation and correction is a highly visual process and the html
version of this document is strongly recommended.

That page also has instructions for a tutorial on general protein validation.
Please load "Protein Kinase A (validation)" tutorial data
for that tutorial.

Overview
--------

This tutorial will show you how to do comprehensive model validation for
*cis-peptides* within the PHENIX graphical user interface (GUI).

*Cis*-nonProlines Overview
--------------------------
You will need another set of tutorial data for this section.
In the main Phenix window, go to New Project -> Set up tutorial data ->
**"Validation - xyloglucanase cis peptides"**.

If you are starting from the main Phenix window, select
Comprehensive validation (X-ray/Neutron).
If you still have the comprehensive validation window open, return to the
**"Configure"** tab.
Select **2cn3.pdb** as the input model and **2cn3.mtz** as the reflections file.
We will not use a restraints file this time, so clear that field if necessary.
Now run the validation; this will take a few moments.

A type of outlier that can be very difficult for real-space refinement alone to
resolve is an incorrect *cis*-peptide.
The peptide bond between carbon and nitrogen joins adjacent amino acids.
Due to the nearby carbonyl, the peptide bond has partial double bond character
and does not rotate freely.
Most peptides are *trans*, but genuine *cis* peptides occur preceding 5% of
prolines and preceding about 1 in 3000 non-proline residues.
Despite its rarity, a *cis* conformation can be tempting to model at lower
resolutions because it may appear to fit constricted or patchy density.
Once modeled, *cis* peptides are difficult to escape, since doing do would
involve rotating 180 degrees through a high energetic barrier.
Fortunately, there is a tool in Coot to simplify human-directed corrections.

Once the validation completes, open the model in Coot.
Then move to the MolProbity tab of the validation output, and then the Protein
tab within MolProbity.
Scroll down to the bottom of the Protein tab to find
**Cis and Twisted peptides** validation.
Note the overall model statistics and the suspiciously high incidence of
non-*trans* peptides in this model.

Peptide bonds necessarily span two residues, so both are listed in the
validation output table.
However, in shorthand, peptide bonds are properly associated with their
following residue, due to the special relationship *cis* peptide bonds have with
a following proline.
Thus, "chain A GLY 161 to GLU 162" is a *cis*-Glu,
and "chain A, GLY 293 to PRO 294" is a *cis*-Pro.


Making Cis-peptide Corrections
------------------------------

This structure is a xyloglucanase.
Real *cis*-nonProline conformations are more common in carbohydrate-active
enzymes like this one than elsewhere.
As a result, this structure contains both real and incorrect *cis* peptides.


**Justified cis conformations**

We'll look at a real example first.
Click on **"GLY 161 to GLU 162"** in the table, which will center Coot on the
N atom of the peptide bond of interest.
Note that Coot marks *cis* peptides with a colored trapezoid (green for
*cis*-Proline, red for *cis*-nonProline, yellow for twisted).
Scroll the electron density to a higher contour to convince yourself that
*cis* truly is the best conformation to fit this region.
You need this kind of strong support - from experimental data, homology, or
chemistry - to justify *cis*-nonProlines.


**Mistaken cis conformations**

Now we will fix a mistake.
Click on **"LYS 269 to GLY 270"** to center the Coot window on this peptide
bond.
In addition to the red trapezoid marking the *cis*-peptide, there are large
areas of difference density around the peptide bond.
Also note the clash between the GLY 270 CA and the LYS 247 O.


To perform the correction: Go to **Calculate** -> **Other Modeling Tools**
from the Coot menu bar, and select **Cis <-> Trans**.
Click on one of the atoms in the offending peptide bond, and Coot will flip the
bond.
The bond conformation has now been corrected, but the atoms have moved out of
the density and geometry distortions have been introduced.
Select Real Space Refine Zone, and then select atoms on either side of the
peptide bond.
This correction involved a large change, so be sure to give Coot at least two
full residues on either side of the bond to work with.
Once you have obtained a satisfactory local refinement, note that the
GLY 270 N is now in position to form a hydrogen bond with the LYS 247 O,
where the clash was.

**ASN 298 to GLY 299** is another good example of a region with marginal
electron density and a mistaken *cis*-peptide.
Select it from the list and try fixing it.
As a bonus, you can then move the ASN 298 sidechain into some highly likely
positive difference density.


**Testing both cis and trans**

Finally, select **"MET 348 to ASN 349"** from the list.
This region has no significant difference density, and the 2Fo-Fc map is fairly
convincing.
Nevertheless, let's test it.
Try changing this peptide to *trans* and do a local refinement.


It's not a good fit to the density!
Convince yourself that there's no way to get even a marginal fit from the
*trans* conformation, then hit **"Undo"** until you get the *cis*-Asn back.
Trying both possibilities like this can be a useful strategy.
