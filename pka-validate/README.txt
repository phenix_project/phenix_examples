Full text of this tutorial, with illustrations, can be found at
https://phenix-online.org/documentation/tutorials/molprobity.html.
Structure validation and correction is a highly visual process and the html
version of this document is strongly recommended.

That page also has instructions for a tutorial on cis-peptides
Please load "Validation - xyloglucanase cis peptides" tutorial data
for that tutorial.

Overview
--------

This tutorial will show you how to do comprehensive model validation within the
PHENIX graphical user interface (GUI).


Setup
-----

The example used for this tutorial is called Protein Kinase A (3dnd).
To get the requisite files open the GUI and set up the tutorial,
called **Protein Kinase A (validation)**.
This should setup the main/GUI window.
On the right-hand side, under Crystals,
click **Validation and map-based comparisons**
and then select **Comprehensive Validation (MolProbity)**.


File inputs
-----------

In the "Comprehensive Validation"  window, browse to the tutorial directory
that you specified above, and select 3dnd.pdb as the "Input model",
3dnd.mtz as the "Reflections file",
and 3dnd.ligands.cif as the "Restraints (CIF)" file.
This CIF file defines restraints for ligands in input PDB file.

For this model, adding certain validations to the Polygon representation is
useful. Scroll down to find the button labeled "Polygon options" and click it.
In the window that pops up, turn on the checkboxes for "Ramachandran favored"
and "Rotamer outliers". Then hit OK.

Now you are ready to run the program, select **Run** from the top menu bar.

  .. _Validation tools:

Validation tools overview
-------------------------



Polygon
=======

When validation has run, click on the "Compare statistics" button.
This will launch a tool called "Polygon" (it may take several seconds),
which is used to simultaneously compare R-work, R-free, RMSD-bonds, RMSD-angles,
Clashscore, and Average B factor.

Well-built models will usually have a small, fairly equilateral polygon,
whereas larger or significantly asymmetric deviations are indicative of
model problems.

As you can see for 3dnd, both RMSD-bonds and RMSD-angles are a bit large,
beyond the expected peaks for structures of similar resolution.
This could indicate that misfit areas of the structure are causing
geometric strain. We will visit some of these areas in the next sections.

From the measures we added, you can also see that Rotamer outliers are high.
Ramachandran favored *looks* unusual, but this measure should have a
high value, unlike the others.
More importantly, this structure's Ramachandran favored score falls around
the peak of Polygon's histogram for structures of similar resolution.
This is important context for choosing which validations to focus on.


You may close the "POLYGON" window and return to the comprehensive validation
window.

Opening Coot
============

Click the "Open in Coot" button.  When Coot loads, note that it says
"Connected to PHENIX" in the toolbar - this allows the validation GUI to
communicate with Coot.

If Coot does not load, go to Preferences->Graphics in the main Phenix window
to specify a path to your Coot installation.

MolProbity Validation
=====================

Now select the "MolProbity" tab in the Phenix Comprehensive validation window.
Under this tab, you will see sub-tabs for "Summary", "Basic geometry",
"Protein", and "Clashes".
If your model contained RNA, there would also be an "RNA" tab.

**Summary**

Under the "Summary" tab are most of the same overall statistics that you would
find when running the MolProbity webserver.
These statistics are colored stoplight-style (green, yellow, and red) as a
rough guide the severity of a given result.

Notice the high percentage of rotamer outliers, and large number of
C-beta deviations (those combine geometry problems around the C-alpha into a
single measure, as explained in Lovell 2003).
The Ramachandran favored percent appears a little low, and is colored yellow.
However, as we saw in the Polygon above, this value is typical of structures
around 2.26Å.
The Ramachandran statistics might be improvable, but other validations take
higher priority in this context.

This tab also displays Rama-Z score validation.
This is a whole-model assessment of how realistic the Ramachandran distribution
is and guards against overfitting to Ramachandran criteria.
This structure has a reasonable Ramachandran distribution, with all its Rama-Z
scores falling within +2 to -2.


**Basic Geometry**

From the MolProbity tab, click on the "Basic geometry" tab.
Here you find a summary of all bond, angle, dihedral, chirality, and planarity
outliers.
Outliers will be listed in the associated lists, and each item is clickable,
which will center in Coot.
Note the bond outlier for Ile A 163 - we'll be seeing this residue again later.

**Protein-specific validations**

Next, go to the "Protein" tab.
This section contains validation information for Ramachandran and rotamer
outliers, C-beta deviations, recommended Asn/Gln/His sidechain flips (these
have NOT already been done for you, as you can tell if you click on His 39
in the flip list to see it in Coot), and non-*trans* peptide bonds.

**RNA-specific validations**

If this structure contained any RNA molecules, there would be an additional
tab named "RNA".
This section contains validation information for RNA nucleotides with covalent
geometry outliers (also found in the Basic geometry tab), RNA nucleotides with
sugar pucker outliers, and RNA suites with unexpected backbone conformations.

**All-atom Contacts**

Go to the "Clashes" tab. This section contains a list of all the
steric overlaps >0.4Å (i.e. "clashes") in the structure, sorted by severity.
Scroll down the list and note that almost all the clashes involve
at least one Hydrogen atom. Hydrogens form most of the steric contacts in a
structure and are crucial to contact analysis! Phenix, MolProbity, and Coot will
generally ensure that hydrogens are added when you need them, but make sure to
use phenix.reduce to add hydrogens manually if you are working outside these
systems.

  .. _General corrections:

Making General Corrections
--------------------------

Refinement programs are excellent at optimizing details, but may struggle with
large changes across energy barriers.
Our corrections are therefore focused on getting residues over energy barriers
and into the right local minimum.

  .. _Rotamers:

**Rotamers**

Select the Protein tab and find the list of rotamer outliers.
Click on **Leu 27** from the A chain, which will center on this residue's CA in
the Coot window.

As you can see in Coot, this orientation is not a terrible fit to the density
- but it is a rotamer outlier and energetically unfavorable due to an eclipsed
Chi angle.
You can see the eclipsed conformation by looking down the CG-CB bond and noting
that the CD1 atom nearly overlaps with the backbone CA.
Recentering on CB may help you arrange this viewpoint.
The residue also has a suggestive positive difference peak near the CD1.

We'll use the tools in Coot to fix this sidechain.
Select the **"Auto Fit Rotamer"** tool.
You can find it in the default toolbar or in Calculate-> Model/Fit/Refine.
It looks like a sidechain surrounded by green.
Then click on an atom in the Leu 27 sidechain.
Did you see it rotate ~180 degrees?

To confirm the correctness of this change, use the **"Undo"** and **"Redo"**
buttons to observe the change again.
Look down the CG-CB bond as before. You should see that the sidechain has a
favorable, staggered conformation after the change.
Also note that the CD2 atom is now pointed towards the positive difference
density peak - you've got the atom close enough to the correct position that
refinement should be able to sort out the details.


**C-Beta Deviations**

Return to the Validation GUI window, and find the list of C-beta position
outliers.
Select **Ile 163** from the A chain, which will center on this residue's CB atom
in the Coot window.
This sidechain also appears in the rotamer outliers list on this tab and on the
bond-length outliers list on the Basic Geometry tab.
Misfit sidechains will often have multiple diagnostic indicators of a problem,
which is useful in identifying the worst offenders.
Also note the large blob of positive density near to the sidechain, further
evidence that it may not be in an optimal position.

Select the **"Auto Fit Rotamer"** and then click an atom in the Ile 163
sidechain.
Alternatively, you can define a Real Space Refine Zone around this sidechain,
and drag the CG2 atom of the short arm into the large difference density peak.
Use the **"Undo"** and **"Redo"** to toggle back and forth across the change.
Note that the CA and especially the CB atoms have moved significantly.

Correcting misfit sidechains such as these can be tricky, as many rounds of
refinement within the wrong local minimum have caused distortions in the model
to accommodate the misfit.
It would be important to revisit this region after another refinement.
In this case, you can expect to find position of this Ile further improved,
as the neighboring atoms are able to recover from the strain caused by the
initial outlier.


**N/Q/H Flips**

Return to the Validation GUI window, and find the list of Backwards Asn/Gln/His
sidechains.

Asparagine, Glutamine, and Histidine sidechains are pseudo symmetric.
They can easily be fit into electron density backwards, eliminating hydrogen
bonds and introducing clashes.
Hydrogen addition via Reduce (in Phenix, Coot, or MolProbity) will usually
"flip" these N/Q/H residues to the positions with best steric contacts.
Here, you can use Coot to force a necessary Histidine flip.

Select **HIS 39** from the A chain, which will center on this residue in the
Coot window.
Note that the CD2 atom of His 39 and the N atom of Asp 41 are involved in a
serious clash.
If we flipped the His 180°, then the ND1 atom would be in a position to form a
hydrogen bond with the backbone NH where this clash is.

Define a Real Space Refine Zone that includes HIS 39.
In the refinement menu that pops up, look under "Active Refinement" and click
the **"Side-chain Flip 180°"** button.
(This flip only works on N/Q/H residues; use the Auto Fit Rotamer tool for
other tasks.)


**Clashes**

Return to the Validation GUI window, and navigate to the Clash tab.

Steric clashes usually serve as emphasis on other model problems.
However, there are some errors that are clearly diagnosed by clashes alone, such
as N/Q/H flips and certain misplaced water molecules.
There are many tools for identifying misplaced waters, and clashes are excellent
for finding the most problematic ones.

Select **"A 177 GLN HG3 ___ A 554 HOH O"** from the list of clashes, which will
center on this clash in the Coot window.
Scroll the 2Fo-Fc map contour down, and note that this water has only marginal
density even at a low contour.
There's no justification for keeping a water in this position.

To remove this water, select the **"Delete item"** tool.
In the Coot sidebar, it looks like a trash can.
In the Delete item tool, toggle the Water mode, then click on the offending
water.

The next water clash on the list, **"A 275 VAL HG21 ___ A 577 HOH O"**, is
another good example of an unjustified water marked by a clash.
Try using Coot to remove it as well.


**Notes**

The kind of sidechain fixups you've done in KiNG or Coot can mostly be
accomplished using real-space refinement in phenix.refine (which includes a
rotamer correction component), up to somewhere between 2 and 2.5 Å (and of
course NQH flips are done automatically by Reduce in either MolProbity or
Phenix).
At lower resolution, however, real-space refinement with rotamer correction
cannot reliably discern between correct and incorrect rotamers.
That's a hard job for people as well, but can often be done if you have the
interactive information on clashes and H-bonds from the non-pairwise, H-aware
all-atom-contact dots.

You can correct many of the other outliers in the GUI lists, especially the
sidechains. Try a few!


Notes for example 'pka-validate':
--------------------------------

This structure (3DND) is intended for use with the validation GUI in PHENIX.
tool in the PHENIX GUI.

Validating this model illustrates a variety of model errors, including
rotamer outliers (LEU 27), C-beta deviations (ILE 163), and easily
correctable steric clashes (HIS 39).

References:
-----------

Orts J, Tuma J, Reese M, Grimm SK, Monecke P, Bartoschek S, Schiffer A, Wendt
KU, Griesinger C, Carlomagno T. Crystallography-independent determination of
ligand binding modes. Angew Chem Int Ed Engl. 2008;47(40):7736-40.
PubMed PMID: 18767090; PDB IDs 3dnd

Structure validation by Calpha geometry: phi,psi and Cbeta deviation.
S.C. Lovell, I.W. Davis, W.B. Arendall, P.I. de Bakker, J.M. Word,
M.G. Prisant, J.S. Richardson, and D.C. Richardson. Proteins 50, 437-50 (2003).

MolProbity: More and better reference data for improved all-atom structure
validation. C.J. Williams, B.J. Hintze, J.J. Headd, N.W. Moriarty, V.B. Chen,
S. Jain, S.M. Prisant MG Lewis, L.L. Videau, D.A. Keedy, L.N. Deis,
I.I.I. Arendall WB, V. Verma, J.S. Snoeyink, P.D. Adams, S.C. Lovell,
J.S. Richardson, and D.C. Richardson. Protein Science 27, 293-315 (2018).

Crystallographic model quality at a glance. L. Urzhumtseva, P.V. Afonine,
P.D. Adams, and A. Urzhumtsev. Acta Cryst. D65, 297-300 (2009).
