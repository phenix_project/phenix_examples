This tutorial contains low-resolution data for demonstrating scale_and_merge

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: NS1 low-resolution SAD data (Scaling multiple datasets with scale_and_merge)
Hit OK once or twice to set up tutorial

On left panel of Phenix GUI hit "last modified" once or twice to
show the tutorial with a check mark next to it.

Run ScaleAndMerge:

In the Phenix GUI:
Select Crystals/Data analysis and manipulation and choose Scale and Merge
Add a title: Scaling data with Scale and Merge
Select a directory with the data:  Hit Browse and then just Open to select
the directory containing: a.sca b.sca c.sca d.sca

Set the unit_cell: 167.464 167.464  93.388  90.000  90.000 120.000

Note that the data files are scalepack unmerged and do not contain the
unit cell information.

Hit Run to scale and merge the data.
After a few seconds the Results panel will show the anomalous correlation
in shells of resolution.  The scaled data will be in ScaleAndMerge_1/scaled_data.mtz.


