#!/bin/sh
echo "Running AutoBuild on a2u-globulin MR data "
phenix.autobuild mup_mr_solution.pdb a2u-sigmaa.mtz a2u-globulin.seq  \
input_map_file=a2u-sigmaa.mtz input_map_labels="FWT PHIC"

