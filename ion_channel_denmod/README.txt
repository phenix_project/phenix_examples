This example is density modification of a cryo-EM map.
The data are half-maps for a human CLC-1 chloride ion channel, 
transmembrane domain (PDB entry 6coy EMD entry 7544, 3.36 A). 

The files are:

half_map_1_box.ccp4,half_map_2_box.ccp4  -- half maps 
6coy.fasta  -- sequence file
6coy.pdb  -- known model
full_map_box.ccp4 -- full reconstruction

The maps are all cut out (boxed) from the deposited maps (just to make the
files smaller for this demo).

Set up and run tutorial data
============================

In the Phenix GUI:
New project/Set up tutorial data/Select a dataset
Choose: Density modification (ion channel at 3.4 A)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Select the "Add files" button and load the two half maps and sequence file.

Hit Run and a density-modified map will be calculated.
Also a local resolution map showing the resolution of the sharpened map will
be generated.

Output maps:
initial_map.ccp4        -- starting average map (sharpened)
denmod_map.ccp4         -- density-modified map
denmod_half_map_1.ccp4        -- density-modified half-map
denmod_half_map_2.ccp4        -- density-modified half-map
denmod_resolution_map.ccp4    -- local resolution map

The density-modified map is your improved map. The local
resolution map is a map that shows the resolution at each point in the map.
It is normally used to color the sharpened map.

If you open in ChimeraX the sharpened map will be displayed, colored by
local resolution.  If you want to change the coloring scheme, you can edit the
command shown in the GUI window and paste it into the ChimeraX command box. You
can also edit this command to color other maps you load into ChimeraX.



COMMAND LINE USAGE:
===================

To run one cycle of density modification from the command line:

phenix.resolve_cryo_em half_map_1_box.ccp4 half_map_2_box.ccp4 \
   seq_file=6coy.fasta cycles=1 

In about 10 minutes this will produce the files:

denmod_map.ccp4  -- density-modified map
denmod_half_map_1.ccp4,denmod_half_map_2.ccp4 -- density-modified half-maps

In Coot you can load in the model (6coy.pdb) and compare the initial map
full_map_box.ccp4) and the density modified map (denmod_map.ccp4). Look
at residue 469 and see how it is better-defined in the density modified map.
