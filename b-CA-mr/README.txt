Carbonic anhydrase is an enzyme that assists rapid inter-conversion of carbon
dioxide and water into carbonic acid, protons and bicarbonate ions to aid
removal of carbon dioxide from the blood in respiration. This ancient enzyme
has three distinct classes: alpha, beta and gamma. Carbonic anhydrase from
mammals belong to the alpha class, the plant enzymes belong to the beta class,
while the enzyme from methane-producing thermophilic bacteria forms the gamma
class. Members of these different classes share very little sequence or
structural similarity. The alpha enzyme is a monomer and the gamma enzyme is
trimeric. The beta enzyme can be a dimer, tetramer, hexamer or octamer.
Haemophilus influenzae β-carbonic anhydrase (HICA,2a8d) is an allosteric
protein.

Model file:   AF-P45148-F1-model_v4.pdb (AlphaFold model)
Target files: 2a8d.mtz, 2a8d.seq (H.influenzae b-CA)
PAE files: AF-P45148-F1-predicted_aligned_error_v4.json
           AF-P45148_PAE.png

The beta-Carbonic Anhydrase (b-CA-MR) tutorial demonstrates a search for
multiple copies of a model in Phaser, when the number of copies is not
precisely known. The search takes advantage of the amalgamation algorithm,
which amalgamates solutions with high Z-score to add more than one copy of an
ensemble for each translation function.

Start by using a tool like phenix.xtriage to evaluate the possible numbers of
copies in the asymmetric unit of the crystal, according to a Matthews volume
calculation. How many copies would make sense for the possible quaternary
structures? How does the crystal symmetry (space group C2) influence your
reasoning?

Prepare the AlphaFold model for MR, using process_predicted_model.

Search for an appropriate number of copies. A good strategy is to start with
one of the smaller likely numbers, then consider adding more copies later; if
you search for too many copies, Phaser will spend a lot of time trying to add
the last one or more.

Inspect the solution, in light of the possible symmetries, to see if the
crystal packing makes sense and whether the quaternary structure has been
determined.  Look at the conformation and position of the N-terminus, then
think about how that can be reconciled with the PAE matrix. It may be relevant
that related structures were present in the data used to train AlphaFold.

REFERENCE

Cronk, JD, Rowlett, RS, Zhang, KYJ, Tu, C, Endrizzi, JA, Lee, J,
Gareiss, PC, Preiss, JR.  Identification of a novel noncatalytic
bicarbonate binding site in eubacterial beta-carbonic anhydrase. 2006.
Biochemistry 45: 4351-4361.
