
TUTORIAL INFO

The p9-build tutorial demonstrates model-building with high-resolution data.
Data file, file with map coefficients, and sequence are required.
The data extend to 1.7A resolution.

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: P. aerophilum translation-initiation factor 5a (building from experimental phases and hires data)

Hit OK once or twice to set up tutorial

On left panel of Phenix GUI hit "last modified" once or twice to
show the tutorial with a check mark next to it.

Files:

seq.dat:   The sequence of the protein 
p9_hires.mtz:  High-resolution data (1.74 A)
p9_data_and_phases.mtz:  Lower-resolution data plus map coefficients (3 A)

In GUI:
Select Crystals, then Model-building, then AutoBuild 

Put in a title:  "P9 high resolution model-building"

Hit the Add File button and choose these files to load, setting their file types as
below (Note: after loading a file, select it, then hit "modify data type" to toggle
what data type or types go with this file):

  seq.dat   (sequence)
  p9_hires.mtz  (high-resolution data)
  p9_data_and_phases.mtz (experimental data, initial map)


Now your parameters are set.

Hit "Run"  to start
After a couple hours the job should finish and you can use Coot to look at
the model that is produced.  It should be quite complete and have a very good
electron density map.

REFERENCE

Peat TS, Newman J, Waldo GS, Berendzen J, Terwilliger TC. Structure of
translation initiation factor 5A from Pyrobaculum aerophilum at 1.75 A
resolution. Structure. 1998 Sep 15;6(9):1207-14. PubMed PMID: 9753699.

PDB: http://www.rcsb.org/pdb/explore/explore.do?structureId=1BKB
