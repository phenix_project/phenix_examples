PredictAndBuild X-ray tutorial with AlphaFold predicted bromodomain 

PDB entry 7qz0 (BAZ2A bromodomain with acetylpyrrole derivative compound 83)

In this example you will use PredictAndBuild to generate a model 
for the BAZ2A bromodomain.  PredictAndBuild will use the Phenix
AlphaFold server to predict the structure of the bromodomain, then it will
trim the predicted model, run molecular replacement to place it, 
morph the predicted model on the molecular replacement solution,
and refine the resulting model.

You are going to supply a sequence file (the sequence of the bromodomain)
and the X-ray data for this structure.

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: bromodomain (X-ray structure solution with PredictAndBuild)
Hit OK once or twice to set up tutorial

On left panel of Phenix GUI hit "last modified" once or twice to 
show the tutorial with a check mark next to it.

Files:

7qz0.fa:   The sequence of the bromodomain
7qz0.mtz:  X-ray data for the bromodomain with bound ligand
7qz0.pdb:  Deposited model for the bromodomain with bound ligand
7qz0_ligand.pdb:  The bound ligand


Note for running off-line:
==========================
If you want to run this tutorial off-line, the directory
Demo_AF_bromodomain_ligand_CarryOn contains the predicted models for this
structure. In the GUI you can check the "carry_on" button on the
Prediction and Building Settings to use these predicted models.

Run PredictAndBuild
===================

In GUI: 
Select AlphaFold (Predicted models...), then PredictAndBuild (Crystallography)

Select "Add files" and choose these files to load:
  7qz0.fa
  7qz0.mtz

Notice that when you load these files the "High-resolution limit" box
will be filled in automatically and a "job title" will be created for 
you.

On the second tab ("Prediction and Building Settings"), change the 
rebuilding strategy from Standard to Quick (the predicted model in 
this case is quite good, so rebuilding is not going to be necessary).

Now your parameters are set.

Hit "Run"  to start

When it finishes (5 - 10 minutes)... launch either Coot or Chimera 
and have a look at the overall_best model and map, and compare them to 
the 7qz0.pdb model of the bromodomain from the PDB.

Notice that there is some extra density in the electron density map that
you have created.  This density is not so clear, but it mostly matches the
ligand in the 7qz0.pdb deposited model.  

You can refine the model by clicking on the "Refine and validate" button.
Have a look at the refined model and density with Coot.

Note: your model may not superimpose on the deposited model, but they should
be equivalent using space-group symmetry.  You can superimpose your model or
your model with ligand on the deposited model with SuperposeModels in 
the Models tab of the GUI if you want.

You can fit the ligand 7qz0_ligand.pdb into the density if you want with
the LigandFit GUI.  In the Phenix GUI, go to Ligands, then select LigandFit.

Select the files like these in your Refine_2 (or similar) directory add
them with the 'Add files' button in the LigandFit window:

AF_bromodomain_ligand_terwill_16_refine_002.mtz (this contains
map coefficients and refinement data, be sure to select both of these with
"modify data type" in the GUI)

AF_bromodomain_ligand_terwill_16_refine_002.pdb (final model, no ligand)

Then find the ligand file in your project directory (AF_bromodomain_ligand):

7qz0_ligand.pdb

and add it to the input files (you can ignore the message that says a model is
already defined, but do set the data type to "Ligand")

You are ready to fit the ligand....hit Run and in a couple minutes it will
finish. You can open the resulting fitted ligand and the map coefficients file
in Coot and see how the ligand fit into the density.  (It should superimpose
on the deposited ligand quite closely). You may need to turn on symmetry in
Coot with Draw/Cell and symmetry/Symmetry On because the deposited model can
be in a different place in space than the version that you just created (they
will differ by allowed origin shifts).


-You can improve the density by running Resolve density modification in 
the Phenix GUI, supplying three files:

AF_bromodomain_ligand_terwill_16_refine_002.mtz (this contains
map coefficients and expermental data and refinement data, be sure to 
select all of these with "modify data type" in the GUI)

AF_bromodomain_ligand_terwill_16_refine_002.pdb (final model, no ligand)

7qz0.fa (sequence file)

Then run density modification to get a density-modified map in AutoBuild_run_xx
in the file: overall_best_denmod_map_coeffs.mtz.

Then you can use 
the LigandFit GUI to automatically fit the ligand using

overall_best_denmod_map_coeffs.mtz  (map coefficients)
AF_bromodomain_ligand_terwill_16_refine_002.pdb (final model, no ligand)
7qz0_ligand.pdb (ligand)

It should go into this density quite nicely, matching the deposited model.
