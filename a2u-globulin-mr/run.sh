#!/bin/sh
echo "Running AutoMR on a2u-globulin data without building..."
phenix.automr mup_search.pdb scale.mtz mass=18000. resolution=2.5 \
    component_type=protein RMS=1.0 sequence.dat copies=4  \
    space_group=p212121 \
    unit_cell="106.820   62.340  114.190  90.00  90.00  90.00" \
    rebuild_after_mr=False
