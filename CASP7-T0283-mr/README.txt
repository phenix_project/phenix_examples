
TUTORIAL INFO

In the CASP7 (Critical Assessment of Structure Prediction 7)
experiment, David Baker's group generated an exceptionally good ab
initio model of target T0283 (which was later deposited as PDB entry
2hh6).  It turned out to be possible to solve the structure with
several of the 5 ab initio models that were entered for CASP7 and then
to complete the structure with automated rebuilding software.

T0283TS020_1.pdb - T0283TS020_5.pdb: the 5 models entered for CASP7
2hh6_sigmaa.mtz: the diffraction data for the target
2hh6.seq: target sequence

REFERENCE

Qian B, Raman S, Das R, Bradley P, McCoy AJ, Read RJ & Baker D.
High-resolution structure prediction and the crystallographic phase
problem. 2007. Nature 450: 259-264.
