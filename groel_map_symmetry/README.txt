In this tutorial, we will find the symmetry in the emd_8750 map using phenix.map_symmetry.

Data and models:
Data directory:  $PHENIX/modules/phenix_examples/groel_map_symmetry

Map:  emd_8750.map -- cryo-EM map for groEL
Resolution: 4 A
Symmetry of map: D7



Set up tutorial data
====================

In the Phenix GUI:
New project/Set up tutorial data/Select a dataset
Under CRYO-EM, choose: Map symmetry (groEL at 4 A)

Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

In the GUI go to CRYO-EM/Map Analysis and manipulation and select Map Symmetry
Enter a title like: "GroEL map symmetry"
Select the map file "emd_8750.map"

Hit Run to find the symmetry of the map.  It should come back with D7 (7-fold
axis and a two-fold perpendicular to the 7-fold).  The name D7 (a) vs (b)
just specifies where the 2-fold is (along x or y). The 7-fold is always along
Z by convention.

The MapSymmetry tool produces a file "symmetry_from_map.ncs_spec" that 
records the symmetry operators found.  You can use this ".ncs_spec" file
to find the asymmetric part of a map or to apply symmetry to a monomeric
unit.

COMMAND-LINE PROCEDURE

 ----- Copying the data to a working directory ------

You can copy the data for this tutorial and get into the working directory with:

  cp -r $PHENIX/modules/phenix_examples/groel_map_symmetry .
  cd groel_map_symmetry

 ----- Finding symmetry in the emd_8750 map ------

You can type:

   phenix.map_symmetry emd_8750.map symmetry=D7

In a couple seconds this will produce a file containing the 14 symmetry 
operators for this map:

   symmetry_from_map.ncs_spec

This symmetry file can be used later to generate the 14-mer from any single 
chain.  It can also be used to identify the unique part of the emd_8750 map.
You can leave out the "symmetry=D7" and it will find the same answer but 
it will take a couple minutes to rule out all other symmetry possibilities.

