
This example is density modification of a cryo-EM map, followed by
optional structure determination by docking a homologous structure

The data are half-maps for apoferritin (EMD entry 20026, 1.8 A)




The files are (NOTE: the map files are in the apoferritin_denmod directory):

  - emd_20026_half_map_1_box.ccp4 emd_20026_half_map_2_box.ccp4 -- half maps
  - seq.dat -- sequence file
  - 20026_auto_sharpen_A.ccp4 -- deposited map, half-map sharpened
  - apoferritin_chainA_rsr.pdb -- model refined against sharpened map
  - 1aew_A.pdb -- chain A from X-ray structure of horse apoferritin (53% identical to human apoferritin)
  - run_denmod_dock.csh  -- command file that will run all these things for you if you want

The maps are all cut out (boxed) from the deposited maps (just to make the
files smaller for this demo).


Set up tutorial data
====================
In the Phenix GUI:
New project/Set up tutorial data/Select a dataset and choose: 
   "Density modification, docking, loop fitting and refinement"
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.


A. DENSITY MODIFICATION OF HALF-MAPS (20 min)

Let's run one cycle of density modification.


Phenix GUI: Select Cryo-EM/Map improvement and choose: ResolveCryoEM
Using the Add File button, load the half maps and sequence file:
Half map file 1: emd_20026_half_map_1_box.ccp4
Half map file 2: emd_20026_half_map_2_box.ccp4
Seq file:  seq_unique.dat

A title and resolution of 1.97 will appear.

Hit Run to start density modification.

When density modification is complete, you can view the density-modified
map superimposed on your sharpened starting map by clicking on
the "Open in ChimeraX" button.

Look at Tyr81 and His13 (side-chain) and Glu162 (main-chain) density.
You will see that the density-modified map from ResolveCryoEM is more clear than
the sharpened deposited map.

B. Cut out part of density-modified map matching deposited model:

You may want to cut out the parts of your map that match apoferritin_chainA_rsr.pdb:

Cut out map around supplied model
=================================

Select Cryo-EM/Map Box
Title: Box density-modified map around supplied model
Map file:    ResolveCryoEM_1/denmod_map.ccp4
Model file:  apoferritin_chainA_rsr.pdb
Output file name prefix:  denmod_map_box
Mask atoms:  True (check box)

Hit Run to box your map and model.  These boxed version will superimpose on
the originals; the map is just much smaller now.

In Coot or ChimeraX you can load in the model (apoferritin_chainA_rsr.pdb) 
and compare the sharpened deposited map (20026_auto_sharpen_A.ccp4) 
with the density modified map (denmod_map_A.ccp4). Look at residues 13 
and 81 and the main chain near residue 161 for examples of substantial 
improvement in the map.

C. STRUCTURE SOLUTION BY DOCKING HOMOLOGOUS MODEL 
   (horse apoferritin, 53% identical)

The file 1aew_A.pdb contains the X-ray structure of 1aew, a homologous protein 
from horse (53% identical, rmsd 0.5 A).  We are going to dock this structure 
into our density-modified map, refine it, fix the sequence, refine again,
and density modify using the model to get a final map.

1. Getting map symmetry
We will want a symmetry file describing the symmetry operators that have been
used in our map.  We can get them automatically in about 30 seconds using
the MapSymmetry tool.

In the GUI, select Cryo-EM/Map analysis and manipulation/MapSymmetry

Add a title: "Map symmetry of denmod_map.ccp4"
Load the map file (ResolveCryoEM_1/denmod_map.ccp4)
and hit Run to find the symmetry.

It should find the symmetry O (a) with 24 molecules in the symmetry group.
The "a" is just whether the 2-fold axis is along x or y.  You should also
now have a symmetry_from_map.ncs_spec file that has the symmetry information 
which you can use later in this tutorial.
 

2. Docking into boxed density-modified map

We can find where 1aew_A.pdb fits in our boxed map with dock_in_map:

Phenix GUI: Select Cryo-EM/Docking and Model-building/Dock in map
Set these parameters:
Title:  Place chain A of 1aew in denmod_map_box.ccp4 
Map file: MapBox/denmod_map.ccp4
Search model:  1aew_A.pdb
Resolution: 3
Number of processors: 4

Hit Run to run the job.

In a minute or two you should get the model placed_model.pdb.  Notice that 
a lot of the side chains do not match the density because the model is 
from a homologous structure.

4.  Refine the model against the density-modified map

We can refine the docked model either against the original or cut-out map. Let's
use the cut-out map.  We can also refine just chain A or the whole 24-mer. 
The difference is if you use the whole 24-mer you are going to include
inter-chain contacts and if you use a single chain you will ignore them. Let's
use just one chain for now:

Phenix GUI:
Select Cryo-EM/Real Space Refinement and choose Real Space Refinement
Set the title: "Refinement of 1aew chain A docked into density-modified map"
Add the map:  MapBox_2/denmod_map_box.ccp4
Add the docked model: DockInMap_4/placed_model.pdb
Set the resolution: 2 (A)

Hit Run to refine the model

After a couple minutes we have the output model: 
placed_model_real_space_refined_000.pdb.  If you look at 
this model in Coot or Chimera you'll see it has moved some side chains into 
density, but that they still don't match the density because the 
sequence of this docked model is not the sequence of human apoferritin 
(look at R29 for example which should be a tyrosine).

5. Fix the sequence 

We can redo the sequence of our docked model with sequence_from_map:

In the Phenix GUI:
Select CryoEM/Docking and Model building and choose Sequence from Map
Enter a title: "Fixing sequence of docked chain A of 1aew"
Enter the map file: MapBox_2/denmod_map_box.ccp4
and the refined docked model: RealSpaceRefine_5/placed_model_real_space_refined_005.pdb
and the sequence file: seq_unique.dat
Set the resolution: 2 (A)

and hit Run to replace side chains based on the sequence file.

In a couple minutes you should get the file: sequence_from_map.pdb with
the sequence of human apoferritin mapped onto the real-space-refined model.
Have a look at this with the map now in Coot or Chimera. You can see that
some of the side chains need refinement and that a loop by P157 needs to be
moved. Let's first just refine our new model.

6. Refine model with new sequence

Phenix GUI:

Select Cryo-EM/Real Space Refinement and choose Real Space Refinement
Set the title: "Refinement of resequenced 1aew chain A docked into density-modified map"
Add the map:  MapBox_2/denmod_map_box.ccp4
Add the resequenced model: SequenceFromMap_6/sequence_from_map.pdb
Set the resolution to 2 (A)
Hit Run to refine the model

In a couple minutes we have the refined model: sequence_from_map_real_space_refined_007.pdb.  This model fits the map pretty well, but the loop at P157 is
still way off (because it is different in the homologous structure we used as
a search model.  We need to re-fit this loop.

7. Fit a loop that is not in density

We can fix this loop using the fit_loops tool. We can look at the model and map
and notice that the part of the model that doesn't fit is residues 155-159.
Let's fix these residues.

In the Phenix GUI:

Select Cryo-EM/Docking and Model building and select "Fit Loops"

Add the map:  MapBox_2/denmod_map_box.ccp4
Add the resequenced refined model: RealSpaceRefine_7/sequence_from_map_real_space_refined_007.pdb
Add the sequence file: seq_unique.dat
Set the resolution to 2 (A)
Set Starting residue: 155
and Ending residue: 159
Specify Replace Residues (check the box)

Then hit Run to refit the loop.

In a minute this should fit this part of the chain into the density.

8. Refine model with fitted loop

We can refine one more time after fitting our loop at P157:

Phenix GUI:

Select Cryo-EM/Real Space Refinement and choose Real Space Refinement
Set the title: "Refinement of model with refit loop"
Add the map:  MapBox_2/denmod_map_box.ccp4
Add the fitted loop model: FitLoops_8/apoferrin_denmod_dock_0_fit_loops_8.pdb
Set the resolution: 2 (A)

Hit Run to refine the model

After a couple minutes we have the output model:
apoferrin_denmod_dock_0_fit_loops_8_real_space_refined.pdb. 
Have a look at this model in ChimeraX.  The loop at P157 should match the
density pretty well now.  You can load in the model apoferritin_chainA_rsr.pdb
and compare it with the model you have now created from 1aew_A.pdb.  You
might also want to load in the docked 1aew_A model 
(DockInMap_4/placed_model.pdb) to compare the structure of the starting model
before and after fixing the sequence and rebuilding the loop at P157.

