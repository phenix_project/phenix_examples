
TUTORIAL INFORMATION

alpha-Dendrotoxin (TOXD, 7139Da) is a small neurotoxin from green mamba
venom.  There is 1 copy of toxd in the asymmetric unit.  The files
provided can be used for three tutorials.

TUTORIAL 1

This tutorial demonstrates the ensembling and sculpting procedures.

Make an ensemble of the two most complete models (the homologous chain from
each of the complexes in 1BUN and 3CI7) and use that to solve the structure.
To do this, start by making a sequence alignment (Multiple sequence alignment
on the Phenix Utilities pulldown) between the target sequence and the PDB
files.  Make a note of which sequence in the alignment is the target sequence,
because you will need this for Sculptor (Target index). Run Ensembler to
superimpose the structures, then run Sculptor to trim back non-aligned loops
and non-identical sidechains.

Compare the result of using the ensemble to using the models individually,
including the less complete model from 1CO7.

Files: 1BUN_B.pdb 1CO7_I.pdb 3CI7_A.pdb toxd.seq toxd.mtz

TUTORIAL 2

This tutorial demonstrates the use of the B-factor and Variance
refinement.  Turn on the refinement of the overall B-factor and the
model variance.  Compare this to the results without refining those
parameters.

Files: 3CI7_A.pdb toxd.seq toxd.mtz

TUTORIAL 3

This tutorial demonstrates the use of electron density as a model.
The density has been cut out of an MIR map for the same crystal and
back-transformed to give structure factors.  To interpolate the
structure factors, Phaser needs to know the center of the region of
density and the extent in the x, y and z directions.  Because there
are no atoms, you need to tell Phaser how much of the content of the
asymmetric unit the density would account for, by providing the
molecular weight of either protein or nucleic acid in the volume
covered by the density.

Files: masked_density.mtz toxd.seq toxd.mtz
Center of density: 7.6, 7.0, 10.1
Extent of density: 27, 35, 27
Protein molecular weight in density region: 7133
