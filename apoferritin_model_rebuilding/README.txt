Apoferritin model rebuilding

This example is model rebuilding using a cryo-EM map and and a 
model created with phenix.map_to_model.  The goal is to improve the model
in mtm_to_rebuild.pdb.  The rebuilding will be done in "quick" model; for normal
use the "medium" mode is recommended (this takes much longer but tries more
methods for model improvement).

The map is apoferritin, density modified (from EMD entry 20026, 1.8 A)

The files are:


apoferritin.seq -- sequence of apoferritin
apoferritin_denmod_box.ccp4 -- density-modified map, boxed around known model
apoferritin_chainA_rsr.pdb -- known model refined against sharpened map
mtm_to_rebuild.pdb -- model created with map_to_model

Note: apoferritin.seq, apoferritin_denmod_box.ccp4, and
 apoferritin_chainA_rsr.pdb are in the apoferritin_model_building directory

The map is cut out (boxed) from the deposited maps (just to make the
files smaller for this demo).

===========================================================================
MODEL REBUILDING TUTORIAL (GUI, 10 minute run time)
===========================================================================

This tutorial will carry out model rebuilding using 1.8 A data on 
human apoferritin.  It is designed to be run from the Phenix GUI.
We are going to rebuild a section of a model.

Set up tutorial data
====================

In the Phenix GUI:
New project/Set up tutorial data/Select a dataset
Choose: Model rebuilding (apoferritin, one chain at 1.8 A)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Examine density-modified map and starting model and compare to known model
==========================================================================

Select Coot (top of page)
Then select: File/Open coordinates/apoferritin_chainA_rsr.pdb
             File/Open map/apoferritin_denmod_box.ccp4
             File/Open map/mtm_to_rebuild.pdb.ccp4
             Dispay Manager/select Properties for the map/Set level: 5
               rmsd/Change by rmsd: 0.3/OK

Have a look at residues 121-123 in mtm_to_rebuild.pdb which do not match
the known model. We are going to rebuild them by working from residues 107-136.

Run model rebuilding
====================

Phenix GUI: Select Cryo-EM/ModelRebuilding ... Non-default parameters:
Title: apoferritin model rebuilding 107-136
Full map file:                       apoferritin_denmod_box.ccp4
model file:                          mtm_to_rebuild.pdb
chain_id:                            A
first_resno:                         107
last_resno:                          136 
Resolution:                          2.5  (optional)
Seq file:                            apoferritin.seq
quick:                               True

Number of processors:                4
Run

Examine model in mtm_to_rebuild_rebuilt.pdb and compare with
 apoferritin_chainA_rsr.pdb
================================================================

Now look at residues 107-136 in the rebuilt model and compare them with
the original and with apoferritin_chainA_rsr.pdb.  These residues should
now match better. 


What to do after model_rebuilding:
==============================
 You might want to rebuild your model more thoroughly with 
phenix.model_rebuilding or you may want to edit it with a 
graphical tool such as Coot.
