# Apoferritin model rebuilding
 
# This example is model rebuilding using a cryo-EM map and and a 
# model created with phenix.map_to_model.  The goal is to improve the
#  model in mtm_to_rebuild.pdb
 
# The map is apoferritin, density modified (from EMD entry 20026, 1.8 A)
 
# The files are:
 
# apoferritin.seq -- sequence of apoferritin
# apoferritin_denmod_box.ccp4 -- density-modified map, boxed around known model
# apoferritin_chainA_rsr.pdb -- known model refined against sharpened map
# mtm_to_rebuild.pdb -- model created with map_to_model

# Note: some of these files are in the apoferritin_model_building directory

if (! -f $PHENIX/modules/phenix_examples/apoferritin_model_building/apoferritin_denmod_box.ccp4)then
  echo "The PHENIX directory is needed for this tutorial"
  goto finish
endif
cp $PHENIX/modules/phenix_examples/apoferritin_model_building/apoferritin_denmod_box.ccp4 .
cp $PHENIX/modules/phenix_examples/apoferritin_model_building/apoferritin_chainA_rsr.pdb .
cp $PHENIX/modules/phenix_examples/apoferritin_model_building/apoferritin.seq .
 
# The map is cut out (boxed) from the deposited maps (just to make the
# files smaller for this demo).
 
# ===========================================================================
# MODEL REBUILDING TUTORIAL (5 minute run time)
# ===========================================================================


# You can set the number of processors (nproc) here:

set nproc=4

echo "RUNNING MODEL REBUILDING NPROC=$nproc"

# To run quick model rebuilding from the command line:

phenix.rebuild_model resolution=2.5 apoferritin_denmod_box.ccp4 \
   seq_file=apoferritin.seq\
   chain_id=A \
   first_resno=107 \
   last_resno=136 \
   nproc=$nproc \
   quick=True mtm_to_rebuild.pdb

# The rebuilt model will be in mtm_to_rebuild_rebuilt.pdb

# What to do after model_rebuilding:
# ==============================

#   You might want to rebuild your model more thoroughly with
#  phenix.rebuild_model or you may want to edit it with a
#  graphical tool such as Coot.

finish:
