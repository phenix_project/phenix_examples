This Xtriage tutorial contains data that are used in the
Xtriage video tutorial available at https://www.youtube.com/watch?v=lWBDjfVN9D4

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: p9-xtriage (P. aerophilum translation initiation-factor 5a)
Hit OK once or twice to set up tutorial

On left panel of Phenix GUI hit "last modified" once or twice to
show the tutorial with a check mark next to it.


Run Xtriage:
============

In the Phenix GUI:
Under Crystals/Data analysis and manipulation choose "Xtriage"

Set the title "Xtriage analysis of unmerged p9 data"
Load the data file into the reflections box: p9_se_w2.sca
Set the unit cell dimensions (not present in this type of unmerged data file):
unit cell: 114 114 32.5 90 90 90 

and hit Run to run Xtriage.

Note that the p9 data has very few unusual features. You can follow the
description in the video tutorial 
https://www.youtube.com/watch?v=lWBDjfVN9D4
with this dataset.
