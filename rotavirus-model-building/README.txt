Model-building Tutorial (rotavirus fragment)

Files:
rotavirus.seq: sequence file
rotavirus_short_box.pdb: model
rotavirus_short_box.ccp4: density map

===========================================================================
MODEL-BUILDING TUTORIAL (GUI, 10 minute run time)
===========================================================================

This tutorial will carry out model-building using 3 A data on
a fragement of rotavirus capsid protein.  It is designed to be run from the Phenix GUI

Set up tutorial data
====================
In the Phenix GUI:
New project/Set up tutorial data/Select a dataset
Choose: Model building (Major capsid protein of group A rotavirus, 3 A)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.


In the Phenix GUI: Select Cryo-EM/Docking and Model-building/ MapToModel

Then set these parameters:
Title:                               rotavirus map-to-model quick run
Map file:                            rotavirus_short_box.ccp4
High-resolution limit:               3.0 (A)
Seq file:                            rotavirus.seq 
Thoroughness:                        quick
Number of processors:                4
Asymmetric map:                      True (check mark)

Then hit Run to start the job.  This will take about 5-10 minutes.
When the model-building is done, hit "Open in ChimeraX" to display the model that
is built along with the boxed sharpened map.


Command-line:
./run.csh

Reference: Grant, T., Grigorieff, N. (2015). eLife 2015;4:e06980.  Measuring the optimal exposure for single particle cryo-EM using a 2.6 Å reconstruction of rotavirus VP6 

