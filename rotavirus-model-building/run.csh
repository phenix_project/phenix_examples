#!/bin/csh -f


# build model into small sharpened map
phenix.map_to_model rotavirus_short_box.ccp4 \
   rotavirus.seq \
   resolution=2.6 \
   nproc=4 \
   asymmetric_map=True

# list output model
ls -tlr map_to_model.pdb

# Get cc of model to map
phenix.map_correlations map_to_model.pdb rotavirus_short_box.ccp4 resolution=3
