Info for rh-dehalogenase-mir tutorial
-------------------------------------


We recommend using both isomorphous and anomalous differences for this
structure.

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: Rhodococcus sp. dehalogenase (MIR)
Hit OK once or twice to set up tutorial

On left panel of Phenix GUI hit "last modified" once or twice to
show the tutorial with a check mark next to it.

Files:

sequence.dat:  sequence
rt_rd_1.sca:  Native dataset
auki_rd_1.sca  deriv #1 5 Au
hgki_rd_1.sca  deriv #2 5 Hg
ndac_rd_1.sca  deriv #3 5 Pt
hgi2_rd_1.sca  deriv #4 5 Hg
smac_1.sca     deriv #5 5 Sm


In GUI:

Select Crystals, then Experimental Phasing, then AutoSol

Put in a title:  "Dehalogenase MIR Gene V protein MAD 2.4 A data "

Hit the Browse button in File Path and choose these files to load:
sequence.dat
rt_rd_1.sca
auki_rd_1.sca
hgki_rd_1.sca
ndac_rd_1.sca
hgi2_rd_1.sca
smac_1.sca

Set the wavelength (symbol lambda) to 1.5418 for all the datasets
and the atom and number of sites as below:

auki_rd_1.sca  deriv #1 5 Au
hgki_rd_1.sca  deriv #2 5 Hg
ndac_rd_1.sca  deriv #3 5 Pt
hgi2_rd_1.sca  deriv #4 5 Hg
smac_1.sca     deriv #5 5 Sm

Set the rt_rd_1.sca dataset type to "SIR/MIR Native" and all the others to 
"SIR/MIR Derivative".  Leave the "Differences" as "Iso+Anom" for all the 
derivatives.

Set the unit cell to:   93.796  79.849  43.108  90.000  90.000  90.00

On the Options and Output tab:
Set the resolution to 2.4 (A)

Hit Run to analyze the data and produce a map and model.
Note: This MIR dataset takes 30 min to 1 hour to run.

Reference:

Newman, J.,  Peat, T.S.,  Richard, R.,  Kan, L.,  Swanson, P.E.,
Affholter, J.A.,  Holmes, I.H.,  Schindler, J.F.,  Unkefer, C.J.,
Terwilliger, T.C.  (1999).  "Haloalkane dehalogenases: structure of a
Rhodococcus enzyme."  Biochemistry 38: 16105-16114

PDB IDs: 1bn6 1bn7 1cqw
