TUTORIAL INFO

The polder tutorial demonstrates the computation of polder maps. A data
file and a model file are required. The example data are structure 1aba
and data truncated to 2.5 A resolution.

Phenix.polder computes omit maps by excluding the bulk solvent in the area around
a selection. An example of application are ligand omit maps. Polder omit maps
can be helpful if the ligand density is weak and obscured by bulk solvent
in conventional omit maps, where the ligand is deleted from the model, and bulk
solvent is modelled in the ligand cavity.

#--------------------------------------------------------------------
Example 1: Ligand density.
Calculate a polder map for solvent molecule MES 88 in structure 1ABA.
#--------------------------------------------------------------------

Instructions to calculate a polder map via the Phenix GUI:

- on the right menu, choose "Maps"
- then choose "Polder" --> a new window opens
- Model file --> select 1aba.pdb
- Data file --> select 1aba_reso_25.mtz
- Omit selection --> type "chain A and resseq 88"
  (residue 88 of chain A will be omitted)
- Hit button "Run"

(screenshots of these steps are in the tutorial pdf on
www.phenix-online.org/presentations/phenix_polder_tutorial.pdf)

Output is the file 1aba_polder_map_coefficients.mtz. which contains
map coefficients for:
  Polder map:
  - mFo-DFc_polder    : polder difference map coefficients
  - PHImFo-DFc_polder : corresponding phases
  Omit map:
  For this map, the omit selection is deleted from the model and bulk solvent
  enters the area ("usual" omit map)
  - mFo-DFc_omit      : omit difference map coefficients
  - PHImFo-DFc_omit   : corresponding phases

Both maps can be opened in COOT, either from the GUI or using the
-->File-->"Open MTZ, mmCIF or fcf or phs..." option. Then choose the
labels (see above, f.ex. mFo-DFc_polder and PHImFo-DFc_polder).
Important: tick the box "Is a difference map".


#--------------------------------------------------------------------
Example 2: Side chain density.
Calculate a polder map for a resdiue (Lys 13) in structure 1ABA.
#--------------------------------------------------------------------

The orientation of both conformations of Lys 13 are not justified by electron
density.

--> calculate a polder map (proceed as in example 1)
selection: chain A and resseq 13

Open the polder map and the omit map in COOT. Try to find orientations of Lys 13
which fit into the polder density. Especially conformation B might need some
re-orientation.

#---------------------
General comments:
#---------------------

The maps can be interpreted at "usual" contour levels of difference maps,
f.ex. 2.5-3.0 sigma.
If contour levels are compared, it might be useful to run
phenix.map_comparison with the polder and the omit map.
