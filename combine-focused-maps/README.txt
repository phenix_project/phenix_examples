Combine focused maps

This tutorial will show you how to run combine_focused_maps to combine the best
parts of two maps.  Each map represents chains A and B of a small peptide.  One map has a clear version of A, the other a clear version of B.  You will
run combine_focused_maps to get a composite map with a clear version of both
A and B.

Maps and models:

Clear A, blurry B:   AB.ccp4, AB.pdb
Blurry A, clear B:   focused_map_B.ccp4, focused_map_B.pdb

==========================================================================
I.  GUI VERSION
==========================================================================

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: Combine best parts of two maps (Apply combine_focused_maps to density for two peptides)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Run combine-focused-maps 
========================

Phenix GUI: Select Cryo-EM/Map improvement and choose "Combine focused maps"

Enter a Title: Combine best parts of two peptides
Select Map file: AB.ccp4
Model file: AB.pdb
Now hit + on the right side of the map and model lines to get new blank ones
Map file: focused_map_B.ccp4
Model file: focused_map_B.pdb
Resolution:  3

Hit Run to run the job.


This will produce composite_map.ccp4. Have a look at the composite map
and the original 2 maps with the "View in ChimeraX" button.

Notice that the two starting maps do not quite superimpose, so the combination
of maps involves a rotation/translation of focused_map_B.ccp4 relative to 
AB.ccp4.


==========================================================================
II.  COMMAND-LINE VERSION
==========================================================================
You can run combine_focused_maps like this:

phenix.combine_focused_maps \
  model_file=AB.pdb  map_file=AB.ccp4 \
  model_file=focused_map_B.pdb  map_file=focused_map_B.ccp4 \
  resolution=3
