Tutorial instructions:
**********************
  - Run Comprehensive validation (cryo-EM) first. Note:
    - relatively good model quality;
    - very poor model-to-map fit as indicated by map CC per residue
      plot and also inspection in Coot.
  - Run real-space refinement, steps include:
    - load model.pdb and map.ccp4 files;
    - specify map resolution;
    - on next tab choose to run minimization_global, simulated_annealing and
      morphing. Simulated Annealing (SA) and morphing are needed to achieve 
      large convergence radius. 
    - set number of macrocyles to 3 (so that refinement takes less time to run).
    
  Afte refinement inspect validation report note slightly improved model 
    geoemtry (it was good to begin with) but most importantly much improved 
    model-to-map fit.
    
  Command line equivalent of this refinement:
  
    phenix.real_space_refine remove_outliers=false model.pdb map.ccp4 \
      run=minimization_global+morphing+simulated_annealing macro_cycles=3 \
      resolution=3.3

File source:
************
  Model, PDB code: 6crz
  Map, EMDB code: 7577
  Resolution: 3.3 A

The structure is rather large for a quick tutorial. The following modifications
have been done to the origianl files in order to obtain files used in this
tutorial:

phenix.pdbtools keep="chain A and resseq 695:1055" 6crz_7577.pdb
phenix.map_box keep_origin=false keep_input_unit_cell_and_grid=false \
  6crz_7577.pdb_modified.pdb 6crz_7577.map
mv 6crz_7577.pdb_modified_box.pdb model.pdb
mv 6crz_7577.pdb_modified_box.ccp4 map.ccp4
  
