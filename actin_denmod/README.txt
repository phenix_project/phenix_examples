This example is density modification of a cryo-EM map.
The data are half-maps for a filament of actin, myosin and calmodulin, 
(PDB entry 6c1g EMD entry 7330, 3.8 A), boxed to contain the strongest
density, which is one actin domain .

The files are:

half_map_1_box.ccp4,half_map_2_box.ccp4  -- half maps 
6c1g.fasta  -- sequence file
6c1g_ABCD.pdb  -- known model for the complex, chains ABCD only
full_map_box.ccp4 -- full reconstruction

The maps are all cut out (boxed) from the deposited maps (just to make the
files smaller for this demo).

Set up tutorial data
====================
In the Phenix GUI:
New project/Set up tutorial data/Select a dataset
Choose: CRYO-EM/Density modification (Actin part of actin-myosiin-calmodulin complex at 3.8 A)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.


Running density modification in the Phenix GUI

In the Phenix GUI, click on "Cryo EM", "Map improvement",
 and then "Resolve CryoEM"

Select the "Add files" button and load the two half maps and sequence file.

Hit Run and a density-modified map will be calculated.
Also a local resolution map showing the resolution of the sharpened map will
be generated.

Output maps:
initial_map.ccp4        -- starting average map (sharpened)
denmod_map.ccp4         -- density-modified map
denmod_half_map_1.ccp4        -- density-modified half-map
denmod_half_map_2.ccp4        -- density-modified half-map
denmod_resolution_map.ccp4    -- local resolution map

The density-modified map is your improved map. The local 
resolution map is a map that shows the resolution at each point in the map. 
It is normally used to color the sharpened map.

If you open in ChimeraX the sharpened map will be displayed, colored by
local resolution.  If you want to change the coloring scheme, you can edit the
command shown in the GUI window and paste it into the ChimeraX command box. You
can also edit this command to color other maps you load into ChimeraX.


Command line usage:

To run one cycle of density modification from the command line:

phenix.resolve_cryo_em half_map_1_box.ccp4 half_map_2_box.ccp4 \
  seq_file=6c1g.fasta 

In about 10 minutes this will produce the files:

denmod_map.ccp4  -- density-modified map
denmod_half_map_1.ccp4,denmod_half_map_2.ccp4 -- density-modified half-maps

