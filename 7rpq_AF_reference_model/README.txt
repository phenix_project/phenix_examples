Reference model refinement of an AlphaFold model

This tutorial uses a trimmed AlphaFold model of the carboxyl-terminal 
processing protease A, CtpA, from Pseudomonas aeruginosa to demonstrate
reference model refinement.  The refinement obtained by using the
AlphaFold model as a reference model has considerably better geometry than
one obtained without using reference model refinement.

Background:  The Phenix AlphaFold server was used to generate an AlphaFold
model for CtpA based on its sequence. This AF model had an average 
predicted local distance difference test (plDDT) of 87.2, which is a medium to
high value, indicating much of the molecule is predicted with confidence but
not all of it.   The model was automatically trimmed based on plDDT values 
using the ProcessPredictedModel Phenix tool. This yielded a trimmed model with
two domains and 374 residues out of 403 in the sequence.  

The trimmed model (2 domains) was used as a search model in molecular 
replacement, and two copies of each chain were placed, yielding a total of
4 chains in a molecular replacement model.  This MR model matches much of the
deposited model, but residues 1-164 of chains A and B in the MR model and all
of chains C and D do not match anything in the deposited model and also 
have very poor density. (The deposited model is also missing residues
87-190 corresponding to residues 45-164 in the MR model).
For this tutorial residues 165-400 of chains A and B from the MR model are 
used for both refinement and as a reference model. 

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: Refinement/ Cpt Protease (Reference model refinement with AlphaFold
   model)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Files to load in GUI:
7RPQ.mtz:              Refinement data for 7RPQ
7RPQ_AF_trimmed.pdb:   AlphaFold model for 7RPQ, placed by MR

The following file is for comparison only:
7RPQ_superposed.pdb:   Deposited model, superposed on 7RPQ_AF_trimmed.pdb

Settings:

Refinement Settings:  Reference model restraints: True
All Parameters / Reference model restraints:  Use starting model as reference model: True

Then hit Run and Run now to start.

You might want to run a second run where you do not use the reference model
restraints to compare

When done, have a look at the MolProbity statistics for refinement with and
without using reference model restraints.  

Compare the Ramachandran plots for the two models, and compare the model in
the region of some of the worst residues in the model refined without
a reference model (e.g., K198)

Also have a look at the deposited model.  You can see what parts of the AlphaFold model that remain still need to be fixed.

