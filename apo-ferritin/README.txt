
Model building for 2.9A resolution cryo-EM reconstruction of Human heavy chain apoferritin
==========================================================================================

emd_6800.map - full apoferritin complex cryo-EM map
3ajo_complex.pdb - full apoferritin complex model
human-apo-ferritin-heavy.seq - sequence file
3ajo_monomer.pdb - single monomer (no waters or ligands)


===========================================================================
MODEL-BUILDING TUTORIAL (GUI, 30 minute run time)
===========================================================================

This tutorial will carry out model-building using 2.9 A data on
human apoferritin.  It is designed to be run from the Phenix GUI

Set up tutorial data
====================
In the Phenix GUI:
New project/Set up tutorial data/Select a dataset
Choose: Model building (apoferritin, 24 chains)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Run Autosharpen Map on emd_6800.map (5 minutes)
===============================================

In GUI select CryoEM/Map Improvement/Map Sharpening

Add a title: "Sharpening emd_6800.map" 
Select "Add file" and load the map file: emd_6800.map
Set the "file type" for this map to "Map" (not half-map)
Add the sequence file
Set the sharpening method to "b-factor"
Set the resolution to 2.9 (A)

Hit Run to auto-sharpen the map

Examine sharpened map
=====================

Select ChimeraX(top of page)
Then select: File/Open/3ajo_complex.pdb
             File/Open/emd_6800.map
Hit the grey square in "Volume viewer" below the word "emd_6800.map" and click around
the middle of "opacity" to show the model inside the map, and close that dialog.
Now select your new map:
File/Open/MapSharpeningCryoEM_1/emd_6800_sharpened.ccp4
Make this one partially opaque too.

Have a look at the two maps, adjusting the contour levels to make each look as much
like the model as possible.  In this case the original map is very good and sharpening
does not change it very much.

Run Map Symmetry to obtain ncs_spec file (1 minute)
===================================================

In the GUI, select Cryo-EM/Map analysis and manipulation/MapSymmetry

Add a title: "Map symmetry of emd_6800_sharpened.ccp4"
Load the map file (MapSharpeningCryoEM_1/emd_6800_sharpened.ccp4)

and hit Run to find the symmetry.

It should find the symmetry O (a) with 24 molecules in the symmetry group.
The "a" is just whether the 2-fold axis is along x or y.  You should also
now have a symmetry_from_map.ncs_spec file that has the symmetry information which
you can use later in this tutorial.


Run Map Box to obtain asymmetric unit (2 minutes)
=================================================

It is convenient to work with just the unique part of this structure, as it has
24 copies of the same thing.  We can cut out the asymmetric (unique) part
with MapBox by providing the ncs_spec file and selecting the extract unique method.

In the GUI, select Cryo-EM/Map analysis and manipulation/MapBox

Add a title: "Get unique part of map from emd_6800_sharpened.ccp4"
Load the map file (MapSharpeningCryoEM_1/emd_6800_sharpened.ccp4)

Check the "Extract unique" box and set the resolution to 2.9 (A)
Hit Run to get the unique part in a new boxed map
When it is done hit "Open in ChimeraX" to look at the boxed map.  If you
then open the MapSharpeningCryoEM_1/emd_6800_sharpened.ccp4 you can see that
they superimpose and the boxed map is just a part of the full sharpened map.

Run Map to Model on unique part of sharpened map
================================================

Now you can build a model of the unique part of this map.

In the Phenix GUI: Select Cryo-EM/Docking and Model-building/ MapToModel

Then set these parameters:
Title:                               emd_6800_sharpened_box.ccp4 map-to-model quick run
Map file:                            MapBox_emd_6800_sharpened_box.ccp4 
High-resolution limit:                          2.9  (optional)
Seq file:                            human-apo-ferritin-heavy.seq
Thoroughness:                        quick
Number of processors:                4
Asymmetric map:                      True (check mark)

Then hit Run to start the job.  This will take about 10-15 minutes.
When the model-building is done, hit "Open in ChimeraX" to display the model that
is built along with the boxed sharpened map.

Create a 24-mer from the newly-built model
==========================================

If your model looks more or less ok, you can create a full molecule with 24
subunits with ApplyNCS in the GUI:

In the Phenix GUI: Select Models/Superpose, search, compare, analyze symmetry/Apply NCS Operators

Add a title: "Applying operators to get 24-mer"

Load the PDB file you just built (MapToModel_4/map_to_model.pdb)
Load the full map: MapSharpeningCryoEM_1/emd_6800_sharpened.ccp4
and load the symmetry operators (called "NCS operators" in this GUI): MapSymmetry_1/symmetry_from_map.ccp4

Check the box for "Trim overlapping"
Set the resolution to 2.9 (A)

Hit Run to create the 24-mer molecule and trim any overlapping parts of chains.

When it finishes (30 sec) you can load it into the ChimeraX session you already 
have from your model-building above.  Then you can load the full
sharpened map as well (MapSharpeningCryoEM_1/emd_6800_sharpened.ccp4) and
you can see both the single chain model and the 24-mer model along with the
boxed map or full map.

As this was a quick build, the model is not quite as good as it might be with a
more thorough build.  In a real case you usually would want to use the 
standard model-building.

Next steps:
===========

After building an initial model, normally you will want to refine it with
Real Space Refinement, and examine the map and model with Coot or Isolde to rebuild
or remove parts that do not fit the density map will.

Map Citation:

Near-Atomic Resolution Structure Determination in Over-Focus with
Volta Phase Plate by Cs-Corrected Cryo-EM Fan X, Zhao L, Liu C, Zhang
JC, Fan K, Yan X, Peng HL, Lei J, Wang HW. Structure. 2017, 25:1623-1630

Model citation:

The universal mechanism for iron translocation to the ferroxidase
site in ferritin, which is mediated by the well conserved transit site.
Masuda T, Goto F, Yoshihara T, Mikami B. Biochem Biophys Res Commun.
2010, 400(1):94-9
