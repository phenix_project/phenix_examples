
TUTORIAL INFORMATION

With a good, redundant data set, the structure of hen egg-white lysozyme can be solved from the anomalous scattering of the intrinsic sulfur atoms.  However, with a poorer data set, such as the one included in this tutorial, it is not possible to solve the anomalous substructure ab initio.  Without a substructure, no phase information can be extracted.

Nonetheless, there is useful phasing signal in these data, once the substructure is known.  One way to determine the substructure is by starting from a molecular replacement model, then use SAD log-likelihood-gradient (LLG) maps to find the sulfur atom positions.  The number of sulfur atoms to search for can be found by counting the corresponding residues in the sequence file. Once the S atoms have been added to the model, the resulting phases automatically combine the phase information from both the molecular replacement model and the SAD data.

In this tutorial, the structure is solved first using a poor molecular replacement model (goat alpha-lactalbumin, 40% sequence identity).  The model-phased density is poor, although it is possible eventually to complete the structure.  The addition of SAD phase information improves the map significantly and makes model completion much more straightforward.  Both AutoMR and Phaser-MR will provide a "Run MR-SAD" button to set up the AutoSol GUI to initiate MR-SAD phasing with the molecular replacement model (once the wavelength and atom type have been filled in). Autosol takes RMSD as input rather than sequence identity and 40% for 129 residues corresponds approximately to 1.0�.

Files: 1fkq_prot.pdb hewl.seq lyso2001_scala1.mtz

There is 1 copy of the search model in the asymmetric unit.  The data were collected with Cu K-alpha radiation (1.5418A).  The results will be better if you make a sequence alignment between the model and the target, then use that to modify the model with sculptor.
