README for RanGDP R76E tutorial
-------------------------------

PDB ID: 1qg2

Citation: Kent HM, Moore MS, Quimby BB, Baker AM, McCoy AJ, Murphy GA,
Corbett AH, Stewart M. (1999).  J Mol Biol 289:565-77.
