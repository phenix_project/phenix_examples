PredictAndBuild with Molecular replacement TUTORIAL

Human Exonuclease-V, PDB entry 7lw7

In this example you will use PredictAndBuild to generate a model for the exoV
structure, trim it, obtain a molecular replacement result, and rebuild the 
molecular replacement model.

This tutorial uses the same data as the AF_exoV_MRSAD tutorial, 

You are going to supply a sequence file (the sequence of the construct that
was used in crystallization) and the crystallographic data (it is
anomalous data but you won't be using the anomalous part of it in this
example).

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: exoV (with PredictAndBuild, AlphaFold model)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Files:

exoV_construct.seq:   The sequence of the part of exoV that was crystallized
7lw7.mtz:             Crystallographic data for exoV as deposited in the PDB

7lw7_offset.pdb:      The structure of exoV from the PDB, offset to match the
                        MR solution that is found by PredictAndBuild (for
                        comparison)

Run PredictAndBuild
===================

In GUI: Select AlphaFold (Predicted models...)/PredictAndBuild (Crystallography)

Add the files:

7lw7.mtz   (data file)
exoV_construct.seq (sequence file)

Then hit "Run"  to start.

When it finishes... launch either Coot or Chimera and have a look at the
final model, final map, and compare them to the 7lwz_offset.pdb model
from the PDB.  Also compare the docked predicted model in the output
directory.  Have a look at parts of the docked AF model that do not
match the deposited model but where the rebuilt model does (e.g. residues
near residue 276).

****************************************************************************
********************* COMMAND-LINE INSTRUCTION  ****************************
****************************************************************************

cp -r $PHENIX/modules/phenix_examples/AF_exoV_PredictAndBuild .
cd AF_exoV_PredictAndBuild 

phenix.predict_and_build 7lw7.mtz seq_file=exoV_construct.seq control.nproc=4 jobname=exoV1 crystal_info.resolution=2.5

