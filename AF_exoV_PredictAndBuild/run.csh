#!/bin/csh

# Command to run predict and build on exoV data and use existing files:

phenix.predict_and_build 7lw7.mtz exoV_construct.seq nproc=4 jobname=exoV

# To run a new job, just change the jobname or move the exoV_PredictAndBuild
# directory
