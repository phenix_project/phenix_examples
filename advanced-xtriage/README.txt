This Advanced Xtriage tutorial contains datasets that are used in the
Advanced Xtriage video tutorials


Advanced Tutorial phenix.xtriage - Checking for twinning
https://www.youtube.com/watch?v=36mc80lrSVU
porin.mtz:
A 2.3 A dataset with merohedral twinning


Advanced Tutorial phenix.xtriage - Space Group Identification, Ice rings & Wilson Plot
https://www.youtube.com/watch?v=QlWahDGBTbE
p9_hires.mtz:
High resolution (1.7 A) data from the p9 dataset


sec17.sca:
A 3.3 A dataset with possible ice rings

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: Advanced Xtriage (Xtriage analysis of twinning, tNCS Wilson plots and ice rings)
Hit OK once or twice to set up tutorial

On left panel of Phenix GUI hit "last modified" once or twice to
show the tutorial with a check mark next to it.


Run Xtriage:
============

You can run Xtriage with any of these datasets in the Phenix GUI like this,
with the p9 dataset as an example:

Under Crystals/Data analysis and manipulation choose "Xtriage"

Set the title "Xtriage analysis of p9 data"
Load the data file: p9_hires.mtz

and hit Run to run Xtriage.

You can then change the title and load in data for the other two datasets as
well. Note that the p9 data has very few unusual features, while the porin 
data are twinned and the sec17 data has ice rings.

