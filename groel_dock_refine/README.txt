Map symmetry, fitting a model into a map, and real-space refinement for cryo-EM 

Data and models:
Data directory:  $PHENIX/modules/phenix_examples/groel_dock_refine

Map:  emd_8750.map -- cryo-EM map for groEL
Template structure:  1ss8_A.pdb -- X-ray structure (chain A only) of 
    groEL from PDB entry 1ss8
Deposited model:  5w0s.pdb -- deposited cryo EM structure fitting emd_8750 
    (for reference)
Resolution: 4 A
Symmetry of map: D7

             SUMMARY

In this tutorial, we will find the symmetry of the emd_8750 map.  Then we will
find the placement of the X-ray structure (PDB entry 1ss8, chain A) in two ways.

We will find the symmetry in the emd_8750 map using phenix.map_symmetry.

In the first procedure for placing the X-ray structure in the cryo-EM map
we will find chain A of 1ss8 in the full emd_8750
map with phenix.dock_in_map. We will then cut out the density in the map 
corresponding to this placed chain to make a small map that covers just the
one placed chain.  We will then refine the placed chain using the full 
original map.  Then we will create the entire 14-mer using the symmetry 
of the map and refine the full structure based on the full original map.

In the second procedure we will use the extract_unique option in phenix.map_box 
to automatically identify the unique part of the map and to create a small map
containing just his unique part.  Then we will use phenix.dock_in_map to
place chain A of 1ss8 in this small map, refine it, and apply symmetry to
create the entire 14-mer.


             DETAILED PROCEDURES

Procedures for running in the GUI and command-line versions are listed

==========================================================================
I.  DOCK and REFINE TUTORIAL -- GUI VERSION
==========================================================================

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: groEL cryo-EM map (emd_8750) and chain A of PDB 1ss8 (Docking model into map with dock_in_map)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Finding symmetry in the emd_8750 map
====================================

Phenix GUI: Select Cryo-EM/Map analysis and manipulation/Map symmetry
Set these parameters:
Title: Find symmetry in emd_8750 map
Map file: emd_8750.map
Symmetry type: D7
Hit Run to run the job

Result:  symmetry_from_map.ncs_spec
Correlation of symmetry-related regions: 0.92   Copies: 14 

This symmetry file can be used later to generate the 14-mer from any single 
chain such as the one we are going to generate next.  It can also be used
to identify the unique part of the emd_8750 map. You can leave out 
the "symmetry=D7" and it will find the same answer but it will take a 
couple minutes to rule out all other symmetry possibilities.


Finding chain A of 1ss8 directly in the full emd_8750 map
=========================================================

Phenix GUI: Select Cryo-EM/Docking and Model-building/Dock in map
Set these parameters:
Title:  Find chain A of 1ss8 in emd_8750.map
Map file: emd_8750.map
Search model:  1ss8.pdb
Resolution: 4
Number of processors: 4

in Search Options, select "Advanced" and uncheck the box for "MR search".  This
speeds up the job a lot in this particular case because the model is a small part
of a big map.

Hit Run to run the job.

The dock_in_map procedure finds density in the map that looks similar to
part of the model that you supply.  It tries first at lower resolution and
if that doesn't work it tries at higher resolution.  It first tries pure 
translations and then it tries rotations and translations. This structure 
is easy to find and it will just take a minute to produce the file:

   placed_model.pdb 


Run map_box to cut out the part of the emd_8750 map matching the docked model
=============================================================================

Phenix GUI: Select Cryo-EM/Map analysis and manipulation/Map box

Set these parameters:
Title:  Cut out map around docked chain A of 1ss8 in emd_8750.map
Map file: emd_8750.map
Model file: DockInMap_2/placed_model.pdb 
Output file name prefix: emd_8750_box
Mask atoms:  True (check the box)

Hit Run to run the job

The "mask_atoms=True" option tells map_box to remove all density that is more
than 3 A from an atom in our model.  You can increase this distance with 
the parameter "mask_atoms_radius=5" if you like.

This produces two files we want (emd_8750_box.pdb is the same 
as DockInMap_2/placed_model.pdb) and emd_8750_box.ccp4 is the boxed map:

  emd_8750_box.ccp4   
  emd_8750_box.pdb


View docked map and model in ChimeraX
=====================================

In the Dock in map window, select Open in ChimeraX (bottom of page) and
have a look at the map and placed model.
Compare the full map with the boxed one.  In the overlap region they should be
the same.

Real-space refinement of docked model
=====================================

Phenix GUI: Select Cryo-EM/Real-space refinement/Real-space refinement

Set these parameters:
Title:  Refine docked chain A of 1ss8 against boxed emd_8750.map
Use the Add file button to load your map and model:
Map file: MapBox_3/emd_8750_box.ccp4
Model file: MapBox_3/emd_8750_box.pdb
Resolution:  4
On Refinement settings tab: Macro cycles:  1  (just to make it quick)
Run

This produces a new model:
  emd_8750_box_real_space_refined_000.pdb

Have a look at this model in your graphics program and compare it to the 
starting model emd_8750_box.pdb.

Apply reconstruction symmetry to refined docked model
=====================================================

Now let's create the entire 14-mer with D7 symmetry with phenix.apply_ncs and
supplying the symmetry operators (symmetry_from_map.ncs_spec) and the
refined single chain A (emd_8750_box_real_space_refined_000.pdb):

Phenix GUI: Select Cryo-EM/Apply NCS operators
Title:  Generate full groEL from refined docked chain A of 1ss8 
Model file: RealSpaceRefine_4/emd_8750_box_real_space_refined_000.pdb
NCS operators: MapSymmetry_1/symmetry_from_map.ncs_spec
Run

Have a look at the full 14-mer, the boxed map, and the original map.  You
could also run real-space refinement of the reconstructed 14-mer against the
full original map at this point if you like.


Extracting unique part of emd_8750 map then finding chain A of 1ss8
===================================================================

Now let's directly extract the unique part of the map in emd_8750 and then 
dock our chain A from 1ss8 into it.

Phenix GUI:  Cryo-EM/Map box
Title: Extract unique part of emd_8570 map
Map file: emd_8750.map
Output file name prefix: emd_8750_unique
Extract unique: True
Resolution: 4
Molecular mass: 1000000
symmetry: d7
Run

This identifies the unique part of the map and puts it in emd_8750_unique.ccp4.
Let's dock chain A of 1ss8 into this unique part of the map:

Phenix GUI: Select Cryo-EM/Dock in map
Non-default parameters:
Title:  Find chain A of 1ss8 in emd_8750_unique.map
Map file: MapBox_5/emd_8750_unique.map
Search model:  1ss8.pdb
Resolution: 4
Number of processors: 4
Run

Have a look in coot (Open in Coot button at bottom of the dock-in-map window).
If you set the Edit/map_parameters/map radius to 45 you can see that the
unique part in this case was exactly one chain of the molecule.


==========================================================================
I.  DOCK and REFINE TUTORIAL -- COMMAND-LINE VERSION
==========================================================================
 ----- Copying the data to a working directory ------

You can copy the data for this tutorial and get into the working directory with:

  cp -r $PHENIX/modules/phenix_examples/groel_dock_refine .
  cd groel_dock_refine

 ----- Finding symmetry in the emd_8750 map ------

You can type:

   phenix.map_symmetry emd_8750.map symmetry=D7

In a couple seconds this will produce a file containing the 14 symmetry 
operators for this map:

   symmetry_from_map.ncs_spec

This symmetry file can be used later to generate the 14-mer from any single 
chain such as the one we are going to generate next.  It can also be used
to identify the unique part of the emd_8750 map. You can leave out 
the "symmetry=D7" and it will find the same answer but it will take a 
couple minutes to rule out all other symmetry possibilities.


 ----- Finding chain A of 1ss8 directly in the full emd_8750 map ------

You can find chain A of 1ss8 with the command:

  phenix.dock_in_map 1ss8_A.pdb emd_8750.map resolution=4 nproc=4 \
     pdb_out=placed_model_from_emd_8750.pdb dock_with_mr=False

The backslash tells the command-line that the next line is a
continuation of the current line. There must be nothing after the backslash
(no spaces) for it to work.  

You can choose how many processors to use with the nproc=4 command. 
The dock_in_map procedure finds density in the map that looks similar to
part of the model that you supply.  It tries first at lower resolution and
if that doesn't work it tries at higher resolution.  It first tries pure 
translations and then it tries rotations and translations. This structure 
is easy to find and it will just take a minute to produce the file:

   placed_model_from_emd_8750.pdb 

Now let's cut out the part of the emd_8750.map corresponding to the 
placed model, placed_model_from_emd_8750.pdb:

  phenix.map_box emd_8750.map placed_model_from_emd_8750.pdb mask_atoms=True

The "mask_atoms=True" option tells map_box to remove all density that is more
than 3 A from an atom in our model.  You can increase this distance with 
the parameter "mask_atoms_radius=5" if you like.

This produces two files we want:

  placed_model_from_emd_8750_box.pdb   
  placed_model_from_emd_8750_box.ccp4

You should load the original map (emd_8750.map), the box map 
(placed_model_from_emd_8750_box.ccp4) and the box model
(placed_model_from_emd_8750_box.pdb) in Coot or ChimeraX or another graphics
program and see how the density matching the placed model is cut out. 

The output PDB file (placed_model_from_emd_8750_box.pdb) will be 
identical to the placed_model_from_emd_8750.pdb file we supplied because 
by default map_box does not shift the origin.  The "ccp4"
file (this is the same format as "mrc" or "map" files) is the cut-out part
of the emd_8750.map file.  The cut-out map will be much smaller than
the original, but the density will be in the same place (it won't be shifted),
so the model in placed_model_from_emd_8750.pdb will still fit right into
the map in placed_model_from_emd_8750_box.ccp4.

Now let's refine the model with real space refinement. Let's use the original
full map for our refinement (we can also use the cut out map if we want but
you may need the keyword ignore_symmetry_conflicts=true).
Let's refine just one cycle to speed it up for now:

  phenix.real_space_refine placed_model_from_emd_8750_box.pdb \
     EMD_8750.map resolution=4 macro_cycles=1 

In about a minute this produces the file:

  placed_model_from_emd_8750_box_real_space_refined_000.pdb

Have a look at this model in your graphics program and compare it to the 
starting model placed_model_from_emd_8750_box.pdb.

Now let's create the entire 14-mer with D7 symmetry with phenix.apply_ncs and
supplying the symmetry operators (symmetry_from_map.ncs_spec) and the
refined single chain A 
(placed_model_from_emd_8750_box_real_space_refined_000.pdb):

  phenix.apply_ncs symmetry_from_map.ncs_spec \
      placed_model_from_emd_8750_box_real_space_refined_000.pdb \
      pdb_out=placed_model_from_emd_8750_box_real_space_refined_D7.pdb

We can refine the full 14-mer against the full original map. Let's refine just 
one cycle to speed it up for now: 

  phenix.real_space_refine emd_8750.map resolution=4 macro_cycles=1 \
     placed_model_from_emd_8750_box_real_space_refined_D7.pdb

which will produce (after about 10 minutes or so):

  placed_model_from_emd_8750_box_real_space_refined_D7_real_space_refined_000.pdb

Have a look at this model that you have created with the map emd_8750 and the
model 1ss8.pdb and compare it with the deposited model 5w0s.pdb. They should be 
very similar!


----- Extracting unique part of emd_8750 map then finding chain A of 1ss8 ----


Now let's directly extract the unique part of the map in emd_8750 and then 
dock our chain A from 1ss8 into it.

We can extract the unique part with:

    phenix.map_box emd_8750.map extract_unique=true resolution=4 \
       molecular_mass=1000000 symmetry=d7

In a minute or two, this will produce the file:

    emd_8750_box.ccp4

with a smaller unit cell (91.02, 65.19, 87.33) A compared with the starting 
unit cell of (295.2  295.2  295.2) A. The map in emd_8750_box.ccp4 will
superimpose on the original emd_8750.map in the region that is boxed.  Note 
that in other places the box map has no meaning. Have a look at this map in 
Coot or ChimeraX.  Note that the cut out part of the structure is compact,
and in this case, looks a lot like chain A of 1ss8.

Now let's fit chain A of the X-ray structure 1ss8 into this small map using
phenix.dock_in_map. This will take about a minute:

    phenix.dock_in_map emd_8750_box.ccp4 1ss8_A.pdb \
      resolution=4 pdb_out=1ss8_A_in_emd_8750_box.pdb

Have a look at the docked model (1ss8_A_in_emd_8750_box.pdb). It should fit
right in the unique part of the map (emd_8750_box.ccp4).

Now you can refine this model against the unique part of the map, create the
full 14-mer, and refine the full 14-mer against the full map just as you did
above. 
