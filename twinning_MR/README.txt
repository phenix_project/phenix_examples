SusD (Starch Utilization System D) is a protein found in various bacteria,
including Bacteroides thetaiotaomicron, a prominent member of the human gut
microbiota. SusD proteins are involved in starch utilization and are crucial
components of starch-degrading systems. These systems allow bacteria to break
down complex carbohydrates, such as starch, into simpler sugars for energy
production and growth.

This tutorial is derived from PDB entry 3snx, the structure of a SusD homologue
from Bacterioides thetaiotaomicron. The tutorial demonstrates the difficulty of
space group determination in the presence of twinning. For the best experience,
please do not look at that PDB entry (in particular the final space group)
until you have finished the tutorial!

Files:

orthorhombic.mtz:                             Data in space group P22121
SusDhomologue.seq:                            Sequence of protein
AF-Q8A7T5-F1-model_v3.pdb:                    AlphaFold model from database
AF-Q8A7T5-F1-predicted_aligned_error_v3.json  Associated PAE matrix
AF-Q8A7T5_PAE.png                             Rendering of PAE matrix

Process the AlphaFold predicted model of this protein and use this model to
attempt to solve the structure. Is there a problem?

A successful molecular replacement solution will generally be unique and
unambiguous. Finding multiple solutions with better than random scores suggests
that there are a number of ways to be partly correct. This can happen if, for
instance, you have pseudosymmetry that you think is true symmetry.

Run phenix.xtriage (or pointless in CCP4) and look at the results of twin
tests. Is there strong evidence for twinning? If there is, what twin law would
you have to consider? It is also useful to look at the intensity moments plots
in the Phaser log, visible from the GUI. Is there an issue here?

What you should find is that there is evidence for twinning but that twinning
is not possible in this lattice with orthorhombic symmetry. What could be the
true space group if twinning has led to the wrong conclusion about the space
group symmetry?

Test the possible space groups with lower than orthorhombic symmetry. You can
use the Phenix reflection file editor (in the Favorites tab of the GUI) to
expand the data to a chosen subgroup of the space group. Enter an explicit
Output space group under Output options, noting that the space group P2 with
a 2-fold along the a-axis (a non-standard setting) can be specified as 
"P 2 1 1".

Citation: This structure, PDB entry 3snx, is unpublished and was determined by
the Joint Center for Structural Genomics.
