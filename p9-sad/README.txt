
TUTORIAL INFO

The p9-sad tutorial demonstrates SAD phasing, so only a single
Scalepack file and sequence are required.  The data extend to 1.7A
resolution, but AutoSol can build a satisfactory model at 2.5A in much
less time, so we recommend truncating the data when running this
tutorial.

The experiment wavelength is 0.9792, and 5 Se sites are expected.
There is also a sulfur atom (Cys side chain) so you might want to look for
S in addition to Se.

Appropriate theoretical f' and f" values can be calculated based on
this information.

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: P. aerophilum translation-initiation factor 5a (SAD)
Hit OK once or twice to set up tutorial

On left panel of Phenix GUI hit "last modified" once or twice to
show the tutorial with a check mark next to it.

Files:

seq.dat:   The sequence of the protein 
p9.sca:  X-ray data for the protein


In GUI:
Select Crystals, then Experimental Phasing, then AutoSol

Put in a title:  "P9 SAD data to 2.5 A with Se and S"

Hit the Browse button in File Path and choose these files to load:
  seq.dat
  p9.sca

Set the wavelength (symbol lambda) to: 0.9792
Set the atom type to: Se

On the second tab ("Options and output"), set the resolution to 2.5 (A)
In the box for "Additional heavy atom types for Phaser SAD, enter: S

Now your parameters are set.

Hit "Run"  to start
After 15-30 minutes the job should finish and you can use Coot to look at
the model and heavy atom sites that are obtained along with the map that
is produced.  You should be able to see that one of the heavy-atom sites 
is labeled as a sulfur and that this superimposes on the side chain of 
Cys-33 (it may be in a different asymmetric unit, so you need to turn on
symmetry in Coot to see that it matches).

REFERENCE

Peat TS, Newman J, Waldo GS, Berendzen J, Terwilliger TC. Structure of
translation initiation factor 5A from Pyrobaculum aerophilum at 1.75 A
resolution. Structure. 1998 Sep 15;6(9):1207-14. PubMed PMID: 9753699.

PDB: http://www.rcsb.org/pdb/explore/explore.do?structureId=1BKB
