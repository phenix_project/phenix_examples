#!/bin/sh
echo "Running AutoSol on P9 data..."
phenix.autosol  seq_file=seq.dat sites=4 atom_type=Se  data=p9_se_w2.sca  \
space_group="I4" unit_cell="113.949 113.949  32.474 90.000  90.000  90.00"  \
resolution=2.4 thoroughness=quick

