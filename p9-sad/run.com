#!/bin/csh
echo "Running AutoSol in " `pwd`
if ( ! -d run ) then
 echo "making directory run"
 mkdir run
endif
cp -p sav/* run/.
cd run
ls -lt 
cat Facts.list
$PHENIX/phenix/phenix/wizards/AutoSol.py > AutoSol.log
./check.com
