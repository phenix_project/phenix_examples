TUTORIAL INFORMATION

beta-Lactamase (BETA, 29kDa) is an enzyme produced by various bacteria, and is
of interest because it is responsible for penicillin resistance, cleaving
penicillin at the beta-lactam ring. There are many small molecule inhibitors of
BETA in clinical use, but bacteria can become resistant to these as well.
Streptomyces clavuligerus produces beta-lactamase inhibitory protein (BLIP,
17.5kDa), which has been investigated as an alternative to small molecule
inhibitors, as it appears more difficult for bacteria to become resistant to
this form of BETA inhibition.

The structures of BETA and BLIP were originally solved separately by
experimental phasing methods. The crystal structure of the complex between BETA
and BLIP has been a test case for molecular replacement because of the
difficulty encountered in the original structure solution. BETA, which models
62% of the unit cell, is trivial to locate, but BLIP is more difficult to find.
The BLIP component was originally found by testing a large number of potential
orientations with a translation function search, until one solution stood out
from the noise.  There is 1 copy of each search model in the asymmetric unit.

Files: beta.pdb blip.pdb beta.seq blip.seq beta_blip_P3221.mtz

The goal is to learn how to navigate the Phenix full-featured interface to
Phaser (the “simple” interface is being deprecated and its use is discouraged),
providing the information the program needs: which model or models are being
used, what is the total composition of the asymmetric unit of the crystal,
given as one or more sequence files (so Phaser knows what fraction of the
structure the model explains), which components are being placed and how many
copies of each. You should also learn to distinguish between situations where
you are placing multiple different components or where you have multiple
alternative models for the same component.

Citation: The crystal structure of a hetero-dimer of beta-lactamase (BETA) and
beta-lactamase inhibitor protein (BLIP), both with molecular replacement models
from crystal structures of the individual BETA and BLIP components. We thank
Mike James and Natalie Strynadka for the diffraction data. 
Reference: Strynadka, N.C.J., Jensen, S.E., Alzari, P.M. & James. M.N.G. (1996)
Nat.  Struct. Biol. 3 290-297.
