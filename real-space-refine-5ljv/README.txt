Tutorial instructions:
**********************
  - Run Comprehensive validation (cryo-EM) first. Note:
    - geometry imperfections that are not expected at this resolution;
    - outliers on all six Ramachandran plots;
    - MG fit may be better (see map and model in Coot).
  - Run real-space refinement, steps include:
    - load model.pdb and map.ccp4 files;
    - specify map resolution;
    - on next tab specify minimization_global as refinement options. Also, set 
      number of macrocyles to 3 (so that refinement takes less time to run).
  
  After refinement inspect validation report, note:
    - there is virtually no outliers;
    - some geometries can be improved manually in Coot; 
    - improved fit of MG and the ligand.
  
  Command line equivalent of this refinement:
  
    phenix.real_space_refine model.pdb map.ccp4 macro_cycles=3 \
      run=minimization_global resolution=3.64

File source:
************
  Model, PDB code: 5ljv
  Map, EMDB code: 4062
  Resolution: 3.64 A

The structure is rather large for a quick tutorial. The following modifications
have been done to the origianl files in order to obtain files used in this
tutorial:

phenix.pdbtools keep="chain A" 5ljv_4062.pdb
phenix.map_box keep_origin=false keep_input_unit_cell_and_grid=false \
  5ljv_4062.pdb_modified.pdb 5ljv_4062.map
mv 5ljv_4062.pdb_modified_box.pdb model.pdb
mv 5ljv_4062.pdb_modified_box.ccp4 map.ccp4
