This is cryo-EM data for EMD-6123 (Helical nanotube formed from 
a 29-residue peptide).

The map file is emd_6123.map
The sequence file is 3j89_6123.seq
The deposited model is 3j89.pdb
The symmery file is 3j89_6123.ncs_spec

The resolution is 3.6 A

You should be able to build a model with the command:

phenix.map_to_model emd_6123.map 3j89_6123.seq 3j89_6123.ncs_spec resolution=3.6 nproc=4 build_in_regions=False include_phase_and_build=False

and it should take about 5 minutes. To get a more complete model you can try leaving off the build_in_regions=False include_phase_and_build=False commands.
