This example is density modification of a cryo-EM map, followed by
structure determination by automatic model-building, fixing a mis-traced
segment, sequence assignment and refinement

NOTE: the details of what needs to be fixed after automatic model-building
(steps 5-11) may change with platform, number of processors or version
of Phenix used

The data are half-maps for apoferritin (EMD entry 20026, 1.8 A)

The files are (NOTE: the map files are in the apoferritin_denmod directory):

  - emd_20026_half_map_1_box.ccp4 emd_20026_half_map_2_box.ccp4 -- half maps
  - seq.dat -- sequence file
  - run_denmod_model_building.csh  -- script to run all these commands
  - 20026_auto_sharpen_A.ccp4 -- deposited map, boxed, half-map sharpened


The maps are all cut out (boxed) from the deposited maps (just to make the
files smaller for this demo).

A GUI version (a quick run) and a COMMAND-LINE version of the tutorial (more
thorough) are below.

==========================================================================
I.  GUI VERSION (quick run, 30 min total run time)
==========================================================================

DENSITY MODIFICATION AND MODEL-BUILDING TUTORIAL (GUI)

This tutorial will carry out density modification and model-building using
1.8 A data on human apoferritin.  It is designed to be run from the Phenix GUI

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: Density modification, model-building and refinement (apoferritin at 1.8 A)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Examine starting map
====================

Select Coot (top of page)/File/Open coordinates/apoferritin_chainA_rsr.pdb
File/Open map/emd-200026_auto_sharpen_A.ccp4
Dispay Manager/select Properties for the map/Set level: 5/rmsd/Change by rmsd: 0.3/OK
Look at Tyr81 (Draw/GotoAtom/Residue number:81) and 
His13 (side-chain) and Glu162 (main-chain) density

Run density modification
========================

Phenix GUI: Select Cryo-EM/ResolveCryoEM ... Non-default parameters:
Title: apoferritin tutorial 1.8/2.2 A
Half map file 1: emd_20026_half_map_1_box.ccp4 
Half map file 2: emd_20026_half_map_2_box.ccp4 
Resolution:                          2.2      
Resolution for density modification: 1.8
Seq file:                            seq.dat
Box before analysis:                 False
Number of processors:                4
Density modify unsharpened maps:     True
Final scale with FSCref:             False
Resolution bins:                     20
Run

Cut out map around supplied model
=================================

Select Cryo-EM/Map Box
Title: Box density-modified map around supplied model
Map file:    ResolveCryoEM_1/denmod_map.ccp4
Model file:  apoferritin_chainA_rsr.pdb
Output file name prefix:  denmod_map_box
Mask atoms:  True
Run

Examine density-modified map and compare to starting map
========================================================

"Open in Coot"  opens on top of existing Coot window. 
Look at Tyr81 and His13 (side-chain) and Glu162 (main-chain) density

Run model-building on one monomer boxed from density modified map
=================================================================

Select Cryo-EM/MapToModel
Title:   Quick build using boxed density modified map
Map file: MapBox_2/denmod_map_box.ccp4 
High-resolution: 1.8
Sequence file: seq_unique.dat
Thoroughness: quick
Number of processors: 4
Asymmetric map: True
Run
Open in Coot at end

Remove worst-fitting residues
=============================

Select Model Tools/ PDBTools
Title:  Remove residues (39:46 or 83:88)
Add file:  MapToModel_3/map_to_model.pdb
Output File name: map_to_model_trimmed.pdb
Options/Remove atom selection:  resseq 83:88 or resseq 39:46
Run

Resequence and fit loops
========================

Select Cryo-EM/SequenceFromMap
Title: Resequence and fit loops in map_to_model_trimmed.pdb
Map file:         MapBox_2/denmod_map_box.ccp4
Model file:       PDBTools_4/map_to_model_trimmed.pdb
Sequence file:    seq.dat
High-resolution limit:  1.8
Sequence and fit loops: True
Run
Look at sequence_from_map.pdb in Coot

(There will still be a few places that need fixing)


==========================================================================
II.  COMMAND-LINE VERSION (more extensive model-building, not as quick)
==========================================================================

A. DENSITY MODIFICATION OF HALF-MAPS

To run one cycle of density modification from the command line you can paste
this in to a terminal window. From the GUI just set all the same parameters
and no need to set the output_directory (it will go in resolve_cryo_em_0 or
a similar directory name):

phenix.resolve_cryo_em emd_20026_half_map_1_box.ccp4 \
    emd_20026_half_map_2_box.ccp4 \
    resolution=2.2 dm_resolution=1.8 \
    seq_file=seq.dat \
    cycles=1 \
    nproc=4 \
    box_before_analysis=False \
    density_modify_unsharpened_maps=True\
    output_directory=denmod \
    output_files.denmod_map_file_name=denmod_map.ccp4 \
    initial_map_file_name=initial_map.ccp4
   
In about 15 minutes this will produce (in a denmod/ or 
  resolve_cryo_em_xx directory) the file: denmod_map.ccp4

B. STRUCTURE SOLUTION BY AUTOMATED MODEL-BUILDING 

We are going to build a model into our density-modified map, 
 refine it, fix the sequence and refine again.

1. Getting map symmetry
We will want a symmetry file describing the symmetry operators that have been
used in our map.  We can get them automatically in about 30 seconds with:

phenix.map_symmetry denmod/denmod_map.ccp4 resolution=2 \
   symmetry_out=denmod_map.ncs_spec

Now denmod_map.ncs_spec has the 24 symmetry operators for octahedral symmetry
in the appropriate location of this map.


2. Automated model-building using a density-modified map

We can build a model into our map with map_to_model:

phenix.map_to_model denmod/denmod_map.ccp4 resolution=2 nproc=4 \
    ncs_file=denmod_map.ncs_spec \
    pdb_out=map_to_model.pdb  \
    seq_file=seq.dat

In a half hour or so you should get the model 1aew_A_built_in_denmod_map.pdb.

3.  Cut out a piece of density-modified map and compare to build_in model
You can cut out a piece of your density modified map and look at it along with
your model this way:

phenix.map_box denmod/denmod_map.ccp4 map_to_model.pdb \
     ignore_symmetry_conflicts=true prefix=denmod_map_A

This will produce denmod_map_A.ccp4 and denmod_map_A.pdb which you can look 
at in Coot or Chimera.  The model is mostly pretty good but may need
some fixing, depending on your version of Phenix

4.  Refine the model against the density-modified map

We can refine the build_in model either against the original or cut-out map. 
 Let's use the cut-out map.  We can also refine just chain A or the 
 whole 24-mer. The difference is if you use the whole 24-mer you are 
going to include inter-chain contacts and if you use a single chain you 
 will ignore them. Let's use just one chain for now:

phenix.real_space_refine map_to_model.pdb denmod_map_A.ccp4 resolution=2 \
     ignore_symmetry_conflicts=True

After a couple minutes we have the output model: 
map_to_model_real_space_refined_000.pdb.  If you look at this model
in Coot or Chimera you'll see it has moved some side chains into density, but
that the model still doesn't match the density for residues 154-162.

5.  If your model has parts that do not fit, try something like the 
following. 

NOTE: the results for these steps may depend on platform, version, 
and number of processors so you may have to adjust them based on what your
model looks like.

First remove the bad part (here the bad part is residues 154 through 162):

phenix.pdbtools map_to_model_real_space_refined_000.pdb \
   remove="resseq 154:162" \
   output.file_name=map_to_model_real_space_refined_del.pdb

6. Redo the sequence assignment and see if we can fill in the gap. 
Here the keyword run_assign_sequence=True is used to reassign the
sequence and then fill in the gap:

phenix.sequence_from_map run_assign_sequence=True \
  model_file=map_to_model_real_space_refined_del.pdb \
  seq_file=seq_unique.dat \
  map_file=denmod_map_A.ccp4 \
  pdb_out=sequence_from_map.pdb\
  resolution=2

This should produce the file sequence_from_map.pdb in a couple minutes.

7. We can fix one other part of the model that does not fit well (residues
 6-9):

phenix.pdbtools sequence_from_map.pdb \
   remove="resseq 6:9" \
   output.file_name=sequence_from_map_del_6-9.pdb

8. Redo the sequence assignment and see if we can fill in the gap as in 6 above:

phenix.sequence_from_map run_assign_sequence=True \
  model_file=sequence_from_map_del_6-9.pdb \
  seq_file=seq_unique.dat \
  map_file=denmod_map_A.ccp4 \
  pdb_out=sequence_from_map_2.pdb \
  resolution=2

This should produce the file sequence_from_map_2.pdb in a couple minutes.

9. Let's refine one more time:

phenix.real_space_refine sequence_from_map_2.pdb denmod_map_A.ccp4 \
    resolution=2 ignore_symmetry_conflicts=True

Now have a look at sequence_from_map_2_real_space_refined_000.pdb and 
denmod_map_A.ccp4 in Coot or Chimera.  The model should fit
the map quite well.  If there are missing side chains, you can fill them in with:

10. Run sequence_from_map

phenix.sequence_from_map sequence_from_map_2_real_space_refined_000.pdb \
    denmod_map_A.ccp4 seq_file=seq_unique.dat resolution=2 \
    pdb_out=sequence_from_map_3.pdb

and now sequence_from_map_3.pdb should have more side chains built.  You could
refine once more too:

11. Final refinement:

phenix.real_space_refine sequence_from_map_3.pdb denmod_map_A.ccp4 \
    resolution=2 ignore_symmetry_conflicts=True

and sequence_from_map_3_real_space_refined_000.pdb is the refined model.
Next you would go over the model carefully using validation tools and
remove any parts that are not ok and fix parts that are not quite right.
You can also create the full 24-mer using your map symmetry and the model

