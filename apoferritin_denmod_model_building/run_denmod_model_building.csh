#!/bin/csh -f

# This example is density modification of a cryo-EM map, followed by
# structure determination by automatic model-building, fixing a mis-traced
# segment, sequence assignment and refinement

# NOTE: the details of what needs to be fixed after automatic model-building
# (steps 5-10) may change with platform, number of processors or version 
# of Phenix used 

# The data are half-maps for apoferritin (EMD entry 20026, 1.8 A)

# The files are:

#   - emd_20026_half_map_1_box.ccp4 emd_20026_half_map_2_box.ccp4 -- half maps
#   - seq.dat -- sequence file
#   - run_denmod_model_building.csh  -- script to run all these commands

# The maps are all cut out (boxed) from the deposited maps (just to make the
# files smaller for this demo).

# You can set the number of processors (nproc) here:
set nproc=4

# Make sure this has not been run in this directory before:

foreach f ( map_to_model_real_space_refined_000.pdb sequence_from_map_2_real_space_refined_000.pdb sequence_from_map_3_real_space_refined_000.pdb)
  if (-f $f) then
   echo "Please run in a clean directory or remove old files from previous runs"
   goto finish
  endif
end

echo "RUNNING DENSITY MODIFICATION AND REFINEMENT WITH NPROC=$nproc"

# A. DENSITY MODIFICATION OF HALF-MAPS

# To run one cycle of density modification from the command line you can paste
# this in to a terminal window. From the GUI just set all the same parameters
# and no need to set the output_directory (it will go in resolve_cryo_em_0 or
# a similar directory name):

phenix.resolve_cryo_em emd_20026_half_map_1_box.ccp4 \
    emd_20026_half_map_2_box.ccp4 \
    resolution=2.2 dm_resolution=1.8 \
    seq_file=seq.dat \
    cycles=1 \
    nproc=$nproc \
    box_before_analysis=False \
    density_modify_unsharpened_maps=True\
    output_directory=denmod \
    output_files.denmod_map_file_name=denmod_map.ccp4 \
    initial_map_file_name=initial_map.ccp4
   
# In about 15 minutes this will produce (in a denmod/ or 
#   resolve_cryo_em_xx directory) the file: denmod_map.ccp4

# B. STRUCTURE SOLUTION BY AUTOMATED MODEL-BUILDING 

# We are going to build a model into our density-modified map, 
#  refine it, fix the sequence and refine again.

# 1. Getting map symmetry
# We will want a symmetry file describing the symmetry operators that have been
# used in our map.  We can get them automatically in about 30 seconds with:

phenix.map_symmetry denmod/denmod_map.ccp4 resolution=2 \
   symmetry_out=denmod_map.ncs_spec

# Now denmod_map.ncs_spec has the 24 symmetry operators for octahedral symmetry
# in the appropriate location of this map.


# 2. Automated model-building using a density-modified map

# We can build a model into our map with map_to_model:

phenix.map_to_model denmod/denmod_map.ccp4 resolution=2 nproc=$nproc \
    ncs_file=denmod_map.ncs_spec \
    pdb_out=map_to_model.pdb  \
    seq_file=seq.dat

# In a half hour or so you should get the model map_to_model.pdb

# 3.  Cut out a piece of density-modified map and compare to build_in model
# You can cut out a piece of your density modified map and look at it along with
# your model this way:

phenix.map_box denmod/denmod_map.ccp4 map_to_model.pdb \
     ignore_symmetry_conflicts=true prefix=denmod_map_A

# This will produce denmod_map_A.ccp4 and denmod_map_A.pdb which you can look 
# at in Coot or Chimera.  The model is mostly pretty good but may need
# some fixing, depending on your version of Phenix

# 4.  Refine the model against the density-modified map

# We can refine the build_in model either against the original or cut-out map. 
#  Let's use the cut-out map.  We can also refine just chain A or the 
#  whole 24-mer. The difference is if you use the whole 24-mer you are 
# going to include inter-chain contacts and if you use a single chain you 
#  will ignore them. Let's use just one chain for now:

phenix.real_space_refine map_to_model.pdb denmod_map_A.ccp4 resolution=2 \
     ignore_symmetry_conflicts=True

# After a couple minutes we have the output model: 
# map_to_model_real_space_refined_000.pdb.  If you look at this model
# in Coot or Chimera you'll see it has moved some side chains into density, but
# that the model still doesn't match the density for residues 154-162.

# 5.  If your model has parts that do not fit, try something like the 
# following. First remove the bad part (here the bad part is residues 154
# through 162):

phenix.pdbtools map_to_model_real_space_refined_000.pdb \
   remove="resseq 154:162" \
   output.file_name=map_to_model_real_space_refined_del.pdb

# 6. Redo the sequence assignment and see if we can fill in the gap. 
# Here the keyword run_assign_sequence=True is used to reassign the
# sequence and then fill in the gap:

echo "Running sequence_from_map with run_assign_sequence=True to fill in gap"
rm -f sequence_from_map.pdb

phenix.sequence_from_map run_assign_sequence=True \
  model_file=map_to_model_real_space_refined_del.pdb \
  seq_file=seq_unique.dat \
  map_file=denmod_map_A.ccp4 \
  pdb_out=sequence_from_map.pdb\
  resolution=2

if ( ! -f sequence_from_map.pdb)then
  echo "Sequence_from_map was not able to fill in the gap in the file map_to_model_real_space_refined_del.pdb"
  echo "Perhaps skip this step or choose another gap to fill."
  echo "Using map_to_model_real_space_refined_000.pdb instead"
  cp -p map_to_model_real_space_refined_000.pdb sequence_from_map.pdb
endif

# This should produce the file sequence_from_map.pdb in a couple minutes.

# 7. We can fix one other part of the model that does not fit well (residues
#  6-9):

phenix.pdbtools sequence_from_map.pdb \
   remove="resseq 6:9" \
   output.file_name=sequence_from_map_del_6-9.pdb

# 8. Redo the sequence assignment and see if we can fill in the gap as in 6 above:

echo "Running sequence_from_map with run_assign_sequence=True to fill in gap"
rm -f sequence_from_map_2.pdb

phenix.sequence_from_map run_assign_sequence=True \
  model_file=sequence_from_map_del_6-9.pdb \
  seq_file=seq_unique.dat \
  map_file=denmod_map_A.ccp4 \
  pdb_out=sequence_from_map_2.pdb \
  resolution=2

if ( ! -f sequence_from_map_2.pdb)then
  echo "Sequence_from_map was not able to fill in the gap in the file sequence_from_map_del_6-9.pdb"
  echo "Perhaps skip this step or choose another gap to fill."
  echo "Using sequence_from_map.pdb instead"
  cp -p sequence_from_map.pdb sequence_from_map_2.pdb
endif

# This should produce the file sequence_from_map_2.pdb in a couple minutes.

# 9. Let's refine one more time:

phenix.real_space_refine sequence_from_map_2.pdb denmod_map_A.ccp4 \
    resolution=2 ignore_symmetry_conflicts=True

# Now have a look at sequence_from_map_2_real_space_refined_000.pdb and 
# denmod_map_A.ccp4 in Coot or Chimera.  The model should fit
# the map quite well.  If there are missing side chains, you can fill them in with:

# 10. Run sequence_from_map

phenix.sequence_from_map sequence_from_map_2_real_space_refined_000.pdb \
    denmod_map_A.ccp4 seq_file=seq_unique.dat resolution=2 \
    pdb_out=sequence_from_map_3.pdb

# and now sequence_from_map_3.pdb should have more side chains built.  You could
# refine once more too:

# 11. Final refinement:

phenix.real_space_refine sequence_from_map_3.pdb denmod_map_A.ccp4 \
    resolution=2 ignore_symmetry_conflicts=True

# and sequence_from_map_3_real_space_refined_000.pdb is the refined model.
#  Next you would go over the model carefully using validation tools and
#  remove any parts that are not ok and fix parts that are not quite right.
#  You can also create the full 24-mer using your map symmetry and the model


finish:
