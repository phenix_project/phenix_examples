#!/bin/csh
#
echo "Testing wizards with model peptide data"
set comparison_date_string = ""
###########################
# get a date string..
echo "`date`" > date
ex date <<EOD
1,%s/  / /g
1,%s/ /_/g
1,%s/:/_/g
write
quit
EOD
set date = `cat date`
echo "DATE STRING : $date"
###########################
#
echo "Generating MAD data..."
$PHENIX/examples/peptide/generate_mad.com > generate_mad.log_$date
#
echo "Generating MIR data..."
$PHENIX/examples/peptide/generate_mir.com > generate_mir.log_$date
#
foreach test_pair  ( \
     "ImportRawData import"  \
     "AutoSol mir"  \
     "AutoSol sad"  \
     "AutoSol mad"  \
     "AutoBuild build"  \
     "AutoBuild rebuild"  \
     "AutoMR mr"  \
     "LigandFit ligand"  \
          )
set test = `echo $test_pair`
set wizard = $test[1-1]
set inp = $test[2-2]
echo "Running $wizard on $inp.inp"
cp $PHENIX/examples/peptide/$inp.inp .
phenix.runWizard $wizard $inp.inp restart > $inp.log_$date
if ($comparison_date_string != "")then
 echo "-------------------------------------------------"
  echo "Comparing $inp.log_$date with $inp.log_$comparison_date_string..."
  diff $inp.log_$comparison_date_string $inp.log_$date  > diff_$inp.log_$date
  cat diff_$inp.log_$date |grep '[a-z]' |wc
  cat diff_$inp.log_$date |grep '[a-z]' 
 echo "-------------------------------------------------"
endif
#
phenix.python<<EOD
import time
time.sleep(1)
EOD
#
end

