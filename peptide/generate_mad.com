#!/bin/csh
setenv CCP4_OPEN UNKNOWN
#  generate 1 mad dataset
cp $PHENIX/examples/peptide/peptide.pdb .
cp $PHENIX/examples/peptide/ha.pdb .
cp $PHENIX/examples/peptide/seq.dat .
cp $PHENIX_SOLVEDIR/p1.sym .
phenix.python<<EOD
import time
time.sleep(1)
EOD
$PHENIX_SOLVEBIN/solve <<EOD
access_file mirima.dat

cell 15 15 15 80 80 80
symfile p1.sym
percent_error 0.5
coordinatefile peptide.pdb
resolution 200 2.
iranseed -199753
solvefile generate_mad.prt

mad_atom se                              ! define the scattering factors...
!
lambda 1
label set 1 with 2 se atoms, lambda 1
wavelength 0.9782             ! wavelength value
fprimv_mad  -10              ! f' value at this wavelength
fprprv_mad  3  
ATOMNAME Se
pdb_xyz_in ha.pdb
!
lambda 2
wavelength 0.977865
fprimv_mad  -7.5
fprprv_mad  5

lambda 3
wavelength 0.8856
fprimv_mad  -2
fprprv_mad  3.5
!
GENERATE_MAD                            ! generate the MAD dataset now.
!
EOD
