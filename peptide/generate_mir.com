#!/bin/csh
#  set CCP4 and SOLVETMPDIR variables:
setenv CCP4_OPEN UNKNOWN
#  command file to generate a 2-deriv MIR dataset
#
cp $PHENIX/examples/peptide/peptide.pdb .
cp $PHENIX_SOLVEDIR/p1.sym .
cp $PHENIX/examples/peptide/seq.dat .
$PHENIX_SOLVEBIN/solve << EOD 
access_file mirima.dat
resolution 20 2.0
cell 15 15 15 80 80 80 
symfile p1.sym
coordinatefile peptide.pdb 
percent_error 10
iranseed -124093
OVERWRITE                               ! overwrite duplicate file names without
                                        !     asking
solvefile generate_mir.prt
!
deriv 1                               ! enter parameters for deriv 1
!
inano                                 ! use anom diffs
atom hg
occ 0.2
bvalue 40. 
xyz -0.620932 -0.0765346 -0.637333
atom hg
occ 0.4 
bvalue 25. 
xyz -0.315098 -0.512727 -0.664318
!
deriv 2
inano
atom hg
occ  0.3
bvalue 30. 
xyz 0.620932 0.5765346 0.337333
atom hg
occ 0.6 
bvalue 25. 
xyz 0.515098 0.212727 0.364318

GENERATE_MIR                            ! generate the MIR dataset now.
!
EOD
