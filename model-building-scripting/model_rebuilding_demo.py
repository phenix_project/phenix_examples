from __future__ import division, print_function
import sys
from libtbx import group_args
import random
from scitbx.array_family import flex
from copy import deepcopy
from libtbx import adopt_init_args
from six.moves import StringIO

def get_map_and_model(
     map_file = None,
     model_file = None,
     target_model_file = None,
     resolution = None,
     verbose = None,
     log = sys.stdout,
     ):
  print("\n",79*"=","\n",20*" ","GET MAP AND MODEL\n",79*"=","\n", file = log)
  print("\nGet a DataManager and read in %s and %s " %(
    map_file, model_file), file = log)

  from iotbx.data_manager import DataManager
  dm = DataManager()
  mmm = dm.get_map_model_manager(map_files = map_file,
    model_file = model_file)
  mmm.add_model_by_id(model_id = 'model',
     model = clean_model(mmm, mmm.model()))
  if target_model_file:
    target_model = dm.get_model(target_model_file)
    target_model = clean_model(mmm, target_model)
  else:
    target_model = mmm.model().deep_copy()
    print("Using starting model as target for comparision", file = log)
  mmm.add_model_by_id(model_id='target_model',model = target_model)

  print("\nSet log to sys.stdout", file = log)
  mmm.set_log(log)
  print("\n",79*"=","\n", file = log)
  if verbose:
    mmm.set_verbose(True)

  if resolution is not None:
    print("Setting resolution to %.2f A " %(resolution), file = log)
    mmm.set_resolution(resolution)

  return mmm

def clean_model(full_size_mmm, model):
  # Make sure model matches full_size_mmm and get rid of everything
  #  except protein and one model
  full_size_mmm.shift_any_model_to_match(model)  # shifts in place
  model = model.apply_selection_string("protein and ( not element h)")
  if model.get_hierarchy().overall_counts().n_models > 1:
    from mmtbx.secondary_structure.find_ss_from_ca \
      import remove_all_models_except_first
    model = full_size_mmm.model_from_hierarchy(
      hierarchy = remove_all_models_except_first(model.get_hierarchy()),
      return_as_model = True)

  # Remove TER/BREAK
  lines = model.model_as_pdb().splitlines()
  lines = remove_ter_records(lines)
  text = "\n".join(lines)
  from iotbx.data_manager import DataManager
  dm = DataManager()
  dm.process_model_str('text',text)
  model = dm.get_model('text')

  return model

def remove_ter_records(lines):
  new_lines=[]
  for line in lines:
    if not line.startswith("TER"):
      new_lines.append(line)
  return new_lines


def box_map_and_model(full_size_mmm,
   selection_string = None,
   verbose = None,
   log = sys.stdout):
  print("\n",79*"=","\n",20*" ",
     "BOXING MAP AND MODEL\n",79*"=","\n", file = log)
  if selection_string is None:
    selection_string = 'all'
  print("\nSelecting '%s' and boxing" %(selection_string), file = log)
  mmm = full_size_mmm.extract_all_maps_around_model(
    selection_string = selection_string,
    soft_mask_around_edges = True)
  if verbose:
    print("Map model manager after boxing:\n",mmm)
  starting_rms_ca = mmm.rmsd_of_matching_residues(ca_only = True,
      target_model_id = 'target_model',
      matching_model_id = 'model')
  starting_rms_all = mmm.rmsd_of_matching_residues(ca_only = False,
      target_model_id = 'target_model',
      matching_model_id = 'model')

  if starting_rms_ca:
    print ("\nStarting CA rmsd of model and target: %.2f A " %(starting_rms_ca),
      file = log)
  if starting_rms_all:
    print ("\nStarting overall rmsd of model and target: %.2f A " %(
    starting_rms_all), file = log)
  return mmm


def refine_model(mmm,
    dummy_run = None,
    refine_cycles = None,
    write_map_and_model = None,
    log = sys.stdout):
  print("\n",79*"=","\n",20*" ","REFINEMENT\n",79*"=","\n", file = log)
  print("\nView or set working resolution", file = log)
  print("Working default resolution: %.2f A " %(mmm.resolution()), file = log)

  print("\nChecking map-model correlation", file = log)
  print("Map-model CC: %.2f" %( mmm.map_model_cc()), file = log)

  print("\nGetting a model-building manager to work with", file = log)
  build = mmm.model_building()

  print ("\nRefinement of model ", file = log)
  if dummy_run:
    refined_model = build.model().deep_copy()
  else:
    refined_model = build.refine(return_model = True,
      refine_cycles = refine_cycles)
  if not refined_model:
    print("No refined model obtained...skipping", file = log)
    return

  print("\nAdd refined model to our map-model manager as 'refined_model'",
     file = log)
  mmm.add_model_by_id(model_id = 'refined_model', model = refined_model)

  print("\nChecking map-model correlation of quick-refined model", file = log)
  cc_mask =  mmm.map_model_cc(model_id = 'refined_model')
  print("Map-model CC: %.2f" %(cc_mask), file = log)
  print("\n",79*"=","\n", file = log)

  if write_map_and_model:
    file_name = 'refined_model_%s.pdb' %(mmm.get_info('text'))
    print("\nWriting out refined model as '%s': " %(file_name), file = log)
    mmm.write_model(model_id = 'refined_model', file_name = file_name)

  return group_args(
     group_args_type = 'one_result refined model',
     model = mmm.get_model_by_id('refined_model'),
     cc_mask = cc_mask)

def replace_side_chains_in_model(mmm,
    dummy_run = None,
    refine_cycles = 3,
    write_map_and_model = None,
    log = sys.stdout):
  print("\n",79*"=","\n",20*" ","REPLACE SIDE CHAINS\n",79*"=","\n", file = log)

  print("\nGetting a model-building manager to work with", file = log)
  build = mmm.model_building()

  print ("\nReplace side chains based on density fit now", file = log)
  sequence = get_target_sequence(mmm, model_id_for_sequence = 'model')

  if dummy_run:
    replace_side_chain_model = build.model().deep_copy()
  else:
    replace_side_chain_model = build.sequence_from_map(
     remove_clashes = False,
     sequence = sequence,
     return_model = True, refine_cycles = refine_cycles)
  if not replace_side_chain_model:
    print("No replace side chain model obtained...skipping", file = log)
    return

  print("\nAdd replace side chains model as 'replace_side_chain_model'",
     file = log)
  mmm.add_model_by_id(model_id = 'replace_side_chain_model', model = replace_side_chain_model)

  print("\nChecking map-model correlation of replace side chain model",
    file = log)
  cc_mask =  mmm.map_model_cc(model_id = 'replace_side_chain_model')
  print("Map-model CC: %.2f" %(cc_mask), file = log)
  print("\n",79*"=","\n", file = log)

  if write_map_and_model:
    file_name = 'replace_side_chain_model_%s.pdb' %(mmm.get_info('text'))
    print("\nWriting out replace side chain model as '%s': " %(file_name),
       file = log)
    mmm.write_model(model_id = 'replace_side_chain_model',
     file_name = file_name)

  return group_args(
     group_args_type = 'one_result replace side chains model',
     model = mmm.get_model_by_id('replace_side_chain_model'),
     cc_mask = cc_mask,
  )

def regularize_model(mmm,
    dummy_run = None,
    refine_cycles = 3,
    write_map_and_model = None,
    log = sys.stdout):
  print("\n",79*"=","\n",20*" ","REGULARIZE MODEL\n",79*"=","\n", file = log)

  sequence = get_target_sequence(mmm,
    model_id_for_sequence = 'model')

  build = mmm.model_building(model_id = 'model')
  if dummy_run:
    regularized_model = build.model().deep_copy()
  else:
    regularized_model = build.regularize_model(
     sequence = sequence,
     return_model = True,
     remove_clashes = False,
     refine_cycles = refine_cycles)
  if not regularized_model:
    print("No regularized model obtained...skipping", file = log)
    return

  print("\nSave the origin shift from original model in regularized model",
     file = log)
  regularized_model.set_shift_cart(mmm.model().shift_cart())

  print("\nAdd regularize-assigned model as 'regularized_model'",
     file = log)
  mmm.add_model_by_id(model_id = 'regularized_model', model = regularized_model)


  print("\nChecking map-model correlation of regularized model",
      file = log)
  cc_mask =  mmm.map_model_cc(model_id = 'regularized_model')
  print("Map-model CC: %.2f" %(cc_mask), file = log)
  print("\n",79*"=","\n", file = log)

  if write_map_and_model:
    file_name = 'regularized_model_%s.pdb' %(mmm.get_info('text'))
    print("\nWriting out regularized model as '%s': " %(file_name),
      file = log)
    mmm.write_model(model_id = 'regularized_model', file_name = file_name)

  return group_args(
     group_args_type = 'one_result regularized model',
     model = mmm.get_model_by_id('regularized_model'),
     cc_mask = cc_mask,
  )

def ca_to_model(mmm,
    dummy_run = None,
    refine_cycles =  3,
    write_map_and_model = None,
    log = sys.stdout):
  print("\n",79*"=","\n",20*" ","CA TO MODEL\n",79*"=","\n", file = log)

  sequence = get_target_sequence(mmm,
    model_id_for_sequence = 'model')

  build = mmm.model_building(model_id = 'model')
  print("\nGetting CA positions from starting model", file = log)

  ca_model = build.model().apply_selection_string(
    "(altloc ' ' or altloc A) and name ca and element c" )
  print("\nMaking sure number of CA positions is same as number of residues..",
     file = log)
  if ca_model.get_sites_cart().size() != len(sequence):
    print("Mismatch of length...CA length: %s  sequence length: %s " %(
      ca_model.get_sites_cart().size(), len(sequence)), file = log)
    return
  else:
    print("Matched length...CA length: %s  sequence length: %s " %(
      ca_model.get_sites_cart().size(), len(sequence)), file = log)

  build.set_defaults(build_methods=['trace_and_build'])
  if dummy_run:
    ca_to_model = build.model().deep_copy()
  else:
    ca_to_model = build.build(
     sequence = sequence,
     starting_model = ca_model,
     use_starting_model_ca_directly = True,
     remove_clashes = False,
     return_model = True,
     mask_around_model = False,
     refine_cycles = refine_cycles)
  if not ca_to_model:
    print("No ca_to_model model obtained...skipping", file = log)
    return
  print("\nSave the origin shift from original model in ca_to model",
     file = log)
  ca_to_model.set_shift_cart(mmm.model().shift_cart())

  print("\nAdd ca_to_assigned model as 'ca_to_model'",
     file = log)
  mmm.add_model_by_id(model_id = 'ca_to_model', model = ca_to_model)


  print("\nChecking map-model correlation of ca_to model",
      file = log)
  cc_mask =  mmm.map_model_cc(model_id = 'ca_to_model')
  print("Map-model CC: %.2f" %(cc_mask), file = log)
  print("\n",79*"=","\n", file = log)

  if write_map_and_model:
    file_name = 'ca_to_model_%s.pdb' %(mmm.get_info('text'))
    print("\nWriting out ca_to model as '%s': " %(file_name),
      file = log)
    mmm.write_model(model_id = 'ca_to_model',
      file_name = file_name)

  return group_args(
     group_args_type = 'one_result ca_to_model ',
     model = mmm.get_model_by_id('ca_to_model'),
     cc_mask = cc_mask,
  )


def replace_segment(mmm,
    refine_cycles= None,
    write_map_and_model = None,
    dummy_run = None,
    log = sys.stdout):
  print("\n",79*"=","\n",20*" ","REPLACE SEGMENT IN MODEL\n",79*"=","\n",
     file = log)

  print ("\nReplace segment in model", file = log)

  print("\nGetting a model-building manager to work with", file = log)
  build = mmm.model_building()

  print("\nReplacing segment now", file = log)
  if dummy_run:
    replaced_segment = build.model().deep_copy()
  else:
    replaced_segment=build.replace_segment(    # rebuild by replacing a segment
     remove_clashes = False,
     refine_cycles=1,)                   # cycles of refinement

    print("\nInsert the segment into model", file = log)
    copy_of_model = build.model().deep_copy()
    new_model = build.insert_fitted_loop(model = copy_of_model)
    if not new_model:
      print("\nNo fitted loop obtained...skipping", file = log)
      return
    replaced_segment = new_model

  print("\nAdd replace-segment model as 'replaced_segment'",
     file = log)
  mmm.add_model_by_id(model_id = 'replaced_segment',
      model = replaced_segment)

  print("\nChecking map-model correlation of rebuild model",
       file = log)
  cc_mask =  mmm.map_model_cc(model_id = 'replaced_segment')
  print("Map-model CC: %.2f" %(cc_mask), file = log)
  print("\n",79*"=","\n", file = log)

  if write_map_and_model:
    file_name = 'replaced_segment_%s.pdb' %(mmm.get_info('text'))
    print("\nWriting out rebuild model as '%s': " %(file_name), file = log)
    mmm.write_model(model_id = 'replaced_segment',
      file_name = file_name)

  return group_args(
     group_args_type = 'one_result replaced_segment',
     model = mmm.get_model_by_id('replaced_segment'),
     cc_mask = cc_mask,
  )

def recombine_model(mmm,
    result = None,
    dummy_run = None,
    refine_cycles = None,
    write_map_and_model = None, log = sys.stdout):
  print("\n",79*"=","\n",20*" ","RECOMBINE MODELS\n",79*"=","\n",
     file = log)

  print ("\nRecombining models", file = log)

  print("\nGetting a model-building manager to work with", file = log)
  build = mmm.model_building()

  print("\nRecombining models now", file = log)
  sequence = get_target_sequence(mmm, model_id_for_sequence = 'model')
  if dummy_run:
    recombined_model = build.model().deep_copy()
  else:
    model_list = []
    for key in result().keys():
      one_result = result.get(key)
      if one_result and hasattr(one_result,'model'):
        print("Adding model '%s'" %(key), file = log)
        model_list.append(one_result.model)
    recombined_model=build.combine_models(    # recombine models
     sequence = sequence,
     model_list = model_list)
    recombined_model=build.refine(model = recombined_model,
      return_model=True,
      refine_cycles = refine_cycles)
    mmm.add_model_by_id(model_id='model', model = recombined_model)

  print("\nChecking map-model correlation of recombined model",
       file = log)
  cc_mask =  mmm.map_model_cc()
  print("Map-model CC: %.2f" %(cc_mask if cc_mask is not None else 0), file = log)
  print("\n",79*"=","\n", file = log)

  if write_map_and_model:
    file_name = 'recombined_model_%s.pdb' %(mmm.get_info('text'))
    print("\nWriting out recombined model as '%s': " %(file_name), file = log)
    mmm.write_model(file_name = file_name)

  return group_args(
     group_args_type = 'one_result recombined_model',
     model = mmm.model(),
     cc_mask = cc_mask,
  )

def highest_scoring_model(mmm,
    result = None,
    dummy_run = None,
    write_map_and_model = None, log = sys.stdout):
  print("\n",79*"=","\n",20*" ","PICK HIGHEST-SCORING MODEL\n",79*"=","\n",
     file = log)

  print ("\n  Highest-scoring model", file = log)

  print("\nGetting a model-building manager to work with", file = log)
  build = mmm.model_building()

  print("\nPicking highest-scoring model now", file = log)
  if dummy_run:
    pass
  else:
    # Pick highest scoring model that also has same atoms as original
    #   so one_result.rmsd_all is not None
    best_result = None
    for key in result().keys():
      one_result = result.get(key)
      if one_result and hasattr(one_result,'model'):
        if (one_result.rmsd_all is not None):
          if best_result is None or one_result.cc_mask > best_result.cc_mask:
            best_result = one_result
    if best_result:
      print("best result: '%s'" %(key), file = log)
      mmm.add_model_by_id(model_id = 'model', model = best_result.model)

  print("\nChecking map-model correlation of highest_score model",
       file = log)
  cc_mask =  mmm.map_model_cc()
  print("Map-model CC: %.2f" %(cc_mask), file = log)
  print("\n",79*"=","\n", file = log)

  if write_map_and_model:
    file_name = 'highest_score_model_%s.pdb' %(mmm.get_info('text'))
    print("\nWriting out highest_score model as '%s': " %(file_name), file = log)
    mmm.write_model(file_name = file_name)

  return group_args(
     group_args_type = 'one_result highest_score_model',
     model = mmm.model(),
     cc_mask = cc_mask,
  )

def summarize_models(mmm,
    matching_segment_info_list = None,
    use_default_segment_list = True, log = sys.stdout):

  if not matching_segment_info_list and use_default_segment_list:
     matching_segment_info_list = mmm.get_info('matching_segment_info_list')

  if not matching_segment_info_list:
    matching_segment_info_list = [None]

  for info in matching_segment_info_list:
    if info is not None:
      print("\nSummary of segment_id %s" %(info.id), file = log)
      model =  info.matching_model
      target_model = info.target_model
    else:
      print("\nSummary of models", file = log)
      model =  mmm.model()
      target_model = mmm.get_model_by_id(model_id = 'target_model')


    ph_model = model.get_hierarchy()
    print("Model has %s residues (chain %s resseq %s:%s) " %(
     ph_model.overall_counts().n_residues,
     ph_model.first_chain_id(),
     ph_model.first_resseq_as_int(),
     ph_model.last_resseq_as_int()), file = log)

    seq_model = ph_model.as_sequence(as_string=True)

    ph_target_model = target_model.get_hierarchy()
    print("Target model has %s residues (chain %s resseq %s:%s) " %(
     ph_target_model.overall_counts().n_residues,
     ph_target_model.first_chain_id(),
     ph_target_model.first_resseq_as_int(),
     ph_target_model.last_resseq_as_int()), file = log)
    seq_target_model = ph_target_model.as_sequence(as_string=True)

    if seq_model == seq_target_model:
     print("Model and target model sequences with %s residues match" %(
      len(seq_model)), file = log)
    else:
     print("\nModel and target sequences with "+
       "%s and %s residues do not match" %(
        len(seq_model),len(seq_target_model)), file = log)
     print("Model: %s" %(seq_model), file = log)
     print("Model: %s" %(seq_target_model), file = log)

def get_target_sequence(mmm,
   model_id_for_sequence = 'model',
   log = sys.stdout):
  print ("\nGet the target sequence...", file = log)
  sequence = mmm.get_model_by_id(
    model_id_for_sequence).as_sequence(as_string=True)
  print ("\nTarget sequence is '%s'" %(sequence), file = log)
  return sequence

def get_crossover_point(working_ca,
    local_ca,
    very_close_ca,
    close_ca,
    n_end = None,
    last = None):
  ''' figure out first place where working_ca is close to local_ca '''
  assert working_ca.size() == local_ca.size()
  if last:
    working_ca = list(working_ca)
    local_ca = list(local_ca)
    working_ca.reverse()
    local_ca.reverse()
    working_ca = flex.vec3_double(working_ca)
    local_ca = flex.vec3_double(local_ca)
  diffs = working_ca - local_ca
  distances = diffs.norms()

  # Cross over within n_end of the end if possible (last position
  #   closer than very_close_ca)
  #  Otherwise cross over at first position within close_ca
  best_crossing_point = None
  ok_crossing_point = None
  for i in range(working_ca.size()):
    if distances[i] <= very_close_ca and i < n_end: # cross at end
      best_crossing_point = i   # last very good point within n_end
    if distances[i] <= close_ca and (ok_crossing_point is None):
      ok_crossing_point = i  # first ok point
  if best_crossing_point is None:
    best_crossing_point = ok_crossing_point
  if best_crossing_point is None:
    return None
  elif (not last):
    return best_crossing_point
  else:
    return (working_ca.size() - best_crossing_point) - 1

def chain_info_is_identical(info):
  ''' Determine if target and matching information is identical'''
  for key in [
    'start_resseq',
    'end_resseq',
    'chain_id',]:
    if info.get('target_model_%s' %(key)) != info.get('matching_model_%s' %(key)):
      return False
  return True

def chain_info(working_mmm, model, include_cc_mask = True):
  if include_cc_mask:
    text = ", map_cc = %.2f " %(working_mmm.map_model_cc(model = model))
  else:
    text = ""
  return "(chain %s resseq %s:%s), %s residues %s)" %(
       model.first_chain_id(),
       model.first_resseq_as_int(),
       model.last_resseq_as_int(),
       model.overall_counts().n_residues,
       text)

def select_parts_of_models_that_match(mmm,
    target_model_id='target_model',
    matching_model_id='model',
    max_gap = 0,
    one_to_one = True,
    residue_names_must_match = True,
    minimum_length = 5,
    verbose = None,
    log = sys.stdout):

  print("\n",79*"=","\n",20*" ","SELECT PARTS OF MODELS THAT MATCH\n",
      79*"=","\n", file = log)

  matching_segment_info_list = mmm.select_matching_segments(
      target_model_id = target_model_id,
      minimum_length = minimum_length,
      max_gap = max_gap,
      one_to_one = one_to_one,
      residue_names_must_match = residue_names_must_match,
      matching_model_id = matching_model_id)

  print("\nTotal of %s segments with minimum length of %s found:\n" %(
     len(matching_segment_info_list), minimum_length), file = log)

  mmm.add_to_info(
     item_name = 'matching_segment_info_list',
     item = matching_segment_info_list,
      )

  if verbose:
    print("\nModel summary after matching:", file = log)
    summarize_models(mmm, log = log)

  return mmm

def run(
   first_set = 1,              # which set to start at
   number_of_sets = 1,          # how many set to run
   random_seed = 178814,        # random seed
   map_file = None,
   model_file = None,
   target_model_file = None,
   mask_map_around_selection = None,
   write_map_and_model = None,
   read_highest_scoring = None,
   recombine_models = None,
   pick_highest_scoring_model = None,
   create_new_model = None,
   run_refine_model = None,
   run_replace_side_chains_in_model = None,
   run_regularize_model = None,
   run_replace_segment = None,
   run_ca_to_model = None,
   set_size = 15,
   overlap = None,
   n_residues_on_each_end = 3,
   minimum_length = 5,
   set_size_variability = 5,
   nproc = 1,
   n_jobs_to_switch_to_parallel_jobs = 3,
   refine_cycles = 3,
   overall_refine_cycles = 5,
   resolution = None,
   verbose = None,
   dummy_run = None,
   log = sys.stdout):

  # Save parameters
  kw_dict = group_args(
   write_map_and_model = write_map_and_model,
   recombine_models = recombine_models,
   pick_highest_scoring_model = pick_highest_scoring_model,
   run_refine_model = run_refine_model,
   run_replace_side_chains_in_model = run_replace_side_chains_in_model,
   run_regularize_model = run_regularize_model,
   run_replace_segment = run_replace_segment,
   run_ca_to_model = run_ca_to_model,
   n_residues_on_each_end = n_residues_on_each_end,
   refine_cycles = refine_cycles,
   verbose = verbose,
   dummy_run = dummy_run)
  kw_dict = kw_dict()  # make it a dict, not group_args


  random.seed(random_seed)

  print("\nDemo of options for rebuilding sections of models", file = log)

  # Read in map and models
  full_size_mmm = get_map_and_model(
    map_file = map_file,
    model_file = model_file,
    target_model_file = target_model_file,
    resolution = resolution,
    verbose = verbose,
    log = log)

  working_mmm = full_size_mmm.deep_copy()  # to be updated as we go
  if write_map_and_model:
    file_name = 'working_model_start.pdb'
    working_mmm.write_model(file_name)

  # Select parts of target and working models that match
  full_size_mmm = select_parts_of_models_that_match(full_size_mmm,
    verbose = verbose,
    minimum_length = minimum_length,
    log = log)

  mmm_dc = full_size_mmm.deep_copy()

  # Decide what segments to run
  run_info_list = get_run_info_list(full_size_mmm,
   first_set = first_set,
   number_of_sets = number_of_sets,
   set_size = set_size,
   overlap = overlap,
   minimum_length = minimum_length,
   set_size_variability = set_size_variability,
   )

  print("\nTotal of %s segments to run:" %(len(run_info_list)), file = log)

  if read_highest_scoring:
    results = run_read_highest_scoring(full_size_mmm,
      run_info_list,
      log = log)

  else:
    results = run_jobs(
     n_jobs_to_switch_to_parallel_jobs = n_jobs_to_switch_to_parallel_jobs,
       full_size_mmm = full_size_mmm,
       nproc = nproc,
       verbose = verbose,
       kw_dict = kw_dict,
       run_info_list = run_info_list,
       log = log)


  # All done ...analyze results

  analyze_results(full_size_mmm, results = results, log = log)

  if create_new_model: #  Make a new model from the pieces
    new_model = make_composite_model(working_mmm,
      results,
      dummy_run = dummy_run,
      overall_refine_cycles = overall_refine_cycles,
      log = log)
  else:
    new_model = None
  return new_model

def run_read_highest_scoring(full_size_mmm,
      run_info_list,
      log = sys.stdout):
  # Read all the highest_scoring info directly and skip everything else
  results = []
  for info in run_info_list:
    result = group_args(
      highest_scoring_model = None,
      run_info = info,
     )
    file_name = 'highest_score_model_%s.pdb' %(info.text)
    print("\nReading highest_score model from '%s': " %(file_name), file = log)
    from iotbx.data_manager import DataManager
    dm = DataManager()
    model = dm.get_model(file_name)
    full_size_mmm.shift_any_model_to_match(model)
    result.highest_scoring_model = group_args(
       model = model)
    results.append(result)
  return results

def run_jobs(
       n_jobs_to_switch_to_parallel_jobs = None,
       full_size_mmm = None,
       nproc = None,
       verbose = None,
       kw_dict = None,
       run_info_list = None,
       log = sys.stdout):
  # Decide where the parallel processing will be...typically can only use a few
  #   processors in parallel on a single run, so if more than a few processors
  #   and more than a few runs, do the runs in parallel

  if len(run_info_list) > n_jobs_to_switch_to_parallel_jobs:
    nproc_for_parallel_run = nproc
    nproc = 1
    print("\nRunning the %s jobs in parallel (nproc=%s), each with nproc=1" %(
     len(run_info_list), nproc_for_parallel_run), file = log)
  else:
    nproc_for_parallel_run = 1
    print("\nRunning the %s jobs each with nproc=%s" %(
     len(run_info_list), nproc), file = log)

  print ("\n\nRunning each segment now...", file = log)
  index_list=[]
  for i in range(len(run_info_list)):
    index_list.append({'i':i})

  if nproc_for_parallel_run == 1 or verbose:
    local_log = log
  else:
    local_log = None

  from libtbx.easy_mp import run_parallel
  results = run_parallel(
     method = 'multiprocessing',
     nproc = nproc_for_parallel_run,
     target_function = run_one_set_as_class(
       kw_dict = kw_dict,
       full_size_mmm = full_size_mmm,
       log = local_log,
       nproc = nproc,
       run_info_list = run_info_list),
     preserve_order=False,
     kw_list = index_list)

  # Note log for each result is in result.log
  return results

def group_results_within_id(set_of_results):
  result = None
  for r in set_of_results:
    if not result:
      result = r
    else:
      r_as_dict = r()
      for key in r_as_dict.keys():
        if result.get(key) is None and r_as_dict.get(key) is not None:
          result.add(key = key, value = r_as_dict.get(key))
  return result

def set_up_one_run(mmm, run_info,
   write_map_and_model = None,
   verbose = None,
   log = sys.stdout):
    print("\n\nSegment %s  Sub-segment %s Model: %s %s:%s  Target: %s %s:%s" %(
     run_info.segment_id,
     run_info.sub_segment_id,
     run_info.model_chain_id,
     run_info.model_start_resseq,
     run_info.model_end_resseq,
     run_info.target_model_chain_id,
     run_info.target_model_start_resseq,
     run_info.target_model_end_resseq,
        ),
     file = log)

    info = mmm.get_info('matching_segment_info_list')[run_info.segment_id]
    model = info.matching_model
    target_model = info.target_model

    # select part for this test
    model = model.apply_selection_string(
         "chain %s and resseq %s:%s" %(
      run_info.model_chain_id,
      run_info.model_start_resseq,
      run_info.model_end_resseq,))
    target_model = target_model.apply_selection_string(
        "chain %s and resseq %s:%s" %(
      run_info.target_model_chain_id,
      run_info.target_model_start_resseq,
      run_info.target_model_end_resseq,))

    full_size_mmm = mmm.customized_copy(
     model_dict = {'model':model, 'target_model':target_model})

    local_mmm = box_map_and_model(full_size_mmm,
      selection_string = None,
      verbose = verbose,
      log = log)
    if write_map_and_model:
      file_name = 'working_model_%s.pdb' %( run_info.text)
      print("Writing working model to %s" %(file_name), file = log)
      local_mmm.write_model(model_id = 'model', file_name = file_name)

      file_name = 'target_model_%s.pdb' %(run_info.text)
      print("Writing target model to %s" %(file_name), file = log)
      local_mmm.write_model(model_id = 'target_model', file_name = file_name)

      file_name = 'working_map_%s.ccp4' %(run_info.text)
      print("Writing working map to %s" %(file_name), file = log)
      local_mmm.write_map(file_name = file_name)

    local_mmm.add_to_info('text', run_info.text)
    return local_mmm

def update_working_model(mmm,
   result = None,
   n_residues_on_each_end = None,
   take_updated_if_within_cc_of = None,
   write_map_and_model = None,
   log = sys.stdout):

  if result.highest_scoring_model.model is result.starting_model.model:
    print("Starting model is highest-scoring...no insertion to do", file = log)
    return mmm

  print("\nMerging new segment into working model and refining", file = log)
  working_model = mmm.model()
  new_model = working_model.deep_copy()

  segment_to_insert = result.highest_scoring_model.model.deep_copy()

  run_info = result.run_info

  # Need to shift result.highest_scoring_model.model to match location of
  #   mmm.  Changes in place...
  mmm.shift_any_model_to_match(segment_to_insert)

  from phenix.model_building.fit_loop import insert_loop_in_model
  insert_loop_in_model(  # modifies new_model directly. Does not affect mmm
      model = new_model,
      loop_to_insert_with_n_residues_on_each_end = segment_to_insert,
      chain_id = run_info.model_chain_id,
      last_resno_before_loop = (
        run_info.model_start_resseq + n_residues_on_each_end) - 1,
      first_resno_after_loop = (
        run_info.model_end_resseq - n_residues_on_each_end)  + 1,
      residues_on_each_end = n_residues_on_each_end,
     )

  working_cc = mmm.map_model_cc(model = working_model)
  new_cc = mmm.map_model_cc(model = new_model)
  if new_cc >= working_cc - take_updated_if_within_cc_of :
    print("\nNew model has CC = %.3f, better than previous of %.3f - %.3f" %(
       new_cc, working_cc, take_updated_if_within_cc_of), file = log)
    mmm.add_model_by_id(model_id='model', model = new_model)
    if write_map_and_model:
      file_name = 'working_model_%s.pdb' %(run_info.id)
      print("Writing current working model to %s" %(file_name), file = log)
      mmm.write_model(file_name)
  else:
    print("\nNew model has CC "+
     "= %.3f, no better than previous of %.3f (skipping)" %( new_cc,
    working_cc), file = log)

  return mmm

def make_composite_model(working_mmm,results,
    dummy_run = None,
    overall_refine_cycles = 5,
    log = sys.stdout):
  print ("\n\n",78*"=","\n","   CREATING COMPOSITE MODEL",
     "\n",78*"=","\n",file = log)

  # Model as input
  original_working_model = working_mmm.model().deep_copy()
  working_model = working_mmm.model().deep_copy()

  working_mmm.add_model_by_id(model_id = 'working', model = working_model)

  current_map_cc = working_mmm.map_model_cc(model_id = 'working')
  print("\nStarting map-model correlation for %s residues: %.3f " %(
     working_model.get_hierarchy().overall_counts().n_residues,
     current_map_cc), file = log)

  # Keeping fixed number of residues and sequence
  sequence = get_target_sequence(working_mmm, model_id_for_sequence = 'model')

  for result in results:
    if not result: continue
    if not result.get('highest_scoring_model'): continue
    highest_scoring = result.get('highest_scoring_model')
    if not highest_scoring or not highest_scoring.get('model'): continue
    local_model = highest_scoring.get('model')
    run_info = result.run_info
    local_original_model = original_working_model.apply_selection_string(
         "chain %s and resseq %s:%s" %(
      run_info.model_chain_id,
      run_info.model_start_resseq,
      run_info.model_end_resseq,))

    merge_model_in(working_mmm, local_model, run_info, log = log)

  current_map_cc = working_mmm.map_model_cc(model_id = 'working')
  print("\nFull map-model correlation for %s residues: %.3f " %(
     working_model.get_hierarchy().overall_counts().n_residues,
     current_map_cc), file = log)

  # Refine the final model
  build = working_mmm.model_building(model_id='working')
  print ("\nRefining final model ", file = log)
  if dummy_run:
    refined_model = build.model().deep_copy()
  else:
    refined_model = build.refine(return_model = True,
         refine_cycles = overall_refine_cycles)

  working_mmm.add_model_by_id(model_id='working', model= refined_model)
  current_map_cc = working_mmm.map_model_cc(model_id = 'working')
  print("\nFinal map-model correlation for %s residues: %.3f " %(
     working_model.get_hierarchy().overall_counts().n_residues,
     current_map_cc), file = log)

  file_name = 'final_model.pdb'
  print("\nWriting final model to %s" %(file_name), file = log)
  working_mmm.write_model(model_id = 'working', file_name = file_name)

  return working_mmm.get_model_by_id(model_id = 'working')

def merge_model_in(
      working_mmm,
      local_model,
      run_info,
      merge_local_model_at_ends = True,
      very_close_ca = 0.5,
      close_ca = 1.,
      n_end = 3,
      log = sys.stdout):
    '''
     Merge local_model into working_mmm 'working'
     Expects local_model to match in chain ID and residue numbers
     Pick the best parts of each but make sure ends match working
     Must match CA at crossover with close_ca, optimally very_close_ca
     Must crossover within n_end of end if at all possible.
    '''

    working_model = working_mmm.get_model_by_id(model_id = 'working')
    selection_string = "chain %s and resseq %s:%s" %(
     run_info.model_chain_id,
     run_info.model_start_resseq,
     run_info.model_end_resseq,)
    working_segment= working_model.apply_selection_string(selection_string)
    working_mmm.shift_any_model_to_match(local_model)



    working_ca = working_segment.apply_selection_string(
      "(altloc ' ' or altloc A) and name ca and element c").get_sites_cart()
    local_ca = local_model.apply_selection_string(
      "(altloc ' ' or altloc A) and name ca and element c").get_sites_cart()

    print("\nCrossing working model %s" %(chain_info(working_mmm,
         working_segment)),
       "\nwith local model %s" %(chain_info(working_mmm,local_model)),
       file = log)

    if working_ca.size() < 1 or working_ca.size() != local_ca.size():
      print("Local model has different size...(local %s vs existing %s)" %(
        local_ca.size(),working_ca.size()), file = log)
      return


    # Find CA closest to ends where we can crossover
    first_crossover_point = get_crossover_point(
      working_ca, local_ca, very_close_ca, close_ca)
    last_crossover_point = get_crossover_point(
      working_ca, local_ca, very_close_ca, close_ca,
      n_end = n_end,
       last = True)

    start_resseq = local_model.first_resseq_as_int()
    if start_resseq is None:
      print("No local model to work with", file = log)
      return

    if (first_crossover_point is None) or ( last_crossover_point is None) or (
        last_crossover_point < first_crossover_point + 2):
      print("No useful region to cross over (crossover points: %s - %s) " %(
         first_crossover_point,
         last_crossover_point), file = log)
      return

    print("Region to cross over ...(crossover points: %s - %s) " %(
        start_resseq + first_crossover_point,
        start_resseq + last_crossover_point), file = log)


    selection = "resseq %s: %s" %(
      start_resseq + first_crossover_point,
      start_resseq + last_crossover_point,)

    local_model = local_model.apply_selection_string(selection)

    # Now just transfer the hierarchy from local_model into working_model
    from phenix.model_building.fit_loop import insert_loop_in_model
    insert_loop_in_model(  # modifies new_model directly
      model = working_model,
      loop_to_insert_with_n_residues_on_each_end = local_model,
      chain_id = run_info.model_chain_id,
      last_resno_before_loop =( run_info.model_start_resseq +
         first_crossover_point),
      first_resno_after_loop = (run_info.model_start_resseq +
         last_crossover_point),
      residues_on_each_end = 1,
     )

    file_name = 'working_model.pdb'
    print("\nWriting working model to %s" %(file_name), file = log)
    working_mmm.write_model(model_id = 'working', file_name = file_name)

def analyze_results(mmm, results = None, only_complete_results = True,
    log = sys.stdout):
  print ("\n\n",78*"=","\n","   SUMMARY OF ALL RESULTS", file = log, end = "")
  if only_complete_results:
   print(" (complete results only)", file = log, end = "")
  print("\n",78*"=", file = log)

  result_dict = {}
  for result in results[:1]:
    for key in result().keys():
      one_result = result.get(key)
      if (not one_result) or (not hasattr(one_result,'model')): continue
      result_dict[key] = group_args(
          group_args_type = 'summary rmsd CA and rmsd all ',
          rmsd_ca_list = [],
          rmsd_all_list = [],
        )

  # Get all the ca and all rmsd values in lists...None if missing
  useful_keys = []
  for result in results:
    for key in result().keys():
      summary_rmsd = result_dict.get(key)
      if not summary_rmsd: # not a useful key...skip
        continue
      one_result = result.get(key)
      if one_result and one_result.get('rmsd_ca'):
        summary_rmsd.rmsd_ca_list.append(one_result.rmsd_ca)
        summary_rmsd.rmsd_all_list.append(one_result.rmsd_all)
        if not key in useful_keys: useful_keys.append(key)
      else:
        summary_rmsd.rmsd_ca_list.append(None)
        summary_rmsd.rmsd_all_list.append(None)

  for key in useful_keys:
    print("\nSummary by set for '%s':\n" %(key), file = log)
    print("    MODEL                    RMSD (CA)     RMSD (ALL)    CC_mask",
      " Model residues", file = log)
    rmsd_ca_list = flex.double()
    rmsd_all_list = flex.double()
    cc_mask_list = flex.double()
    for result in results:
      if only_complete_results and (
          not is_complete_result(result, useful_keys)):
        continue
      one_result = result.get(key)
      if not one_result: continue
      run_info = result.run_info
      if one_result.rmsd_ca is not None:
        rmsd_ca_list.append(one_result.rmsd_ca)
      if one_result.rmsd_all is not None:
        rmsd_all_list.append(one_result.rmsd_all)
      if one_result.cc_mask is not None:
        cc_mask_list.append(one_result.cc_mask)
      text = result.run_info.text
      print_one_result(key, one_result, text = text, log = log)

    key = 'overall'
    overall_one_result = group_args(
      key = key,
      rmsd_ca = rmsd_ca_list.min_max_mean().mean,
      rmsd_all = rmsd_all_list.min_max_mean().mean,
      cc_mask = cc_mask_list.min_max_mean().mean,
      )
    print(file = log)
    print_one_result(key, overall_one_result, log = log)

def is_complete_result(result, useful_keys):
  for key in useful_keys:
    if not result.get(key):
      return False
  return True

def get_run_info_list(mmm,
   first_set = 1,
   number_of_sets = 1,
   set_size = 15,
   overlap = None,
   set_size_variability = 5,
   minimum_length= 5,
   maximum_number_of_sets_per_segment = 1000):
  run_info_list = []
  matching_segment_info_list = mmm.get_info('matching_segment_info_list')
  if not matching_segment_info_list:
    return run_info_list

  set_number = -1

  for info in matching_segment_info_list:
    previous_end = None
    n = info.target_model_end_resseq - info.target_model_start_resseq + 1
    for i in range (maximum_number_of_sets_per_segment):
      if not previous_end:
        start = 0
      elif overlap and overlap > 0:
        start = max(start + 1, previous_end - overlap)
      else:
        start = previous_end
      size = max(minimum_length, set_size + random.randint(
        -set_size_variability,set_size_variability))
      end = min(n-1, start + size)
      size = end - start + 1
      previous_end = end
      if size < set_size - set_size_variability:
        break
      set_number += 1
      if ((first_set is None) or (set_number >= first_set )) and (
        (number_of_sets is None) or (first_set is None) or (
          set_number <= first_set + number_of_sets -1)):
        run_info = group_args(
          group_args_type = "One residue range (%s:%s) for segment %s " %(
            start,end,info.id),
          id = set_number,
          segment_id = info.id,
          sub_segment_id = i,
          target_model_start_resseq = info.target_model_start_resseq + start,
          target_model_end_resseq = info.target_model_start_resseq + end,
          target_model_chain_id = info.target_model_chain_id,
          model_start_resseq = info.matching_model_start_resseq + start,
          model_end_resseq = info.matching_model_start_resseq + end,
          model_chain_id = info.matching_model_chain_id,
         )
        run_info.text = "%s%s-%s%s" %(
           run_info.model_chain_id,
           run_info.model_start_resseq,
           run_info.model_chain_id,
           run_info.model_end_resseq)
        run_info_list.append(run_info)

  return run_info_list

def run_one_set(
   full_size_mmm,
   run_info = None,
   run_refine_model = None,
   run_replace_side_chains_in_model = None,
   run_regularize_model = None,
   run_replace_segment = None,
   run_ca_to_model = None,
   nproc = 1,
   verbose = None,
   dummy_run = None,
   write_map_and_model = None,
   recombine_models = None,
   pick_highest_scoring_model = None,
   n_residues_on_each_end = None,
   refine_cycles = None,
   log = sys.stdout):

  if log is None:
    log = StringIO()
    log_type = 'StringIO'
  else:
    log_type = 'stream'

  print ("\n\n",78*"=","\n",
     "   RUNNING ONE TEST  Segment_id: %s  Residues: %s" %(
        run_info.id, run_info.text), "\n",78*"=", file = log)

  mmm = set_up_one_run(full_size_mmm, run_info,
       write_map_and_model = write_map_and_model,
       verbose = verbose,
       log = log)

  result = group_args(
    starting_model = group_args(
      group_args_type = 'one_result starting model',
      model = mmm.model().deep_copy(),
      cc_mask = mmm.map_model_cc(),
     ),
    refined_model = None,
    replace_side_chain_model = None,
    regularized_model = None,
    model_from_ca = None,
    rebuilt_model = None,
    run_info = run_info,
   )

  print("\n Summary before rebuilding ",file = log)
  summarize_result(mmm, result = result, log = log)

  if run_refine_model:
    print("\nSet resolution, refine model", file = log)
    result.refined_model = refine_model(mmm.deep_copy(),
       dummy_run = dummy_run,
       write_map_and_model = write_map_and_model,
       refine_cycles = refine_cycles,
       log = log)

  if run_replace_side_chains_in_model:
    print("\n Replace side chains", file = log)
    result.replace_side_chain_model = replace_side_chains_in_model(
      mmm.deep_copy(),
      dummy_run = dummy_run,
      write_map_and_model = write_map_and_model,
      refine_cycles = refine_cycles,
      log = log)

  if run_regularize_model:
    print("\n Regularize model (creates new backbone model)", file = log)
    result.regularized_model = regularize_model(mmm.deep_copy(),
       dummy_run = dummy_run,
       write_map_and_model = write_map_and_model,
       refine_cycles = refine_cycles,
       log = log)

  if run_ca_to_model:
    print("\n Convert CA trace to model ", file = log)
    result.model_from_ca  = ca_to_model(mmm.deep_copy(),
       dummy_run = dummy_run,
       write_map_and_model = write_map_and_model,
       refine_cycles = refine_cycles,
       log = log)

  if run_replace_segment:
    print("\n Replace segment in model ", file = log)
    result.rebuilt_model = replace_segment(mmm.deep_copy(),
       dummy_run = dummy_run,
       write_map_and_model = write_map_and_model,
       refine_cycles = refine_cycles,
       log = log)

  if recombine_models:
    print("\n Recombining models ", file = log)
    result.recombined_model = recombine_model(mmm.deep_copy(),
       result = result,
       dummy_run = dummy_run,
       write_map_and_model = write_map_and_model,
       refine_cycles = refine_cycles,
       log = log)


  if pick_highest_scoring_model:
    print("\n Picking highest_scoring model", file = log)
    calculate_rmsd_values(mmm,result)  # need these
    result.highest_scoring_model = highest_scoring_model(mmm.deep_copy(),
       result = result,
       dummy_run = dummy_run,
       write_map_and_model = write_map_and_model,
       log = log)

  print("\n Summary after rebuilding ",file = log)
  summarize_result(mmm, result = result, log = log)

  if log_type == 'StringIO':
    result.log = log.getvalue()
  else:
    result.log = None

  return result

def calculate_rmsd_values(mmm, result, skip_if_present=True):
  for key in result().keys():
    one_result = result.get(key)
    if one_result and hasattr(one_result,'model'):
      if skip_if_present and hasattr(one_result,'rmsd_ca'):
        continue # already done
      one_result.rmsd_ca = mmm.rmsd_of_matching_residues(ca_only = True,
          target_model_id = 'target_model',
          matching_model = one_result.model)
      one_result.rmsd_all = mmm.rmsd_of_matching_residues(ca_only = False,
        target_model_id = 'target_model',
        matching_model = one_result.model)


def summarize_result(mmm, result = None, log = sys.stdout):
  text = result.run_info.text

  calculate_rmsd_values(mmm, result)

  print("\n",78*"=","\n","      SUMMARY\n", file = log)
  print("    MODEL                    RMSD (CA)     RMSD (ALL)    CC_mask",
      " Model residues", file = log)
  for key in result().keys():
    one_result = result.get(key)
    if (not one_result ) or (not hasattr(one_result,'model')):
       continue

    print_one_result(key, one_result, text = text, log = log)

def print_one_result(key, one_result, text = "", log = sys.stdout):
    if one_result.rmsd_ca is not None and one_result.rmsd_all is not None:
      print(" %26s     %.2f A      %.2f A        %.2f   %s" %(
        key, one_result.rmsd_ca, one_result.rmsd_all,one_result.cc_mask,
         text), file = log)
    elif  one_result.rmsd_ca is not None:
      print(" %26s     %.2f A      NA            %.2f   %s" %(
        key, one_result.rmsd_ca, one_result.cc_mask,text), file = log)

class run_one_set_as_class:
  def __init__(self,
      kw_dict,
      full_size_mmm,
      log,
      nproc,
      run_info_list):

    adopt_init_args(self, locals())

  def __call__(self, i):

    result = run_one_set(self.full_size_mmm,
      run_info = self.run_info_list[i],
      log = self.log,
      nproc = self.nproc,
      **self.kw_dict)

    return result

if __name__=="__main__":
    run(
      set_size = 15,               # typical size of a segment to work on
      overlap = 6,                 # overlap between sets
      set_size_variability = 5,    # how much to randomly vary the set size
      first_set = 1,               # which set to start at
      minimum_length = 8,          # minimum length of a segment to work on
      number_of_sets = None,          # how many set to run (all if set to None)
      random_seed = 278814,        # random seed
      map_file = '3j9e_6240_box.ccp4',          # working map
      model_file = 'deeptrace_formatted.pdb',   # starting model
      target_model_file = '3j9e_6240_box.pdb',  # the answer, if available
      run_refine_model = True,              # Run refinement test
      run_replace_side_chains_in_model = True,  # Run replace side chains test
      run_regularize_model = True,          # Run regularize model test
      run_replace_segment = True,             # Run replace segment test
      run_ca_to_model = True,               # Run CA-to-model test
      recombine_models = True,                # recombine models
      pick_highest_scoring_model = True,      # pick highest_scoring model
      create_new_model = True,                # make a new composite model
      nproc = 64,                               # Number of processors
      refine_cycles = 3,                       # Refinement cycles for a segment
      overall_refine_cycles = 5,               # Refinement cycles overall
      resolution = None,                       # Optional nominal resolution
      mask_map_around_selection = False,       # mask the map nearby
      write_map_and_model = True,             # write out files
      read_highest_scoring = False,              # read in files from prev run
      verbose = False,                        # verbose output
      dummy_run = False,                      # Just substitute starting
    )
