This example is density modification of a cryo-EM map.
The data are half-maps for beta-galactosidase, obtained from a subset of the
images used to create the 2.2 A map (EMD-2984).  For comparison,
the images were fully processed to yield a full map 

The files are:

half_map_1.ccp4,half_map_2.ccp4  -- half maps 
bgal.fasta  -- sequence file
bgal.pdb  -- known model
emd_2984_3.5A.ccp4 -- high-resolution map truncated at 3.5 A
full_map.ccp4 -- full reconstruction

Set up tutorial data
====================
In the Phenix GUI:
New project/Set up tutorial data/Select a dataset
Choose: Density modification (Beta-galactosidase at 3.9 A)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.


Running density modification in the Phenix GUI

In the Phenix GUI, click on "Cryo EM", "Map improvement",
 and then "Resolve CryoEM"

Select the "Add files" button and load the two half maps and sequence file.

Hit Run and a density-modified map will be calculated.
Also a local resolution map showing the resolution of the sharpened map will
be generated.

Output maps:
initial_map.ccp4        -- starting average map (sharpened)
denmod_map.ccp4         -- density-modified map
denmod_half_map_1.ccp4        -- density-modified half-map
denmod_half_map_2.ccp4        -- density-modified half-map
denmod_resolution_map.ccp4    -- local resolution map

The density-modified map is your improved map. The local
resolution map is a map that shows the resolution at each point in the map.
It is normally used to color the sharpened map.

If you open in ChimeraX the sharpened map will be displayed, colored by
local resolution.  If you want to change the coloring scheme, you can edit the
command shown in the GUI window and paste it into the ChimeraX command box. You
can also edit this command to color other maps you load into ChimeraX.

To run one cycle of density modification from the command line:

phenix.resolve_cryo_em half_map_1.ccp4 half_map_2.ccp4 seq_file=bgal.fasta \
   cycles=1 

This will produce the files:

denmod_map.ccp4  -- density-modified map
denmod_half_map_1.ccp4,denmod_half_map_2.ccp4 -- density-modified half-maps

In Coot you can load in the model (bgal.pdb) and compare the initial map
 (full_map.ccp4) and the density modified map(denmod_map.ccp4). 

If you just center in Coot on "chain A" and zoom in on some of 
the aromatic residues you see you can see differences between the initial 
and density-modified map. Some other places where you may be able to 
see clear differences are the side chains Trp 58, Phe 235, and Leu 433, 
and the main-chain near Asp 426.  You can also compare these maps to 
the truncated high-resolution map (emd_2984_3.5A.ccp4).

