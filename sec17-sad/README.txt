
TUTORIAL INFO

The Sec17 tutorial demonstrates SAD phasing, so only a single Scalepack file
and sequence are required.

The experiment wavelength is 0.98 and the resolution 3.3 A.

Appropriate theoretical f' and f" values can be calculated based on
this information.

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: Vesicular transport protein Sec17 (SAD) 
Hit OK once or twice to set up tutorial

On left panel of Phenix GUI hit "last modified" once or twice to
show the tutorial with a check mark next to it.

Files:

sec17.seq:   The sequence of the protein 
sec17.sca:  X-ray data for the protein


In GUI:
Select Crystals, then Experimental Phasing, then AutoSol

Put in a title:  "Sec17 SAD data to 3.3 A with Se"

Hit the Browse button in File Path and choose these files to load:
  sec17.seq
  sec17.sca

Set the wavelength (symbol lambda) to: 0.98
Set the atom type to: Se

On the second tab ("Options and output"), set the resolution to 3.3 (A)

Now your parameters are set.

Hit "Run"  to start
After 15-30 minutes the job should finish and you can use Coot to look at
the model and heavy atom sites that are obtained along with the map that
is produced.  

This structure is quite difficult.  The map obtained is ok; the correlation between 
a map calculated from the deposited model (1QQE) and the density modified map 
from AutoSol is about 0.7 in this case, but at 3.3 A with a modest map 
the model-building only builds fragments of the model.

You can go on and hit the "Run AutoBuild" button and then run AutoBuild starting
from the AutoSol map and model. This will build most (but not all) of the model 
and yield a free R value of about 0.37.


REFERENCE:

Rice LM, Brunger AT. Crystal structure of the vesicular transport protein
Sec17: implications for SNAP function in SNARE complex disassembly. Mol Cell.
1999 Jul;4(1):85-95.

http://www.ncbi.nlm.nih.gov/pubmed/10445030
PDB: 1QQE
