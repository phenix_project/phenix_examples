
TUTORIAL INFORMATION

The structure 1ic1 (ICAM-1) has two molecules in the asymmetric unit.
The average B-factor for all the atoms in molecule 1 is 28 A^2 and
for all the atoms in molecule 2 it is 69 A^2. Each molecule consists
of two domains. There is a difference in the hinge angle between
the two domains in the two molecules.

Model files: 1ic1_A1.pdb 1ic1_A2.pdb
Target files: 1ic1_sigmaa.mtz 1ic1.seq

There are 2 copies of each search model in the asymmetric unit
The model and the target have 100% sequence identity

MACROCYCLE ROT ON TRA ON BFAC ON VRMS ON

The job will run quickly with the following keywords set

RESCORE ROTATION OFF
RESCORE TRANLATION OFF

Citation:
A dimeric crystal structure for the N-terminal two domains of intercellular adhesion molecule-1.
Casasnovas, JM, Stehle, T, Liu, JH, Wang, JH, Springer, TA
Journal: (1998) Proc.Natl.Acad.Sci.USA 95: 4134-4139
