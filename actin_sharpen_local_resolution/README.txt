This example is anisotropic sharpening and local resolution of a cryo-EM map.

The data are half-maps for a filament of actin, myosin and calmodulin, 
(PDB entry 6c1g EMD entry 7330, 3.8 A), boxed to contain the strongest
density, which is one actin domain .

The files are:

half_map_1_box.ccp4,half_map_2_box.ccp4  -- half maps 
6c1g.fasta  -- sequence file
6c1g_ABCD.pdb  -- known model for the complex (chains ABCD only)
full_map_box.ccp4 -- full reconstruction

Note: all data are copied from the actin_denmod tutorial

The maps are all cut out (boxed) from the deposited maps (just to make the
files smaller for this demo).

Anisotropic sharpening:

In the Phenix GUI, in the "Cryo EM" section, click on "Map improvement",
 and then "Map Sharpening"

Select "Add file" and enter the two half-maps, the full map, and the model.

A title will be generated for you, but you can edit it if you like.

Keep the sharpening method set at "shells", and leave the shell-sharpening options
set at "Anisotropic sharpening", "Local resolution map", and "sharpen all maps".

You can optionally also select local sharpening; that will take longer,
 about 10 minutes with 8 processors.

Hit Run and an anisotropically-sharpened map will be calculated. 

Also a local resolution map showing the resolution of the sharpened map will
be generated.  

Output maps:
half_map_1_box_and_half_map_2_box_sharpened.ccp4        -- sharpened map
half_map_1_box_and_half_map_2_box_local_resolution.ccp4 -- local resolution map

The naming is just the names of the two half maps connected by the word "and",
ending with "sharpened.ccp4" or "local_resolution.ccp4".

The sharpened map is your improved map. The local resolution map is a map
that shows the resolution at each point in the map. It is normally used to
color the sharpened map.

If you open in ChimeraX the sharpened map will be displayed, colored by 
local resolution.  If you want to change the coloring scheme, you can edit the
command shown in the GUI window and paste it into the ChimeraX command box. You
can also edit this command to color other maps you load into ChimeraX.

Local resolution:

In the example above, a local resolution calculation was carried out for you.
You can run a local resolution calculation yourself with the 
"Local resolution" method in the Cryo EM section of the Phenix GUI. 
You can use the original half-maps for this. (The local resolution does 
not change when you sharpen the map, just your view of the map changes).

Enter the two half-maps (half_map_1_box.ccp4,half_map_2_box.ccp4) and select
the number of processors to use. Hit Run and in a minute you should
get your local resolution map.  If you hit the ChimeraX button, you will
get your original map, colored by local resolution.

Output maps:
half_map_1_box_and_half_map_2_box_full.ccp4        -- average of half maps
half_map_1_box_and_half_map_2_box_local_resolution.ccp4 -- local resolution map

