Human Exonuclease-V, PDB entry 7lw7

Human exonuclease V, also known as EXO5, is an enzyme that plays a role in DNA
repair and maintenance. As an exonuclease, it operates by removing nucleotides
from the end of DNA strands, making it an essential component of various DNA
repair mechanisms, particularly in the resolution of DNA lesions and
double-strand breaks.

The first Alphafold database was trained on PDB and sequence data available on
15 February 2021. At this time there were no detectable homologues of ExoV that
had known structures, so models built with this database had no templates.

The structure was determined by Hambarde et al
(https://doi.org/10.1016/j.molcel.2021.05.027), who exploited the presence of
an iron-sulfur (Fe4S4) cluster and collected data at the Fe edge wavelength
(1.7369 Å) for SAD phasing. Unfortunately, these data are not deposited.
Instead, data collected far from the iron edge (1.1158 Å) are available.

Files:
model: AF-Q9H790-F1-model_v2.pdb
sequence of expression construct: exoV_construct.seq
diffraction data: 7lw7.mtz
PAE matrix: AF-Q9H790-F1-predicted_aligned_error_v2.json
rendering of PAE matrix: AF-Q9H790_PAE.png

This tutorial demonstrates the quality of model that can be obtained using
AlphaFold, as well as demonstrating the utility of MR-SAD (combining a
molecular replacement solution with anomalous data to find anomalous scatterer
sites). We will discover whether an AlphaFold model would have been
sufficiently accurate to use successfully as a molecular replacement model and,
if so, could MR-SAD have been used to find the Fe atom positions using data
with a weak anomalous signal.

The construct used for crystallisation omitted the 30 N-terminal residues
expected to form a signal sequence but included a 3-residue expression tag.

A Matthews volume calculation shows that there is room for one copy of the
protein (346 residues).

The model provided for the protein was obtained from the AlphaFold database at
the European Bioinformatics Institute, along with the PAE (predicted aligned
error) matrix (in .json format), which is very useful in allowing you to
automatically divide the model into domains. To see the information contained
in this matrix, you can either take a look at the rendering in the .png file,
included here, or interact with the PAE matrix on the AlphaFold database site,
drawing a box around blocks of dark green to see what this corresponds to on
the predicted structure. 

Determine how many domains are indicated by the PAE matrix. 

Look at the conformation of the N-terminal signal sequence. Do you think the
conformation is biologically relevant? Run process_predicted_model from the
Phenix GUI (under the AlphaFold tab), remembering to specify the PAE file in
.json format as an optional input file. Examine the input and output PDB files
in coot, ChimeraX or PyMol, coloring by B-factor. Note that the highest
confidence regions of the structure have large pLDDT values (“B-factors” in the
PDB file from AlphaFold); if these were interpreted as B-factors, they would
get the least weight in the MR calculations! After the model has been
processed, they are given low B-factors. In addition, the lowest-confidence
residues are removed.

Phaser needs to have an estimate for coordinate error. Experience so far
suggests that an estimated overall RMSD of 0.8-1.2A works well for AlphaFold
models; for this case, the choice of 1.0 will work well.

When you've solved the structure by MR, the Phenix GUI will notice that the
data included Friedel mates and will offer you the option to follow up with
MR-SAD. Enter the atom type for the largest anomalous scatterer and provide a
value for the expected RMSD (1.0 Å is a good generic choice, but you can find a
refined estimate at the end of the Phaser-MR log file). You may wish to turn
off the "Autobuild option under the "Options and output" tab to save time.

Do the Fe atoms found in the MR-SAD procedure make sense?

Citation: https://doi.org/10.1016/j.molcel.2021.05.027
