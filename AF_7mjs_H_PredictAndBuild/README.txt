PredictAndBuild Cryo-EM TUTORIAL with Fab heavy chain 
Running time: 10 min

PDB entry 7mjs chain H (Fab heavy chain in complex with MFSD2A)

In this example you will use PredictAndBuild to generate a model 
for an Fab heavy chain in which a loop that interacts with other chains
is not correctly predicted.  You will use PredictAndBuild to trim it, 
dock it in the map, rebuild the loop and refine the model.

You are going to supply a sequence file (the sequence of the Fab heavy chain
only) and two half-maps covering just the Fab heavy chain.

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: Fab heavy chain (with predicted models supplied)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to 
show the tutorial with a check mark next to it.

Files:

7mjs_23883_H.fa:   The sequence of the Fab heavy chain
7mjs_23883_H_1.ccp4:  Half-map 1 for this part of the structure
7mjs_23883_H_2.ccp4:  Half-map 2
7mjs_23883_H_full.ccp4: Full map

NOTE: maps and model have been boxed and offset from their original positions

Note for running off-line:
==========================
If you want to run this tutorial off-line, the directory
Demo_AF_7mjs_H_CarryOn contains the predicted models for this
structure. In the GUI you can check the "carry_on" button on the
Prediction and Building Settings to use these predicted models.

Run PredictAndBuild
===================

In GUI: 
Select AlphaFold (Predicted models...)/PredictAndBuild (Cryo-EM)

Select "Add files" and choose these files to load:
  7mjs_23883_H.fa
  7mjs_23883_H_1.ccp4
  7mjs_23883_H_2.ccp4 

Notice that when you load these files, the "High-resolution limit" box
will be filled in automatically and a "job title" will be created for 
you.

To make this analysis a little more difficult, in the 
Prediction and Building settings uncheck "Include templates from the PDB".

Now your parameters are set.

Hit "Run"  to start

When it finishes (about 10 minutes)... launch either Coot or Chimera 
and have a look at the overall_best model and map, and compare them to 
the 7mjs_23883_H.pdb model of the Fab from the PDB.

Also have a look at the untrimmed docked predicted models for each cycle.
These are in the output directory (accessible with the Output directory
button at the bottom of the Results pages).  These are the the
AlphaFold predicted models, superimposed on the overall_best model.  Notice
how the loop at residues 100-120 is quite different from the model in the
PDB on the first cycle but is much more similar on the second, as it has 
used the rebuilt model from the first cycle as a template.
