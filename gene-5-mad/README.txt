
TUTORIAL INFO

Input files are three wavelengths (peak, inflection, and high remote)
collected on a SeMet crystal.  

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: Bacteriophage f1 Gene V protein (MAD)
Hit OK once or twice to set up tutorial

On left panel of Phenix GUI hit "last modified" once or twice to
show the tutorial with a check mark next to it.

Files:

sequence.dat:   The sequence of the protein
peak.sca:  Peak dataset
infl.sca:  Inflection point dataset
high.sca:  High-energy remote dataset
p9.sca:  X-ray data for the protein

In GUI:
Select Crystals, then Experimental Phasing, then AutoSol

Put in a title:  "Gene V protein MAD 2.6 A data with Se"

Hit the Browse button in File Path and choose these files to load:
sequence.dat
high.sca
peak.sca
infl.sca

Set the wavelength (symbol lambda) and f' and f" values for each dataset:

            lambda  f'      f''
high.sca    0.9000  -1.6      3.4
peak.sca    0.9794  -7.7      5.8
infl.sca    0.9797  -9.6      2.2

Set the atom type to: Se

On the Options and Output tab:
Set the resolution to 2.6 (A)

Hit Run to analyze the data and produce a map and model.



REFERENCE:

Skinner MM, Zhang H, Leschnitzer DH, Guan Y, Bellamy H, Sweet RM, Gray CW,
Konings RN, Wang AH, Terwilliger TC. Structure of the gene V protein of
bacteriophage f1 determined by multiwavelength x-ray diffraction on the
selenomethionyl protein. Proc Natl Acad Sci U S A. 1994 Mar 15;91(6):2071-5.

http://www.ncbi.nlm.nih.gov/pubmed/8134350
PDB: 1VQB
