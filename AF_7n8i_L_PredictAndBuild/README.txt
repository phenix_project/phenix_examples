PredictAndBuild Cryo-EM TUTORIAL (Run time: 20 min)

SARS CoV2 N-terminal domain (NTD), PDB entry 7n8i

In this example you will use PredictAndBuild to generate a model 
for the NTD of SARS CoV2, trim it, obtain a molecular replacement 
result, and rebuild the molecular replacement model.

This tutorial is like the AF_exoV_PredictAndBuild tutorial except that
it uses cryo-EM data instead of X-ray data.

You are going to supply a sequence file (the sequence of the 
NTD only) and two half-maps covering just the NTD.

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: 7n8i (with PredictAndBuild, AlphaFold model)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to 
show the tutorial with a check mark next to it.

Files:

7n8i_24237.seq:   The sequence of the NTD of this variant of Sars Cov2
7n8i_24237_box_1.ccp4:  Half-map 1 for this part of the structure
7n8i_24237_box_2.ccp4:  Half-map 2


Run PredictAndBuild
===================

In GUI: 
Select AlphaFold (Predicted models...)/PredictAndBuild (Cryo-EM)

Select 'Add file' and load in these files:
  7n8i_24237.seq
  7n8i_24237_box_1.ccp4
  7n8i_24237_box_2.ccp4 

On the second tab ("Prediction and Building Settings"), change the
rebuilding strategy from Standard to Quick (the predicted model in
this case is quite good, so rebuilding is not going to be necessary).

Hit "Run"  to start

When it finishes ... launch either Coot or Chimera and have a look at the
final model and final map.

****************************************************************************
*********************  COMMAND-LINE INSTRUCTIONS ***************************
****************************************************************************

Copy the entire directory AF_7n8i_L_PredictAndBuild from the phenix_examples 
directory to your working directory:

cp -r $PHENIX/modules/phenix_examples/AF_7n8i_L_PredictAndBuild .

Now get into the local directory:

cd AF_7n8i_L_PredictAndBuild

phenix.predict_and_build number_of_models=1 crystal_info.resolution=2.56  half_map=7n8i_24237_box_1.ccp4 half_map=7n8i_24237_box_2.ccp4 seq_file=7n8i_24237.seq number_of_models=1 include_templates_from_pdb=False control.nproc=4 cycles=1 

