Ligand fitting in the Phenix GUI

This tutorial will fit ATP into the binding site of N-ethylmaleimide sensitive
factor.

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: N-ethylmaleimide sensitive factor + ATP (ligand fitting)
Hit OK once or twice to set up tutorial

On left panel of Phenix GUI hit "last modified" once or twice to
show the tutorial with a check mark next to it.

Refine the model and get an mFo-DFc map to fit the ligand
=========================================================

Let's first get a good difference map to use in ligand fitting.

In the Phenix GUI: 

Select Crystals/Refinement/and choose phenix.refine 
Set the title: "Refinement of unliganded model"
Add the data file:  nsf-d2.mtz
Add the model: nsf-d2_noligand.pdb
Set the resolution: 2 (A)

This dataset does not have free R flags, so we need to generate them. In
the middle panel of the Input Data page, click "Options..." and then
check the box by "Generate new R-free flags" and hit OK.

Hit Run/Run now to refine the model and create a difference map.
The GUI will start up Coot while it is running.  When it is finished, have
a look at the mFo-DFc difference map (nsf-d2-ligand_0_refine_001.mtz 
FOFCWT PHFOFCWT) which should show very clear density for the ligand. Now
we are ready to fit the ligand into this density.


Set up Ligand Fitting
=====================

In the Phenix GUI choose Ligands and select "LigandFit"

Give it a title: "Fitting ATP into nsf-d2 difference map"
Select the three files you need with the "Add file" button and set their
data types:

Refine_0/nsf-d2-ligand-0-refine_001.mtz  data type: precalculated map coeffs
Refine_0/nsf-d2-ligand-0-refine_data.mtz  data type: precalculated map coeffs
Refine_0/nsf-d2-ligand-0-refine_001.pdb data type: ligand-free model
atp.pdb  # the ligand 

Note: you might need to click "modify data type" and set the data type more
than once to get everything right.

Set the ligand map type to "pre-calculated" and make sure the
Input labels are "FOFCWT PHFOFCWT"

You are ready to fit the ligand....hit Run and in a couple minutes it will
finish. 

You can open the resulting fitted ligand on the "Summary" tab with
"Open in Coot". If you have your previous Coot window open, you might want to
turn off the original map and model. The ligand should fit nicely into 
the difference density.

