Apoferritin model-building

This example is model-building using a cryo-EM map and phenix.map_to_model
The map is apoferritin, density modified (from EMD entry 20026, 1.8 A)
The model-building will use the "quick" option to speed it up. A "medium" run
is recommended for normal use. 

The files are:

apoferritin.seq -- sequence of apoferritin
apoferritin_denmod_box.ccp4 -- density-modified map, boxed around known model
apoferritin_chainA_rsr.pdb -- known model refined against sharpened map

The map is cut out (boxed) from the deposited maps (just to make the
files smaller for this demo).

===========================================================================
MODEL-BUILDING TUTORIAL (GUI, 12 minute run time)
===========================================================================

This tutorial will carry out model-building using 1.8 A data on 
human apoferritin.  It is designed to be run from the Phenix GUI

Set up tutorial data
====================
In the Phenix GUI:
New project/Set up tutorial data/Select a dataset
Choose: Model building (apoferritin, one chain at 1.8 A)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Examine density-modified map
===============================

Select Coot (top of page)
Then select: File/Open coordinates/apoferritin_chainA_rsr.pdb
             File/Open map/apoferritin_denmod_box.ccp4
             Dispay Manager/select Properties for the map/Set level: 5
               rmsd/Change by rmsd: 0.3/OK
Look at Trp 87 (Draw/GotoAtom/Residue number:87) His 105 and notice they both
fit the density well.

Run model-building 
==================

Phenix GUI: Select Cryo-EM/Docking and Model-building/ MapToModel

Then set these: parameters:
Title:                               apoferritin map-to-model quick run
Map file:                            apoferritin_denmod_box.ccp4
High-resolution limit:                          2.5  (optional)
Seq file:                            apoferritin.seq
Thoroughness:                        quick
Number of processors:                4
Asymmetric map:                      True (check mark)

Then hit Run to start the job.

Examine model in map_to_model_xx/map_to_model.pdb and compare with
apoferritin_chainA_rsr.pdb
==================================================================

"Open in Coot"  opens on top of existing Coot window and loads model. Most
of this quickly-built model is pretty good, but not everything.
Look at Trp 87 and His105 in apoferritin_chainA_rsr.pdb and notice that
at His 105 map_to_model.pdb model is very similar but at Trp 87 it is
built backwards intot he density in this quick build.


What to do after map_to_model:
==============================
 You might want to rebuild your model to improve it with the tool
 Rebuild and Model in Place.  Or if the model is not complete (fragmented)
 you might want to try and complete it with Reassemble a Cryo-EM Model.
