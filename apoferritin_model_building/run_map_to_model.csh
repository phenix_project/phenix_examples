#!/bin/csh -f

# run.csh

# This example is model-building using a cryo-EM map and phenix.map_to_model
# The map is apoferritin, density modified (from EMD entry 20026, 1.8 A)

# The files are:

# apoferritin.seq -- sequence of apoferritin
# apoferritin_denmod_box.ccp4 -- density-modified map, boxed around known model
# apoferritin_chainA_rsr.pdb -- known model refined against sharpened map

# You can set the number of processors (nproc) here:

set nproc=4

echo "RUNNING MODEL-BUILDING NPROC=$nproc"

# To run quick model-building from the command line:

phenix.map_to_model resolution=2.5 apoferritin_denmod_box.ccp4 \
   seq_file=apoferritin.seq\
   nproc=$nproc \
   thoroughness=quick output_directory=map_to_model

# In about 5 minutes this will produce the file:
# map_to_model/map_to_model.pdb

# In Coot or Chimera you can load in the model (apoferritin_chainA_rsr.pdb) 
# and compare it to your new model in map_to_model/map_to_model.pdb

#      What to do after map_to_model:
#  You might want to rebuild your model to improve it with the tool
#  phenix.rebuild_model.  Or if the model is not complete (fragmented)
#  you might want to try and complete it with phenix.model_completion
