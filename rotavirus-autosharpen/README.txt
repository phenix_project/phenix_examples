AUTOSHARPEN TUTORIAL

This tutorial will carry out automatic map sharpening on a small map containing
part of a rotavirus VP6 protein at a resolution of 2.6 A. It can be run from
the GUI or command line.

GUI INSTRUCTIONS:

Set up tutorial data
====================

New project/Set up tutorial data/Select a dataset
Choose: Major capsid protein of group A rotavirus (Autosharpen map)
Hit OK once or twice to set up tutorial
On left panel of Phenix GUI hit "last modified" once or twice to show the
tutorial with a check mark next to it.

Examine starting map
====================

Select Coot (top of page)/File/Open coordinates/rotavirus.pdb
then: File/Open map/rotavirus_blurred_map.ccp4

Run auto-sharpening
========================

In GUI: Select Cryo-EM/Map improvement and choose MapSharpening

Title: Rotavirus auto-sharpen 2.6 A
Select "Add file" and choose: rotavirus_blurred_map.ccp4
 Modify the data type to be "Map"

Add the sequence file: rotavirus.fa

Set the sharpening type to "b_factor" (Wilson B factor applied as a
function of resolution).

Set the resolution: 2.6 (A)

Hit Run

When done...hit the "Open in ChimeraX" button to see the original 
(blurred) and sharpened maps.


COMMAND-LINE INSTRUCTIONS:

phenix.auto_sharpen rotavirus_blurred_map.ccp4 resolution=2.6

You can compare the resulting map (sharpened_map.ccp4) with the model in 
rotavirus_blurred_map.pdb.

Reference: Grant, T., Grigorieff, N. (2015). eLife 2015;4:e06980.  Measuring the optimal exposure for single particle cryo-EM using a 2.6 Å reconstruction of rotavirus VP6
