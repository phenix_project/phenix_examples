#!/bin/csh -f

# auto-sharpen blurred map
phenix.auto_sharpen rotavirus_blurred_map.ccp4 resolution=2.6
