Streptomyces lividans acetyl-xylan esterase is an enzyme involved in the
degradation of xylan, a complex polysaccharide found in plant cell walls. Xylan
is often acetylated, and the enzyme's role is to cleave these acetyl groups.
The removal of acetyl groups from xylan is important for the subsequent
enzymatic degradation of xylan into simpler sugars, such as xylose. This
process is crucial in the utilization of xylan as a carbon source for
microorganisms like Streptomyces lividans. Acetyl-xylan esterases have gained
attention in biotechnology and biofuel production.

This tutorial demonstrates how to account for translational
non-crystallographic symmetry (tNCS) in molecular replacement.

The structure, PDB entry 2cc0, was originally solved by molecular replacement
using the structure of a relatively remote homologue. Here we use the AlphaFold
database structure, to concentrate on the tNCS aspect.

Note that the expression construct covered just the CE4 domain of the Strep
lividans acetyl-xylan esterase, comprising residues 42-233, whereas the
AlphaFold database model includes the leader sequence and an additional
C-terminal domain.

Files:
AF-Q54413-F1-model_v3.pdb  AlphaFold database model for whole protein
AF_processed.pdb           Trimmed and processed model
Sl_CE4_domain.seq          Sequence of expression construct
Sl_esterase.mtz            Intensity data corresponding to PDB 2cc0
                           (but note alternative indexing)

Check the data using phenix.xtriage. How large is the biggest native Patterson
peak? What are the fractional coordinates of that peak? Could the presence of
tNCS confuse space group determination?

If time permits, you could prepare the model yourself with
process_predicted_model in the Phenix GUI under the AlphaFold tab, remembering
to provide the PAE file in .json format as an optional input file. This will
convert the pLDDT values in the B-factor column of the model into sensible
B-factors that downweight the lower-confidence parts of the model, as well as
trimming off the least-confident parts and checking whether the model should be
separated into independent domains.  Alternatively, you could just look at the
full model to see which domain corresponds to the residues in the expression
construct, and then use the pre-prepared model.

Solve the structure by molecular replacement in Phaser. How does the presence
of tNCS restrict your choice of the possible number of copies in the asymmetric
unit?

Citation: https://doi.org/10.1074/jbc.M513066200

The data used for this tutorial, which include intensity data that were not
deposited at the PDB, were kindly provided by Gideon Davies. Note that these
data use an alternative indexing to the deposited structure and data in PDB
entry 2cc0, if you want to compare structures at the end!
