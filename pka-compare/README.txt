
Notes for example 'pka-compare':
--------------------------------

This set of structures is intended for use with the structure comparison
tool in the PHENIX GUI.  The files can be loaded into the GUI simultaneously
by dragging the entire data directory into the list at the top of the
window.

References:
-----------

Madhusudan, Akamine P, Xuong NH, Taylor SS. Crystal structure of a transition
state mimic of the catalytic subunit of cAMP-dependent protein kinase. Nat
Struct Biol. 2002 9:273-7.
PubMed PMID: 11896404; PDB ID 1l3r

Wu J, Yang J, Kannan N, Madhusudan, Xuong NH, Ten Eyck LF, Taylor SS.
Crystal  structure of the E230Q mutant of cAMP-dependent protein kinase
reveals an unexpected apoenzyme conformation and an extended N-terminal A
helix. Protein Sci. 2005 14:2871-9.
PubMed PMID: 16253959; PDB ID 1syk

Kim C, Xuong NH, Taylor SS. Crystal structure of a complex between the
catalytic and regulatory (RIalpha) subunits of PKA. Science 2005 307:690-6.
PubMed PMID: 15692043; PDB ID 3fhi

Orts J, Tuma J, Reese M, Grimm SK, Monecke P, Bartoschek S, Schiffer A, Wendt
KU, Griesinger C, Carlomagno T. Crystallography-independent determination of
ligand binding modes. Angew Chem Int Ed Engl. 2008;47(40):7736-40.
PubMed PMID: 18767090; PDB IDs 3dnd, 3dne

Thompson EE, Kornev AP, Kannan N, Kim C, Ten Eyck LF, Taylor SS. Comparative
surface geometry of the protein kinase family. Protein Sci. 2009 18:2016-26.
PubMed PMID: 19610074; PDB ID 3fjq
